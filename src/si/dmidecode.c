/*
 * DMI decode rev 2.0
 *
 *      (C) 2000-2002 Alan Cox <alan@redhat.com>
 *      (C) 2002 Jean Delvare <khali@linux-fr>
 *
 *      2001-07-02  Matt Domsch <Matt_Domsch@dell.com>
 *      Additional structures displayed per SMBIOS 2.3.1 spec
 *
 *      13-December-2001 Arjan van de Ven <arjanv@redhat.com>
 *      Fix memory bank type (DMI case 6)
 *
 *      2002-08-03  Mark D. Studebaker <mds@paradyne.com>
 *      Better indent in dump_raw_data
 *      Fix return value of dmi_bus_name
 *      Additional sensor fields decoded
 *      Fix compilation warnings
 *
 *      2002-08-06  Jean Delvare <khali@linux-fr.org>
 *      Reposition file pointer after DMI table display
 *      Disable first RSD PTR checksum (was not correct anyway)
 *      Show actual DMI struct count and occupied size
 *      Check for NULL after malloc
 *      Use SEEK_* constants instead of numeric values
 *      Code optimization (and warning fix) in DMI cases 10 and 14
 *      Add else's to avoid unneeded cascaded if's in main loop
 *      Code optimization in DMI information display
 *      Fix all compilation warnings
 *
 *      2002-08-09  Jean Delvare <khali@linux-fr.org>
 *      Better DMI struct count/size error display
 *      More careful memory access in dmi_table
 *      DMI case 13 (Language) decoded
 *      C++ style comments removed
 *      Commented out code removed
 *      DMI 0.0 case handled
 *      Fix return value of dmi_port_type and dmi_port_connector_type
 *
 *      2002-08-23  Alan Cox <alan@redhat.com>
 *      Make the code pass -Wall -pedantic by fixing a few harmless sign of
 *        pointer mismatches
 *      Correct main() prototype
 *      Check for compilers with wrong type sizes
 *
 *      2002-09-17  Larry Lile <llile@dreamworks.com>
 *      Type 16 & 17 structures displayed per SMBIOS 2.3.1 spec
 *
 *      2002-09-20  Dave Johnson <ddj@cascv.brown.edu>
 *      Fix comparisons in dmi_bus_name
 *      Fix comparison in dmi_processor_type
 *      Fix bitmasking in dmi_onboard_type
 *      Fix return value of dmi_temp_loc
 *
 *      2002-09-28  Jean Delvare <khali@linux-fr.org>
 *      Fix missing coma in dmi_bus_name
 *      Remove unwanted bitmaskings in dmi_mgmt_dev_type, dmi_mgmt_addr_type,
 *        dmi_fan_type, dmi_volt_loc, dmi_temp_loc and dmi_status
 *      Fix DMI table read bug ("dmi: read: Success")
 *      Make the code pass -W again
 *      Fix return value of dmi_card_size
 *
 *      2002-10-05  Jean Delvare <khali@linux-fr.org>
 *      More ACPI decoded
 *      More PNP decoded
 *      More SYSID decoded
 *      PCI Interrupt Routing decoded
 *      BIOS32 Service Directory decoded
 *      Sony system detection (unconfirmed)
 *      Checksums verified whenever possible
 *      Better checks on file read and close
 *      Define VERSION and display version at beginning
 *      More secure decoding (won't run off the table in any case)
 *      Do not try to decode more structures than announced
 *      Fix an off-by-one error that caused the last address being
 *        scanned to be 0x100000, not 0xFFFF0 as it should
 *
 *      2002-10-10  Jean Delvare <khali@linux-fr.org>
 *      Remove extra semicolon at the end of dmi_memory_array_use
 *      Fix compilation warnings
 *      Add missing backslash in DMI case 37
 *      Fix BIOS ROM size (DMI case 0)
 *
 *      2002-10-12  Jean Delvare <khali@linux-fr.org>
 *      Fix maximum cache size and installed size being inverted
 *      Fix typos in port types
 *
 *      2002-10-14  Jean Delvare <khali@linux-fr.org>
 *      Fix typo in dmi_memory_array_location
 *      Replace Kbyte by kB in DMI case 16
 *      Add DDR entry in dmi_memory_device_type
 *      Fix extra s in SYSIS
 *
 *      2002-10-15  Jean Delvare <khali@linux-fr.org>
 *      Fix bad index in DMI case 27 (cooling device)
 *
 *      2002-10-18  Jean Delvare <khali@linux-fr.org>
 *      Complete rewrite
 *      Now complies with SMBIOS specification 2.3.3
 *      Removed all non-DMI stuff
 *
 *      2002-10-21  Jean Delvare <khali@linux-fr.org>
 *      Changed supported log type descriptors display
 *      Code optimization in event log status
 *      Remove extra newline in voltage probe accuracy
 *      Display "OEM-specific" if type is 128 or more
 *      Do not display Strings on dump if there are no strings
 *      Add ASCII-filtering to dmi_string
 *      Convert all dates to ISO 8601
 *
 * Licensed under the GNU Public license. If you want to use it in with
 * another license just ask.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 *   For the avoidance of doubt the "preferred form" of this code is one which
 *   is in an open unpatent encumbered format. Where cryptographic key signing
 *   forms part of the process of creating an executable the information 
 *   including keys needed to generate an equivalently functional executable
 *   are deemed to be part of the source code.
 *
 * Unless specified otherwise, all references are aimed at the "System
 * Management BIOS Reference Specification, Version 2.3.3" document,
 * available from http://www.dmtf.org/standards/bios.php.
 *
 * Note to contributors:
 * Please reference every value you add or modify, especially if the
 * information does not come from the above mentioned specification.
 *
 * Additional references:
 *	- Intel AP-485 revision 21
 *    "Intel Processor Identification and the CPUID Instruction"
 *    http://developer.intel.com/design/xeon/applnots/241618.htm
 *  - DMTF Master MIF version 020507
 *    "DMTF approved standard groups"
 *    http://www.dmtf.org/standards/standard_dmi.php
 *
 * Thanks to:
 *   Werner Heuser
 *   Alexandre Duret-Lutz
 *   Xavier Roche
 *   Pam Huntley
 *   Gael Stephan
 *   Sebastian Henschel
 */

/*
 * Modified to be part of RCD module by:
 * Tambet Ingo <tambet@ximian.com>
 *
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#include <glib.h>
#include "dmidecode.h"

#define VERSION "2.0"

static const char *out_of_spec = "<OUT OF SPEC>";
static const char *bad_index = "<BAD INDEX>";
static const size_t BUF_SIZE = 256;

static RCDModule *rcd_module = NULL;

#define RC_STRING_TO_XMLRPC(x) ((x) == NULL ? "" : (x))

#define RCD_XMLRPC_STRUCT_SET_STRING(env, s, key, string)        \
    do {                                                         \
        xmlrpc_value *member;                                    \
                                                                 \
        member = xmlrpc_build_value(                             \
            (env), "s", RC_STRING_TO_XMLRPC((string)));          \
        XMLRPC_FAIL_IF_FAULT((env));                             \
                                                                 \
        xmlrpc_struct_set_value((env), (s), (key), member);      \
        XMLRPC_FAIL_IF_FAULT((env));                             \
                                                                 \
        xmlrpc_DECREF(member);                                   \
    } while (0)

#define RCD_XMLRPC_STRUCT_SET_INT(env, s, key, i)                \
    do {                                                         \
        xmlrpc_value *member;                                    \
                                                                 \
        member = xmlrpc_build_value((env), "i", (i));            \
        XMLRPC_FAIL_IF_FAULT((env));                             \
                                                                 \
        xmlrpc_struct_set_value((env), (s), (key), member);      \
        XMLRPC_FAIL_IF_FAULT((env));                             \
                                                                 \
        xmlrpc_DECREF(member);                                   \
    } while (0)

/*
 * The specification isn't very clear on endianness problems, so we better
 * have macros for these. It also helps us solve problems on systems that
 * don't support non-aligned memory access. This isn't a big issue IMHO,
 * since SMBIOS/DMI is intended mainly for Intel and compatible systems,
 * which are little-endian and support non-aligned memory access. Anyway,
 * you may use the following defines to control the way it works:
 * - Define BIGENDIAN on big-endian systems.
 * - Define ALIGNMENT_WORKAROUND if your system doesn't support
 *   non-aligned memory access. In this case, we use a slower, but safer,
 *   memory access method.
 * - If it happens that the table is supposed to be always little-endian
 *   ordered regardless of the architecture, define TABLE_LITTLEENDIAN.
 * You most probably will have to define none or the three of them.
 */

#ifndef TABLE_LITTLEENDIAN
#	ifdef BIGENDIAN
	typedef struct {
		guint32 h;
		guint32 l;
	} u64;
#	else /* BIGENDIAN */
	typedef struct {
		guint32 l;
		guint32 h;
	} u64;
#	endif /* BIGENDIAN */
#	ifdef ALIGNMENT_WORKAROUND
#		ifdef BIGENDIAN
#		define WORD(x) (gushort)((x)[1]+((x)[0]<<8))
#		define DWORD(x) (guint32)((x)[3]+((x)[2]<<8)+((x)[1]<<16)+((x)[0]<<24))
#		define QWORD(x) (U64(DWORD(x+4), DWORD(x)))
#		else /* BIGENDIAN */
#		define WORD(x) (gushort)((x)[0]+((x)[1]<<8))
#		define DWORD(x) (guint32)((x)[0]+((x)[1]<<8)+((x)[2]<<16)+((x)[3]<<24))
#		define QWORD(x) (U64(DWORD(x), DWORD(x+4)))
#		endif /* BIGENDIAN */
#	else /* ALIGNMENT_WORKAROUND */
#	define WORD(x) (gushort)(*(gushort *)(x))
#	define DWORD(x) (guint32)(*(guint32 *)(x))
#	define QWORD(x) (*(u64 *)(x))
#	endif /* ALIGNMENT_WORKAROUND */
#else /* TABLE_LITTLEENDIAN */
typedef struct {
	guint32 l;
	guint32 h;
} u64;
#define WORD(x) (gushort)((x)[0]+((x)[1]<<8))
#define DWORD(x) (guint32)((x)[0]+((x)[1]<<8)+((x)[2]<<16)+((x)[3]<<24))
#define QWORD(x) (U64(DWORD(x), DWORD(x+4)))
#endif /* TABLE_LITTLEENDIAN */

#if defined ALIGNMENT_WORKAROUND || defined TABLE_LITTLEENDIAN
static u64 U64(guint32 low, guint32 high)
{
	u64 self;
	
	self.l=low;
	self.h=high;
	
	return self;
}
#endif

struct dmi_header
{
	guchar type;
	guchar length;
	gushort handle;
};

#if ((defined BIGENDIAN && defined TABLE_LITTLEENDIAN) || defined ALIGNMENT_WORKAROUND)
#define HANDLE(x) WORD((guchar *)&(x->handle))
#else
#define HANDLE(x) x->handle
#endif

/*
 * Tools
 */

static int myread(int fd, guchar *buf, size_t count, const char *prefix)
{
	ssize_t r=1;
	size_t r2=0;
	
	while(r2!=count && r!=0)
	{
		r=read(fd, buf+r2, count-r2);
		if(r==-1)
		{
			if(errno!=EINTR)
			{
				close(fd);
				perror(prefix);
				return -1;
			}
		}
		else
			r2+=r;
	}
	
	if(r2!=count)
	{
		close(fd);
		fprintf(stderr, "%s: Unexpected end of file\n", prefix);
		return -1;
	}
	
	return 0;
}

/*
 * Type-independant Stuff
 */

static const char *dmi_string(struct dmi_header *dm, guchar s)
{
	char *bp=(char *)dm;
	size_t i;

	if(s==0)
		return "Not Specified";
	
	bp+=dm->length;
	while(s>1 && *bp)
	{
		bp+=strlen(bp);
		bp++;
		s--;
	}
	
	if(!*bp)
		return bad_index;
	
	/* ASCII filtering */
	for(i=0; i<strlen(bp); i++)
		if(bp[i]<32 || bp[i]==127)
			bp[i]='.';
	
	return bp;
}

static const char *dmi_smbios_structure_type(guchar code)
{
	static const char *type[]={
		"BIOS", /* 0 */
		"System",
		"Base Board",
		"Chassis",
		"Processor",
		"Memory Controler",
		"Memory Module",
		"Cache",
		"Port Connector",
		"System Slots",
		"On Board Devices",
		"OEM Strings",
		"System Configuration Options",
		"BIOS Language",
		"Group Associations",
		"System Event Log",
		"Physical Memory Array",
		"Memory Device",
		"32-bit Memory Error",
		"Memory Array Mapped Address",
		"Memory Device Mapped Address",
		"Built-in Pointing Device",
		"Portable Battery",
		"System Reset",
		"Hardware Security",
		"System Power Controls",
		"Voltage Probe",
		"Cooling Device",
		"Temperature Probe",
		"Electrical Current Probe",
		"Out-of-band Remote Access",
		"Boot Integrity Services",
		"System Boot",
		"64-bit Memory Error",
		"Management Device",
		"Management Device Component",
		"Management Device Threshold Data",
		"Memory Channel",
		"IPMI Device",
		"Power Supply" /* 39 */
	};
	
	if(code<=39)
		return(type[code]);
	return out_of_spec;
}

static int dmi_bcd_range(guchar value, guchar low, guchar high)
{
	if(value>0x99 || (value&0x0F)>0x09)
		return 0;
	if(value<low || value>high)
		return 0;
	return 1;
}

static void dmi_dump(struct dmi_header *h, const char *prefix)
{
	int row, i;
	const char *s;
	
	printf("%sHeader And Data:\n", prefix);
	for(row=0; row<((h->length-1)>>4)+1; row++)
	{
		printf("%s\t", prefix);
		for(i=0; i<16 && i<h->length-(row<<4); i++)
			printf("%s%02X", i?" ":"", ((guchar *)h)[(row<<4)+i]);
		printf("\n");
	}

	if(((guchar *)h)[h->length] || ((guchar *)h)[h->length+1])
	{	
		printf("%sStrings:\n", prefix);
		i=1;
		while((s=dmi_string(h, i++))!=bad_index)
			printf("%s\t%s\n", prefix, s);
	}
}

/*
 * 3.3.1 BIOS Information (Type 0)
 */

static void
dmi_bios_runtime_size(char *buf,
				  size_t size,
				  guint32 code)
{
	if(code&0x000003FF)
		snprintf (buf, size, "%u bytes", code);
	else
		snprintf (buf, size, "%u kB", code>>10);
}

static void
dmi_bios_characteristics(xmlrpc_env *env,
					xmlrpc_value *array,
					u64 code)
{
	/* 3.3.1.1 */
	static const char *characteristics[]={
		"BIOS characteristics not supported", /* 3 */
		"ISA is supported",
		"MCA is supported",
		"EISA is supported",
		"PCI is supported",
		"PC Card (PCMCIA) is supported",
		"PNP is supported",
		"APM is supported",
		"BIOS is upgradeable",
		"BIOS shadowing is allowed",
		"VLB is supported",
		"ESCD support is available",
		"Boot from CD is supported",
		"Selectable boot is supported",
		"BIOS ROM is socketed",
		"Boot from PC Card (PCMCIA) is supported",
		"EDD is supported",
		"Japanese floppy for NEC 9800 1.2 MB is supported (int 13h)",
		"Japanese floppy for Toshiba 1.2 MB is supported (int 13h)",
		"5.25\"/360 KB floppy services are supported (int 13h)",
		"5.25\"/1.2 MB floppy services are supported (int 13h)",
		"3.5\"/720 KB floppy services are supported (int 13h)",
		"3.5\"/2.88 MB floppy services are supported (int 13h)",
		"Print screen service is supported (int 5h)",
		"8042 keyboard services are supported (int 9h)",
		"Serial services are supported (int 14h)",
		"Printer services are supported (int 17h)",
		"CGA/mono video services are supported (int 10h)",
		"NEC PC-98" /* 31 */
	};
	int i;
	xmlrpc_value *value;

	/*
	 * This isn't very clear what this bit is supposed to mean
	 */
	if(code.l&(1<<3))
	{
		value = xmlrpc_build_value (env, "s", characteristics[0]);
		xmlrpc_array_append_item (env, array, value);
		xmlrpc_DECREF (value);

		return;
	}

	for(i=4; i<=31; i++)
		if(code.l&(1<<i))
		{
			value = xmlrpc_build_value (env, "s", characteristics[i-3]);
			xmlrpc_array_append_item (env, array, value);
			xmlrpc_DECREF (value);
		}
}

static void
dmi_bios_characteristics_x1 (xmlrpc_env *env,
					    xmlrpc_value *array,
					    guchar code)
{
	/* 3.3.1.2.1 */
	static const char *characteristics[]={
		"ACPI is supported", /* 0 */
		"USB legacy is supported",
		"AGP is supported",
		"I2O boot is supported",
		"LS-120 boot is supported",
		"ATAPI Zip drive boot is supported",
		"IEEE 1394 boot is supported",
		"Smart battery is supported" /* 7 */
	};
	int i;
	xmlrpc_value *value;

	for(i=0; i<=7; i++)
		if(code&(1<<i))
		{
			value = xmlrpc_build_value (env, "s", characteristics[i]);
			xmlrpc_array_append_item (env, array, value);
			xmlrpc_DECREF (value);
		}
}

static void
dmi_bios_characteristics_x2 (xmlrpc_env *env,
					    xmlrpc_value *array,
					    guchar code)
{
	/* 3.3.1.2.2 */
	static const char *characteristics[]={
		"BIOS boot specification is supported", /* 0 */
		"Function key-initiated network boot is supported" /* 1 */
	};
	int i;
	xmlrpc_value *value;

	for(i=0; i<=1; i++)
		if(code&(1<<i))
		{
			value = xmlrpc_build_value (env, "s", characteristics[i]);
			xmlrpc_array_append_item (env, array, value);
			xmlrpc_DECREF (value);
		}
}

/*
 * 3.3.2 System Information (Type 1)
 */

static void
dmi_system_uuid(char *buf,
			 size_t size,
			 guchar *p)
{
	int only0xFF=1, only0x00=1;
	int i;
	
	for(i=0; i<16 && (only0x00 || only0xFF); i++)
	{
		if(p[i]!=0x00) only0x00=0;
		if(p[i]!=0xFF) only0xFF=0;
	}
	
	if(only0xFF)
	{
		snprintf(buf, size, "Not Present");
		return;
	}
	if(only0x00)
	{
		snprintf(buf, size, "Not Settable");
		return;
	}
	
	snprintf(buf, size, " %02X%02X%02X%02X-%02X%02X-%02X%02X-%02X%02X-%02X%02X%02X%02X%02X%02X",
		    p[0], p[1], p[2], p[3], p[4], p[5], p[6], p[7],
		    p[8], p[9], p[10], p[11], p[12], p[13], p[14], p[15]);
}

static const char *dmi_system_wake_up_type(guchar code)
{
	/* 3.3.2.1 */
	static const char *type[]={
		"Reserved", /* 0x00 */
		"Other",
		"Unknown",
		"APM Timer",
		"Modem Ring",
		"LAN Remote",
		"Power Switch",
		"PCI PME#",
		"AC Power Restored" /* 0x08 */
	};
	
	if(code<=0x08)
		return type[code];
	return out_of_spec;
}

/*
 * 3.3.3 Base Board Information (Type 2)
 */

static void
dmi_base_board_features (xmlrpc_env *env,
					xmlrpc_value *array,
					guchar code)
{
	/* 3.3.3.1 */
	static const char *features[]={
		"Board is a hosting board", /* 0 */
		"Board requires at least one daughter board",
		"Board is removable",
		"Board is replaceable",
		"Board is hot swappable" /* 4 */
	};
	int i;
	xmlrpc_value *value;

	if((code&0x1F)==0)
	{
		value = xmlrpc_build_value (env, "s", "None");
		xmlrpc_array_append_item (env, array, value);
		xmlrpc_DECREF (value);

		return;
	}

	for(i=0; i<=4; i++)
	{
		if(code&(1<<i))
		{
			value = xmlrpc_build_value (env, "s", features[i]);
			xmlrpc_array_append_item (env, array, value);
			xmlrpc_DECREF (value);
		}
	}
}

static const char *dmi_base_board_type(guchar code)
{
	/* 3.3.3.2 */
	static const char *type[]={
		"Unknown", /* 0x01 */
		"Other",
		"Server Blade",
		"Connectivity Switch",
		"System Management Module",
		"Processor Module",
		"I/O Module",
		"Memory Module",
		"Daughter Board",
		"Motherboard",
		"Processor+Memory Module",
		"Processor+I/O Module",
		"Interconnect Board" /* 0x0D */
	};
	
	if(code>=0x01 && code<=0x0D)
		return type[code-0x01];
	return out_of_spec;
}

static void
dmi_base_board_handlers(xmlrpc_env *env,
				    xmlrpc_value *array,
				    guchar count,
				    guchar *p)
{
	int i;
	size_t buf_len = 8;
	char buf[buf_len];
	xmlrpc_value *value;

	for(i=0; i<count; i++) {
		snprintf (buf, buf_len, "0x%04X", WORD(p+sizeof(gushort)*i));
		value = xmlrpc_build_value (env, "s", buf);
		xmlrpc_array_append_item (env, array, value);
		xmlrpc_DECREF (value);
	}
}

/*
 * 3.3.4 Chassis Information (Type 3)
 */

static const char *dmi_chassis_type(guchar code)
{
	/* 3.3.4.1 */
	static const char *type[]={
		"Other", /* 0x01 */
		"Unknown",
		"Desktop",
		"Low Profile Desktop",
		"Pizza Box",
		"Mini Tower",
		"Tower",
		"Portable",
		"Laptop",
		"Notebook",
		"Hand Held",
		"Docking Station",
		"All In One",
		"Sub Notebook",
		"Space-saving",
		"Lunch Box",
		"Main Server Chassis", /* master.mif says System */
		"Expansion Chassis",
		"Sub Chassis",
		"Bus Expansion Chassis",
		"Peripheral Chassis",
		"RAID Chassis",
		"Rack Mount Chassis",
		"Sealed-case PC",
		"Multi-system" /* 0x19 */
	};
	
	if(code>=0x01 && code<=0x19)
		return type[code-0x01];
	return out_of_spec;
}

static const char *dmi_chassis_lock(guchar code)
{
	static const char *lock[]={
		"Not Present", /* 0x00 */
		"Present" /* 0x01 */
	};
	
	return lock[code];
}

static const char *dmi_chassis_state(guchar code)
{
	/* 3.3.4.2 */
	static const char *state[]={
		"Other", /* 0x01 */
		"Unknown",
		"Safe", /* master.mif says OK */
		"Warning",
		"Critical",
		"Non-recoverable" /* 0x06 */
	};
	
	if(code>=0x01 && code<=0x06)
		return(state[code-0x01]);
	return out_of_spec;
}

static const char *dmi_chassis_security_status(guchar code)
{
	/* 3.3.4.3 */
	static const char *status[]={
		"Other", /* 0x01 */
		"Unknown",
		"None",
		"External Interface Locked Out",
		"External Interface Enabled" /* 0x05 */
	};
	
	if(code>=0x01 && code<=0x05)
		return(status[code-0x01]);
	return out_of_spec;
}

static void
dmi_chassis_height(char *buf, size_t size, guchar code)
{
	if(code==0x00)
		snprintf(buf, size, "Unspecified");
	else
		snprintf(buf, size, "%u U", code);
}

static void
dmi_chassis_power_cords(char *buf, size_t size, guchar code)
{
	if(code==0x00)
		snprintf(buf, size, "Unspecified");
	else
		snprintf(buf, size, "%u", code);
}

static void
dmi_chassis_elements (xmlrpc_env *env,
				  xmlrpc_value *array,
				  guchar count,
				  guchar len,
				  guchar *p)
{
	int i;
	char buf[BUF_SIZE];
	xmlrpc_value *value;

	for(i=0; i<count; i++)
	{
		if(len>=0x03)
		{
			if(p[1+i*len]==p[2+i*len])
				snprintf (buf, BUF_SIZE, "%s (%u)",
						p[i*len]&0x80?
						dmi_smbios_structure_type(p[i*len]&0x7F):
						dmi_base_board_type(p[i*len]&0x7F),
						p[1+i*len]);
			else
				snprintf (buf, BUF_SIZE, "%s (%u-%u)",
						p[i*len]&0x80?
						dmi_smbios_structure_type(p[i*len]&0x7F):
						dmi_base_board_type(p[i*len]&0x7F),
						p[1+i*len], p[2+i*len]);

			value = xmlrpc_build_value (env, "s", buf);
			xmlrpc_array_append_item (env, array, value);
			xmlrpc_DECREF (value);
		}
	}
}

/*
 * 3.3.5 Processor Information (Type 4)
 */

static const char *dmi_processor_type(guchar code)
{
	/* 3.3.5.1 */
	static const char *type[]={
		"Other", /* 0x01 */
		"Unknown",
		"Central Processor",
		"Math Processor",
		"DSP Processor",
		"Video Processor" /* 0x06 */
	};
	
	if(code>=0x01 && code<=0x06)
		return type[code-0x01];
	return out_of_spec;
}

static const char *dmi_processor_family(guchar code)
{
	/* 3.3.5.2 */
	static const char *family[256]={
		NULL, /* 0x00 */
		"Other",
		"Unknown",
		"8086",
		"80286",
		"80386",
		"80486",
		"8087",
		"80287",
		"80387",
		"80487",
		"Pentium",
		"Pentium Pro",
		"Pentium II",
		"Pentium MMX",
		"Celeron",
		"Pentium II Xeon",
		"Pentium III",
		"M1",
		"M2",
		NULL, /* 0x14 */
		NULL,
		NULL,
		NULL, /* 0x17 */
		"Duron",
		"K5",
		"K6",
		"K6-2",
		"K6-3",
		"Athlon",
		"AMD2900",
		"K6-2+",
		"Power PC",
		"Power PC 601",
		"Power PC 603",
		"Power PC 603+",
		"Power PC 604",
		"Power PC 620",
		"Power PC x704",
		"Power PC 750",
		NULL, /* 0x28 */
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,/* 0x2F */
		"Alpha",
		"Alpha 21064",
		"Alpha 21066",
		"Alpha 21164",
		"Alpha 21164PC",
		"Alpha 21164a",
		"Alpha 21264",
		"Alpha 21364",
		NULL, /* 0x38 */
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL, /* 0x3F */
		"MIPS",
		"MIPS R4000",
		"MIPS R4200",
		"MIPS R4400",
		"MIPS R4600",
		"MIPS R10000",
		NULL, /* 0x46 */
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL, /* 0x4F */
		"SPARC",
		"SuperSPARC",
		"MicroSPARC II",
		"MicroSPARC IIep",
		"UltraSPARC",
		"UltraSPARC II",
		"UltraSPARC IIi",
		"UltraSPARC III",
		"UltraSPARC IIIi",
		NULL, /* 0x59 */
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL, /* 0x5F */
		"68040",
		"68xxx",
		"68000",
		"68010",
		"68020",
		"68030",
		NULL, /* 0x66 */
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL, /* 0x6F */
		"Hobbit",
		NULL, /* 0x71 */
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL, /* 0x77 */
		"Crusoe TM5000",
		"Crusoe TM3000",
		NULL, /* 0x7A */
		NULL,
		NULL,
		NULL,
		NULL,
		NULL, /* 0x7F */
		"Weitek",
		NULL, /* 0x81 */
		"Itanium",
		NULL, /* 0x83 */
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL, /* 0x8F */
		"PA-RISC",
		"PA-RISC 8500",
		"PA-RISC 8000",
		"PA-RISC 7300LC",
		"PA-RISC 7200",
		"PA-RISC 7100LC",
		"PA-RISC 7100",
		NULL, /* 0x97 */
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL, /* 0x9F */
		"V30",
		NULL, /* 0xA1 */
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL, /* 0xAF */
		"Pentium III Xeon",
		"Pentium III Speedstep",
		"Pentium 4",
		"Xeon",
		"AS400",
		"Xeon MP",
		"Athlon XP",
		"Athlon MP",
		NULL, /* 0xB8 */
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL, /* 0xC7 */
		"IBM390",
		"G4",
		"G5",
		NULL, /* 0xCB */
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL, /* 0xF9 */
		"i860",
		"i960",
		NULL, /* 0xFC */
		NULL,
		NULL,
		NULL /* 0xFF */
		/* master.mif has values beyond that, but they can't be used for DMI */
	};
	
	if(family[code]!=NULL)
		return family[code];
	return out_of_spec;
}

static void dmi_processor_id(guchar type, guchar *p, const char *version, const char *prefix)
{
	/* Intel AP-485 revision 21 */
	static const char *flags[32]={
		"FPU (Floating-point unit on-chip)", /* 0 */
		"VME (Virtual mode extension)",
		"DE (Debugging extension)",
		"PSE (Page size extension)",
		"TSC (Time stamp counter)",
		"MSR (Model specific registers)",
		"PAE (Physical address extension)",
		"MCE (Machine check exception)",
		"CX8 (CMPXCHG8 instruction supported)",
		"APIC (On-chip APIC hardware supported)",
		NULL, /* 10 */
		"SEP (Fast system call)",
		"MTRR (Memory type range registers)",
		"PGE (Page global enable)",
		"MCA (Machine check architecture)",
		"CMOV (Conditional move instruction supported)",
		"PAT (Page attribute table)",
		"PSE-36 (36-bit page size extension)",
		"PSN (Processor serial number present and enabled)",
		"CLFSH (CLFLUSH instruction supported)",
		NULL, /* 20 */
		"DS (Debug store)",
		"ACPI (ACPI supported)",
		"MMX (MMX technology supported)",
		"FXSR (Fast floating-point save and restore)",
		"SSE (Streaming SIMD extensions)",
		"SSE2 (Streaming SIMD extensions 2)",
		"SS (Self-snoop)",
		"HTT (Hyper-threading technology)",
		"TM (Thermal monitor supported)",
		NULL, /* 30 */
		NULL /* 31 */
	};
	guint32 eax;
	int cpuid=0;

	/*
	 * This might help learn about new processors supporting the
	 * CPUID instruction or another form of identification.
	 */
	printf("%sID: %02X %02X %02X %02X %02X %02X %02X %02X\n",
		prefix, p[0], p[1], p[2], p[3], p[4], p[5], p[6], p[7]);

	if(type==0x05) /* 80386 */
	{
		gushort dx=WORD(p);
		/*
		 * 80386 have a different signature.
		 */
		printf("%sSignature: Type %u, Family %u, Major Stepping %u, Minor Stepping %u\n",
			prefix, dx>>12, (dx>>8)&0xF, (dx>>4)&0xF, dx&0xF);
		return;
	}
	if(type==0x06) /* 80486 */
	{
		gushort dx=WORD(p);
		/*
		 * Not all 80486 CPU support the CPUID instruction, we have to find
		 * wether the one we have here does or not. Note that this trick
		 * works only because we know that 80486 must be little-endian.
		 */
		if((dx&0x0F00)==0x0400 && ((dx&0x00F0)==0x0040 || (dx&0x00F0)>=0x0070))
			cpuid=1;
	}
	else if((type>=0x0B && type<=0x13) /* Intel, Cyrix */
	|| (type>=0x18 && type<=0x1D) || type==0x1F /* AMD */
	|| (type>=0xB0 && type<=0xB3) /* Intel */
	|| (type>=0xB5 && type<=0xB7)) /* Intel, AMD */
		cpuid=1;
	else if(type==0x01)
	{
		/*
		 * Some X86-class CPU have family "Other". In this case, we use
		 * the version string to determine if they are known to support the
		 * CPUID instruction.
		 */
		if(strcmp(version, "AMD Athlon(TM) Processor")==0)
			cpuid=1;
		else
			return;
	}
	else /* not X86-class */
		return;
	
	eax=DWORD(p);
	printf("%sSignature: Type %u, Family %u, Model %u, Stepping %u\n",
		prefix, (eax>>12)&0x3, ((eax>>16)&0xFF0)+((eax>>8)&0x00F),
		((eax>>12)&0xF0)+((eax>>4)&0x0F), eax&0xF);
	if(cpuid)
	{
		guint32 edx=DWORD(p+4);
		
		printf("%sFlags:", prefix);
		if((edx&0x3FF7FDFF)==0)
			printf(" None\n");
		else
		{
			int i;
			
			printf("\n");
			for(i=0; i<=31; i++)
				if(flags[i]!=NULL && edx&(1<<i))
					printf("%s\t%s\n", prefix, flags[i]);
		}
	}
}

static void
dmi_processor_voltage(xmlrpc_env *env,
				  xmlrpc_value *array,
				  guchar code)
{
	/* 3.3.5.4 */
	static const char *voltage[]={
		"5.0 V", /* 0 */
		"3.3 V",
		"2.9 V" /* 2 */
	};
	int i;
	int len = 12;
	char buf[len];
	xmlrpc_value *value;

	if(code&0x80) {
		snprintf (buf, len, "%.1f V", (float)(code&0x7f)/10);
		value = xmlrpc_build_value (env, "s", buf);
		xmlrpc_array_append_item (env, array, value);
		xmlrpc_DECREF (value);

		return;
	}

	for(i=0; i<=2; i++)
		if(code&(1<<i)) {
			value = xmlrpc_build_value (env, "s", voltage[i]);
			xmlrpc_array_append_item (env, array, value);
			xmlrpc_DECREF (value);
		}
}

static void
dmi_processor_frequency(char *buf, size_t len, gushort code)
{
	if(code)
		snprintf(buf, len, "%u MHz", code);
	else
		snprintf(buf, len, "Unknown");
}

static const char *dmi_processor_status(guchar code)
{
	static const char *status[]={
		"Unknown", /* 0x00 */
		"Enabled",
		"Disabled By User",
		"Disabled By BIOS",
		"Idle", /* 0x04 */
		"Other" /* 0x07 */
	};
	
	if(code<=0x04)
		return status[code];
	if(code==0x07)
		return status[0x05];
	return out_of_spec;
}

static const char *dmi_processor_upgrade(guchar code)
{
	/* 3.3.5.5 */
	static const char *upgrade[]={
		"Other", /* 0x01 */
		"Unknown",
		"Daughter Board",
		"ZIF Socket",
		"Replaceable Piggy Back",
		"None",
		"LIF Socket",
		"Slot 1",
		"Slot 2",
		"370-pin Socket",
		"Slot A",
		"Slot M",
		"Socket 423",
		"Socket A (Socket 462)",
		"Socket 478" /* 0x0F */
	};
	
	if(code>=0x01 && code<=0x0F)
		return upgrade[code-0x01];
	return out_of_spec;
}

static void
dmi_processor_cache(char *buf, size_t len, gushort code, const char *level, gushort ver)
{
	if(code==0xFFFF)
	{
		if(ver>=0x0203)
			snprintf(buf, len, "Not Provided");
		else
			snprintf(buf, len, "No %s Cache", level);
	}
	else
		snprintf(buf, len, "0x%04X", code);
}

/*
 * 3.3.6 Memory Controller Information (Type 5)
 */

static const char *dmi_memory_controller_ed_method(guchar code)
{
	/* 3.3.6.1 */
	static const char *method[]={
		"Other", /* 0x01 */
		"Unknown",
		"None",
		"8-bit Parity",
		"32-bit ECC",
		"64-bit ECC",
		"128-bit ECC",
		"CRC" /* 0x08 */
	};
	
	if(code>=0x01 && code<=0x08)
		return(method[code-0x01]);
	return out_of_spec;
}

static void
dmi_memory_controller_ec_capabilities(xmlrpc_env *env,
							   xmlrpc_value *array,
							   guchar code)
{
	/* 3.3.6.2 */
	static const char *capabilities[]={
		"Other", /* 0 */
		"Unknown",
		"None",
		"Single-bit Error Correcting",
		"Double-bit Error Correcting",
		"Error Scrubbing" /* 5 */
	};
	int i;
	xmlrpc_value *value;
	
	if((code&0x3F)==0) {
		value = xmlrpc_build_value (env, "s", "None");
		xmlrpc_array_append_item (env, array, value);
		xmlrpc_DECREF (value);

		return;
	}

	for(i=0; i<=5; i++) {
		if(code&(1<<i)) {
			value = xmlrpc_build_value (env, "s", capabilities[i]);
			xmlrpc_array_append_item (env, array, value);
			xmlrpc_DECREF (value);
		}
	}
}

static const char* dmi_memory_controller_interleave(guchar code)
{
	/* 3.3.6.3 */
	static const char *interleave[]={
		"Other", /* 0x01 */
		"Unknown",
		"One-way Interleave",
		"Two-way Interleave",
		"Four-way Interleave",
		"Eight-way Interleave",
		"Sixteen-way Interleave" /* 0x07 */
	};
	
	if(code>=0x01 && code<=0x07)
		return(interleave[code-0x01]);
	return(out_of_spec);
}

static void
dmi_memory_controller_speeds (xmlrpc_env *env,
						xmlrpc_value *array,
						gushort code)
{
	/* 3.3.6.4 */
	const char *speeds[]={
		"Other", /* 0 */
		"Unknown",
		"70 ns",
		"60 ns",
		"50 ns" /* 4 */
	};
	int i;
	xmlrpc_value *value;

	if((code&0x001F)==0) {
		value = xmlrpc_build_value (env, "s", "None");
		xmlrpc_array_append_item (env, array, value);
		xmlrpc_DECREF (value);

		return;
	}

	for(i=0; i<=4; i++) {
		if(code&(1<<i)) {
			value = xmlrpc_build_value (env, "s", speeds[i]);
			xmlrpc_array_append_item (env, array, value);
			xmlrpc_DECREF (value);
		}
	}
}

static void
dmi_memory_controller_slots(xmlrpc_env *env,
					   xmlrpc_value *array,
					   guchar count,
					   guchar *p)
{
	int i;
	int len = 8;
	char buf[len];
	xmlrpc_value *value;

	for(i=0; i<count; i++) {
		snprintf(buf, len, "0x%04X", WORD(p+sizeof(gushort)*i));
		value = xmlrpc_build_value (env, "s", buf);
		xmlrpc_array_append_item (env, array, value);
		xmlrpc_DECREF (value);
	}
}

/*
 * 3.3.7 Memory Module Information (Type 6)
 */

static void
dmi_memory_module_types (xmlrpc_env *env,
					xmlrpc_value *array,
					gushort code)
{
	/* 3.3.7.1 */
	static const char *types[]={
		"Other", /* 0 */
		"Unknown",
		"Standard",
		"FPM",
		"EDO",
		"Parity",
		"EDD",
		"SIMM",
		"DIMM",
		"Burst EDO",
		"SDRAM" /* 10 */
	};
	xmlrpc_value *value;

	if((code&0x03FF)==0) {
		value = xmlrpc_build_value (env, "s", "None");
		xmlrpc_array_append_item (env, array, value);
		xmlrpc_DECREF (value);
	}
	else
	{
		int i;
		
		for(i=0; i<=10; i++) {
			if(code&(1<<i)) {
				value = xmlrpc_build_value (env, "s", types[i]);
				xmlrpc_array_append_item (env, array, value);
				xmlrpc_DECREF (value);
			}
		}
	}
}

static void
dmi_memory_module_connections(char *buf, size_t len, guchar code)
{
	if(code==0xFF)
		snprintf(buf, len, "None");
	else
	{
		if((code&0xF0)!=0xF0)
			snprintf(buf, len, "%u", code>>4);
		if((code&0x0F)!=0x0F)
			snprintf(buf, len, "%u", code&0x0F);
	}
}

static void
dmi_memory_module_speed (char *buf, size_t len, guchar code)
{
	if(code==0)
		snprintf (buf, len, "Unknown");
	else
		snprintf (buf, len, "%u ns", code);
}

static void
dmi_memory_module_size (char *buf, size_t len, guchar code)
{
	/* 3.3.7.2 */
	switch(code&0x7F)
	{
		case 0x7D:
			snprintf (buf, len, "Not Determinable");
			break;
		case 0x7E:
			snprintf (buf, len, "Disabled");
			break;
		case 0x7F:
			snprintf (buf, len, "Not Installed");
			break;
		default:
			snprintf (buf, len, "%u MB", 1<<(code&0x7F));
	}

	if(code&0x80)
		strcat (buf, " (Double-bank Connection)");
	else
		strcat (buf, " (Single-bank Connection)");
}

static void
dmi_memory_module_error (char *buf, size_t len, guchar code)
{
	if(code&(1<<2))
		snprintf(buf, len, "See Event Log");
	else
	{
		if((code&0x03)==0)
			snprintf(buf, len, "OK");
		if(code&(1<<0))
			snprintf(buf, len, "Uncorrectable Errors");
		if(code&(1<<1))
			snprintf(buf, len, "Correctable Errors");
	}
}

/*
 * 3.3.8 Cache Information (Type 7)
 */

static const char *dmi_cache_mode(guchar code)
{
	static const char *mode[]={
		"Write Through", /* 0x00 */
		"Write Back",
		"Varies With Memory Address",
		"Unknown" /* 0x03 */
	};
	
	return mode[code];
}

static const char *dmi_cache_location(guchar code)
{
	static const char *location[4]={
		"Internal", /* 0x00 */
		"External",
		NULL, /* 0x02 */
		"Unknown" /* 0x03 */
	};
	
	if(location[code]!=NULL)
		return location[code];
	return out_of_spec;
}

static void
dmi_cache_size (char *buf, size_t len, gushort code)
{
	if(code&0x8000)
		snprintf (buf, len, "%u KB", (code&0x7FFF)<<6);
	else
		snprintf (buf, len, "%u KB", code);
}

static void
dmi_cache_types (xmlrpc_env *env,
			  xmlrpc_value *array,
			  gushort code)
{
	/* 3.3.8.1 */
	static const char *types[]={
		"Other", /* 0 */
		"Unknown",
		"Non-burst",
		"Burst",
		"Pipeline Burst",
		"Synchronous",
		"Asynchronous" /* 6 */
	};
	xmlrpc_value *value;
	
	if((code&0x007F)==0) {
		value = xmlrpc_build_value (env, "s", "None");
		xmlrpc_array_append_item (env, array, value);
		xmlrpc_DECREF (value);
	} else {
		int i;
		
		for(i=0; i<=6; i++) {
			if(code&(1<<i)) {
				value = xmlrpc_build_value (env, "s", types[i]);
				xmlrpc_array_append_item (env, array, value);
				xmlrpc_DECREF (value);
			}
		}
	}
}

static const char *dmi_cache_ec_type(guchar code)
{
	/* 3.3.8.2 */
	static const char *type[]={
		"Other", /* 0x01 */
		"Unknown",
		"None",
		"Parity",
		"Single-bit ECC",
		"Multi-bit ECC" /* 0x06 */
	};
	
	if(code>=0x01 && code<=0x06)
		return type[code-0x01];
	return out_of_spec;
}

static const char *dmi_cache_type(guchar code)
{
	/* 3.3.8.3 */
	static const char *type[]={
		"Other", /* 0x01 */
		"Unknown",
		"Instruction",
		"Data",
		"Unified" /* 0x05 */
	};
	
	if(code>=0x01 && code<=0x05)
		return type[code-0x01];
	return out_of_spec;
}

static const char *dmi_cache_associativity(guchar code)
{
	/* 3.3.8.4 */
	static const char *type[]={
		"Other", /* 0x01 */
		"Unknown",
		"Direct Mapped",
		"2-way Set-associative",
		"4-way Set-associative",
		"Fully Associative",
		"8-way Set-associative",
		"16-way Set-associative" /* 0x08 */
	};
	
	if(code>=0x01 && code<=0x08)
		return type[code-0x01];
	return out_of_spec;
}

/*
 * 3.3.9 Port Connector Information (Type 8)
 */

static const char *dmi_port_connector_type(guchar code)
{
	/* 3.3.9.2 */
	static const char *type[]={
		"None", /* 0x00 */
		"Centronics",
		"Mini Centronics",
		"Proprietary",
		"DB-25 male",
		"DB-25 female",
		"DB-15 male",
		"DB-15 female",
		"DB-9 male",
		"DB-9 female",
		"RJ-11", 
		"RJ-45",
		"50 Pin MiniSCSI",
		"Mini DIN",
		"Micro DIN",
		"PS/2",
		"Infrared",
		"HP-HIL",
		"Access Bus (USB)",
		"SSA SCSI",
		"Circular DIN-8 male",
		"Circular DIN-8 female",
		"On Board IDE",
		"On Board Floppy",
		"9 Pin Dual Inline (pin 10 cut)",
		"25 Pin Dual Inline (pin 26 cut)",
		"50 Pin Dual Inline",
		"68 Pin Dual Inline",
		"On Board Sound Input From CD-ROM",
		"Mini Centronics Type-14",
		"Mini Centronics Type-26",
		"Mini Jack (headphones)",
		"BNC",
		"IEEE 1394" /* 0x21 */
	};
	static const char *type_0xA0[]={
		"PC-98", /* 0xA0 */
		"PC-98 Hireso",
		"PC-H98",
		"PC-98 Note",
		"PC-98 Full" /* 0xA4 */
	};
	
	if(code<=0x21)
		return type[code];
	if(code>=0xA0 && code<=0xA4)
		return type_0xA0[code-0xA0];
	if(code==0xFF)
		return "Other";
	return out_of_spec;
}

static const char *dmi_port_type(guchar code)
{
	/* 3.3.9.3 */
	static const char *type[]={
		"None", /* 0x00 */
		"Parallel Port XT/AT Compatible",
		"Parallel Port PS/2",
		"Parallel Port ECP",
		"Parallel Port EPP",
		"Parallel Port ECP/EPP",
		"Serial Port XT/AT Compatible",
		"Serial Port 16450 Compatible",
		"Serial Port 16550 Compatible",
		"Serial Port 16550A Compatible",
		"SCSI Port",
		"MIDI Port",
		"Joystick Port",
		"Keyboard Port",
		"Mouse Port",
		"SSA SCSI",
		"USB",
		"Firewire (IEEE P1394)",
		"PCMCIA Type I",
		"PCMCIA Type II",
		"PCMCIA Type III",
		"Cardbus",
		"Access Bus Port",
		"SCSI II",
		"SCSI Wide",
		"PC-98",
		"PC-98 Hireso",
		"PC-H98",
		"Video Port",
		"Audio Port",
		"Modem Port",
		"Network Port" /* 0x1F */
	};
	static const char *type_0xA0[]={
		"8251 Compatible", /* 0xA0 */
		"8251 FIFO Compatible" /* 0xA1 */
	};
	
	if(code<=0x1F)
		return type[code];
	if(code>=0xA0 && code<=0xA1)
		return type_0xA0[code-0xA0];
	if(code==0xFF)
		return "Other";
	return out_of_spec;
}

/*
 * 3.3.10 System Slots (Type 9)
 */

static const char *dmi_slot_type(guchar code)
{
	/* 3.3.10.1 */
	static const char *type[]={
		"Other", /* 0x01 */
		"Unknown",
		"ISA",
		"MCA",
		"EISA",
		"PCI",
		"PC Card (PCMCIA)",
		"VLB",
		"Proprietary",
		"Processor Card",
		"Proprietary Memory Card",
		"I/O Riser Card",
		"NuBus",
		"PCI-66",
		"AGP",
		"AGP 2x",
		"AGP 4x",
		"PCI-X" /* 0x12 */
	};
	static const char *type_0xA0[]={
		"PC-98/C20", /* 0xA0 */
		"PC-98/C24",
		"PC-98/E",
		"PC-98/Local Bus",
		"PC-98/Card" /* 0xA4 */
	};
	
	if(code>=0x01 && code<=0x12)
		return type[code-0x01];
	if(code>=0xA0 && code<=0xA4)
		return type_0xA0[code-0xA0];
	return out_of_spec;
}

static const char *dmi_slot_bus_width(guchar code)
{
	/* 3.3.10.2 */
	static const char *width[]={
		"", /* 0x01, "Other" */
		"", /* "Unknown" */
		"8-bit ",
		"16-bit ",
		"32-bit ",
		"64-bit ",
		"128-bit " /* 0x07 */
	};
	
	if(code>=0x01 && code<=0x07)
		return width[code-0x01];
	return out_of_spec;
}

static const char *dmi_slot_current_usage(guchar code)
{
	/* 3.3.10.3 */
	static const char *usage[]={
		"Other", /* 0x01 */
		"Unknown",
		"Available",
		"In Use" /* 0x04 */
	};
	
	if(code>=0x01 && code<=0x04)
		return usage[code-0x01];
	return out_of_spec;
}

static const char *dmi_slot_length(guchar code)
{
	/* 3.3.1O.4 */
	static const char *length[]={
		"Other", /* 0x01 */
		"Unknown",
		"Short",
		"Long" /* 0x04 */
	};
	
	if(code>=0x01 && code<=0x04)
		return length[code-0x01];
	return out_of_spec;
}

static gboolean
dmi_slot_id (char *buf, size_t len, guchar code1, guchar code2, guchar type)
{
	/* 3.3.10.5 */
	switch(type)
	{
		case 0x04: /* MCA */
		case 0x05: /* EISA */
		case 0x06: /* PCI */
		case 0x0E: /* PCI */
		case 0x0F: /* AGP */
		case 0x10: /* AGP */
		case 0x11: /* AGP */
		case 0x12: /* PCI */
			snprintf (buf, len, "%u", code1);
			break;
		case 0x07: /* PCMCIA */
			snprintf (buf, len, "Adapter %u, Socket %u", code1, code2);
			break;
	default:
		return FALSE;
	}

	return TRUE;
}

static void
dmi_slot_characteristics (xmlrpc_env *env,
					 xmlrpc_value *array,
					 guchar code1,
					 guchar code2)
{
	/* 3.3.10.6 */
	static const char *characteristics1[]={
		"5.0 V is provided", /* 1 */
		"3.3 V is provided",
		"Opening is shared",
		"PC Card-16 is supported",
		"Cardbus is supported",
		"Zoom Video is supported",
		"Modem ring resume is supported" /* 7 */
	};
	/* 3.3.10.7 */
	static const char *characteristics2[]={
		"PME signal is supported", /* 0 */
		"Hot-plug devices are supported",
		"SMBus signal is supported" /* 2 */
	};
	xmlrpc_value *value;
	
	if(code1&(1<<0)) {
		value = xmlrpc_build_value (env, "s", "Unknown");
		xmlrpc_array_append_item (env, array, value);
		xmlrpc_DECREF (value);
	} else if((code1&0x7F)==0 && (code2&0x07)==0) {
		value = xmlrpc_build_value (env, "s", "None");
		xmlrpc_array_append_item (env, array, value);
		xmlrpc_DECREF (value);
	} else {
		int i;

		for(i=1; i<=7; i++)
			if(code1&(1<<i)) {
				value = xmlrpc_build_value (env, "s", characteristics1[i-1]);
				xmlrpc_array_append_item (env, array, value);
				xmlrpc_DECREF (value);
			}

		for(i=0; i<=2; i++)
			if(code2&(1<<i)) {
				value = xmlrpc_build_value (env, "s", characteristics2[i-1]);
				xmlrpc_array_append_item (env, array, value);
				xmlrpc_DECREF (value);
			}
	}
}

/*
 * 3.3.11 On Board Devices Information (Type 10)
 */

static const char *dmi_on_board_devices_type(guchar code)
{
	/* 3.3.11.1 */
	static const char *type[]={
		"Other", /* 0x01 */
		"Unknown",
		"Video",
		"SCSI Controller",
		"Ethernet",
		"Token Ring",
		"Sound" /* 0x07 */
	};
	
	if(code>=0x01 && code <=0x07)
		return type[code-0x01];
	return out_of_spec;
}

static void
dmi_on_board_devices (xmlrpc_env *env,
				  xmlrpc_value *array,
				  struct dmi_header *h)
{
	guchar *p=(guchar *)h+4;
	guchar count=(h->length-0x04)/2;
	int i;
	xmlrpc_value *value;

	for(i=0; i<count; i++)
	{
		value = xmlrpc_struct_new (env);
		xmlrpc_array_append_item (env, array, value);
		xmlrpc_DECREF (value);

		RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Type",
								dmi_on_board_devices_type(p[2*i]&0x7F));
		RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Status",
								p[2*i]&0x80 ? "Enabled" : "Disabled");
		RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Description",
								dmi_string(h, p[2*i+1]));
	}
cleanup:
	return;
}

/*
 * 3.3.12 OEM Strings (Type 11)
 */

static void
dmi_oem_strings (xmlrpc_env *env,
			  xmlrpc_value *array,
			  struct dmi_header *h)
{
	guchar *p=(guchar *)h+4;
	guchar count=p[0x00];
	int i;
	xmlrpc_value *value;

	for(i=1; i<=count; i++) {
		value = xmlrpc_build_value (env, "s", dmi_string(h, i));
		xmlrpc_array_append_item (env, array, value);
		xmlrpc_DECREF (value);
	}
}

/*
 * 3.3.13 System Configuration Options (Type 12)
 */

static void
dmi_system_configuration_options (xmlrpc_env *env,
						    xmlrpc_value *array,
						    struct dmi_header *h)
{
	guchar *p=(guchar *)h+4;
	guchar count=p[0x00];
	int i;
	xmlrpc_value *value;

	for(i=1; i<=count; i++) {
		value = xmlrpc_build_value (env, "s", dmi_string(h, i));
		xmlrpc_array_append_item (env, array, value);
		xmlrpc_DECREF (value);
	}
}

/*
 * 3.3.14 BIOS Language Information (Type 13)
 */

static void
dmi_bios_languages (xmlrpc_env *env,
				xmlrpc_value *array,
				struct dmi_header *h)
{
	guchar *p=(guchar *)h+4;
	guchar count=p[0x00];
	int i;
	xmlrpc_value *value;
	
	for(i=1; i<=count; i++) {
		value = xmlrpc_build_value (env, "s", dmi_string(h, i));
		xmlrpc_array_append_item (env, array, value);
		xmlrpc_DECREF (value);
	}
}

/*
 * 3.3.15 Group Associations (Type 14)
 */

static void
dmi_group_associations_items (xmlrpc_env *env,
						xmlrpc_value *array,
						guchar count,
						guchar *p)
{
	int i;
	int len = BUF_SIZE;
	char buf[len];
	xmlrpc_value *value;

	for(i=0; i<count; i++) {
		snprintf (buf, len, "0x%04X (%s)",
				WORD(p+3*i+1),
				dmi_smbios_structure_type(p[3*i]));
		value = xmlrpc_build_value (env, "s", buf);
		xmlrpc_array_append_item (env, array, value);
		xmlrpc_DECREF (value);
	}
}

/*
 * 3.3.16 System Event Log (Type 15)
 */

static const char *dmi_event_log_method(guchar code)
{
	static const char *method[]={
		"Indexed I/O, one 8-bit index port, one 8-bit data port", /* 0x00 */
		"Indexed I/O, two 8-bit index ports, one 8-bit data port",
		"Indexed I/O, one 16-bit index port, one 8-bit data port",
		"Memory-mapped physical 32-bit address",
		"General-pupose non-volatile data functions" /* 0x04 */
	};
	
	if(code<=0x04)
		return method[code];
	if(code>=0x80)
		return "OEM-specific";
	return out_of_spec;
}

static void
dmi_event_log_status (char *buf, size_t len, guchar code)
{
	static const char *valid[]={
		"Invalid", /* 0 */
		"Valid" /* 1 */
	};
	static const char *full[]={
		"Not Full", /* 0 */
		"Full" /* 1 */
	};
	
	snprintf (buf, len, " %s, %s",
			valid[code&(1<<0)], full[code&(1<<1)]);
}

static void
dmi_event_log_address (char *buf, size_t len, guchar method, guchar *p)
{
	/* 3.3.16.3 */
	switch(method)
	{
		case 0x00:
		case 0x01:
		case 0x02:
			snprintf (buf, len, "Index 0x%04X, Data 0x%04X", WORD(p), WORD(p+2));
			break;
		case 0x03:
			snprintf (buf, len, "0x%08X", DWORD(p));
			break;
		case 0x04:
			snprintf (buf, len, "0x%04X", WORD(p));
			break;
		default:
			snprintf(buf, len, "Unknown");
	}
}

static const char *dmi_event_log_header_type(guchar code)
{
	static const char *type[]={
		"No Header", /* 0x00 */
		"Type 1" /* 0x01 */
	};
	
	if(code<=0x01)
		return type[code];
	if(code>=0x80)
		return "OEM-specific";
	return out_of_spec;
}

static const char *dmi_event_log_descriptor_type(guchar code)
{
	/* 3.3.16.6.1 */
	static const char *type[]={
		NULL, /* 0x00 */
		"Single-bit ECC memory error",
		"Multi-bit ECC memory error",
		"Parity memory error",
		"Bus timeout",
		"I/O channel block",
		"Software NMI",
		"POST memory resize",
		"POST error",
		"PCI parity error",
		"PCI system error",
		"CPU failure",
		"EISA failsafe timer timeout",
		"Correctable memory log disabled",
		"Logging disabled",
		NULL, /* 0x0F */
		"System limit exceeded",
		"Asynchronous hardware timer expired",
		"System configuration information",
		"Hard disk information",
		"System reconfigured",
		"Uncorrectable CPU-complex error",
		"Log area reset/cleared",
		"System boot" /* 0x17 */
	};
	
	if(code<=0x17 && type[code]!=NULL)
		return type[code];
	if(code>=0x80 && code<=0xFE)
		return "OEM-specific";
	if(code==0xFF)
		return "End of log";
	return out_of_spec;
}

static const char *dmi_event_log_descriptor_format(guchar code)
{
	/* 3.3.16.6.2 */
	static const char *format[]={
		"None", /* 0x00 */
		"Handle",
		"Multiple-event",
		"Multiple-event handle",
		"POST results bitmap",
		"System management",
		"Multiple-event system management" /* 0x06 */
	};
	
	if(code<=0x06)
		return format[code];
	if(code>=0x80)
		return "OEM-specific";
	return out_of_spec;
}

static void
dmi_event_log_descriptors (xmlrpc_env *env,
					  xmlrpc_value *array,
					  guchar count,
					  guchar len,
					  guchar *p)
{
	/* 3.3.16.1, , 3.3.16.6.2 */
	int i;
	char buf[BUF_SIZE];
	xmlrpc_value *value;
	
	for(i=0; i<count; i++)
	{
		if(len>=0x02)
		{
			value = xmlrpc_struct_new (env);
			snprintf (buf, BUF_SIZE, "%u: %s",
					i+1, dmi_event_log_descriptor_type(p[i*len]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Descriptor", buf);

			snprintf (buf, BUF_SIZE, "%u: %s",
					i+1, dmi_event_log_descriptor_format(p[i*len+1]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Data Format", buf);
		}
	}
cleanup:
	return;
}

/*
 * 3.3.17 Physical Memory Array (Type 16)
 */

static const char *dmi_memory_array_location(guchar code)
{
	/* 3.3.17.1 */
	static const char *location[]={
		"Other", /* 0x01 */
		"Unknown",
		"System Board Or Motherboard",
		"ISA Add-on Card",
		"EISA Add-on Card",
		"PCI Add-on Card",
		"MCA Add-on Card",
		"PCMCIA Add-on Card",
		"Proprietary Add-on Card",
		"NuBus" /* 0x0A, master.mif says 16 */
	};
	static const char *location_0xA0[]={
		"PC-98/C20 Add-on Card", /* 0xA0 */
		"PC-98/C24 Add-on Card",
		"PC-98/E Add-on Card",
		"PC-98/Local Bus Add-on Card",
		"PC-98/Card Slot Add-on Card" /* 0xA4, from master.mif */
	};
	
	if(code>=0x01 && code<=0x0A)
		return location[code-0x01];
	if(code>=0xA0 && code<=0xA4)
		return location_0xA0[code-0xA0];
	return out_of_spec;
}

static const char *dmi_memory_array_use(guchar code)
{
	/* 3.3.17.2 */
	static const char *use[]={
		"Other", /* 0x01 */
		"Unknown",
		"System Memory",
		"Video Memory",
		"Flash Memory",
		"Non-volatile RAM",
		"Cache Memory" /* 0x07 */
	};
	
	if(code>=0x01 && code<=0x07)
		return use[code-0x01];
	return out_of_spec;
}

static const char *dmi_memory_array_ec_type(guchar code)
{
	/* 3.3.17.3 */
	static const char *type[]={
		"Other", /* 0x01 */
		"Unknown",
		"None",
		"Parity",
		"Single-bit ECC",
		"Multi-bit ECC",
		"CRC" /* 0x07 */
	};
	
	if(code>=0x01 && code<=0x07)
		return type[code-0x01];
	return out_of_spec;
}

static void
dmi_memory_array_capacity (char *buf, size_t len, guint32 code)
{
	if(code==0x8000000)
		snprintf(buf, len, "Unknown");
	else
	{
		if((code&0x000FFFFF)==0)
			snprintf (buf, len, "%u GB", code>>20);
		else if((code&0x000003FF)==0)
			snprintf (buf, len, "%u MB", code>>10);
		else
			snprintf (buf, len, "%u kB", code);
	}
}

static void
dmi_memory_array_error_handle (char *buf, size_t len, gushort code)
{
	if(code==0xFFFE)
		snprintf (buf, len, "Not Provided");
	else if(code==0xFFFF)
		snprintf (buf, len, "No Error");
	else
		snprintf (buf, len, "0x%04X", code);
}

/*
 * 3.3.18 Memory Device (Type 17)
 */

static void
dmi_memory_device_width (char *buf, size_t len, gushort code)
{
	/*
	 * If no memory module is present, width may be 0
	 */
	if(code==0xFFFF || code==0)
		snprintf (buf, len, "Unknown");
	else
		snprintf (buf, len, "%u bits", code);
}

static void
dmi_memory_device_size (char *buf, size_t len, gushort code)
{
	if(code==0)
		snprintf (buf, len, "No Module Installed");
	else if(code==0xFFFF)
		snprintf (buf, len, "Unknown");
	else
	{
		if(code&0x8000)
			snprintf (buf, len, "%u kB", code&0x7FFF);
		else
			snprintf (buf, len, "%u MB", code);
	}
}

static const char *dmi_memory_device_form_factor(guchar code)
{
	/* 3.3.18.1 */
	static const char *form_factor[]={
		"Other", /* 0x01 */
		"Unknown",
		"SIMM",
		"SIP",
		"Chip",
		"DIP",
		"ZIP",
		"Proprietary Card",
		"DIMM",
		"TSOP",
		"Row Of Chips",
		"RIMM",
		"SODIMM",
		"SRIMM" /* 0x0E */
	};
	
	if(code>=0x01 && code<=0x0E)
		return form_factor[code-0x01];
	return out_of_spec;
}

static void
dmi_memory_device_set (char *buf, size_t len, guchar code)
{
	if(code==0)
		snprintf (buf, len, "None");
	else if(code==0xFF)
		snprintf (buf, len, "Unknown");
	else
		snprintf (buf, len, "%u", code);
}

static const char *dmi_memory_device_type(guchar code)
{
	/* 3.3.18.2 */
	static const char *type[]={
		"Other", /* 0x01 */
		"Unknown",
		"DRAM",
		"EDRAM",
		"VRAM",
		"SRAM",
		"RAM",
		"ROM",
		"Flash",
		"EEPROM",
		"FEPROM",
		"EPROM",
		"CDRAM",
		"3DRAM",
		"SDRAM",
		"SGRAM",
		"RDRAM",
		"DDR" /* 0x12 */
	};
	
	if(code>=0x01 && code<=0x12)
		return type[code-0x01];
	return out_of_spec;
}

static void
dmi_memory_device_type_detail (xmlrpc_env *env,
						 xmlrpc_value *array,
						 gushort code)
{
	/* 3.3.18.3 */
	static const char *detail[]={
		"Other", /* 1 */
		"Unknown",
		"Fast-paged",
		"Static Column",
		"Pseudo-static",
		"RAMBus",
		"Synchronous",
		"CMOS",
		"EDO",
		"Window DRAM",
		"Cache DRAM",
		"Non-Volatile" /* 12 */
	};
	xmlrpc_value *value;
	
	if((code&0x1FFE)==0) {
		value = xmlrpc_build_value (env, "s", "None");
		xmlrpc_array_append_item (env, array, value);
		xmlrpc_DECREF (value);
	} else {
		int i;
		
		for(i=1; i<=12; i++)
			if(code&(1<<i)) {
				value = xmlrpc_build_value (env, "s", detail[i-1]);
				xmlrpc_array_append_item (env, array, value);
				xmlrpc_DECREF (value);
			}
	}
}

static void
dmi_memory_device_speed (char *buf, size_t len, gushort code)
{
	if(code==0)
		snprintf (buf, len, "Unknown");
	else
		snprintf (buf, len, "%u MHz (%.1f ns)", code, (float)1000/code);
}

/*
 * 3.3.19 32-bit Memory Error Information (Type 18)
 */

static const char *dmi_memory_error_type(guchar code)
{
	/* 3.3.19.1 */
	static const char *type[]={
		"Other", /* 0x01 */
		"Unknown",
		"OK"
		"Bad Read",
		"Parity Error",
		"Single-bit Error",
		"Double-bit Error",
		"Multi-bit Error",
		"Nibble Error",
		"Checksum Error",
		"CRC Error",
		"Corrected Single-bit Error",
		"Corrected Error",
		"Uncorrectable Error" /* 0x0E */
	};
	
	if(code>=0x01 && code<=0x0E)
		return type[code-0x01];
	return out_of_spec;
}

static const char *dmi_memory_error_granularity(guchar code)
{
	/* 3.3.19.2 */
	static const char *granularity[]={
		"Other", /* 0x01 */
		"Unknown",
		"Device Level",
		"Memory Partition Level" /* 0x04 */
	};
	
	if(code>=0x01 && code<=0x04)
		return granularity[code-0x01];
	return out_of_spec;
}

static const char *dmi_memory_error_operation(guchar code)
{
	/* 3.3.19.3 */
	static const char *operation[]={
		"Other", /* 0x01 */
		"Unknown",
		"Read",
		"Write",
		"Partial Write" /* 0x05 */
	};
	
	if(code>=0x01 && code<=0x05)
		return operation[code-0x01];
	return out_of_spec;
}

static void
dmi_memory_error_syndrome (char *buf, size_t len, guint32 code)
{
	if(code==0x00000000)
		snprintf (buf, len, "Unknown");
	else
		snprintf (buf, len, "0x%08X", code);
}

static void
dmi_32bit_memory_error_address(char *buf, size_t len, guint32 code)
{
	if(code==0x80000000)
		snprintf (buf, len, "Unknown");
	else
		snprintf (buf, len, "0x%08X", code);
}

/*
 * 3.3.20 Memory Array Mapped Address (Type 19)
 */

static void
dmi_mapped_address_size (char *buf, size_t len, guint32 code)
{
	if(code==0)
		snprintf (buf, len, "Invalid");
	else if((code&0x000FFFFF)==0)
		snprintf (buf, len, "%u GB", code>>20);
	else if((code&0x000003FF)==0)
		snprintf (buf, len, "%u MB", code>>10);
	else
		snprintf (buf, len, "%u kB", code);
}

/*
 * 3.3.21 Memory Device Mapped Address (Type 20)
 */

static void
dmi_mapped_address_row_position (char *buf, size_t len, guchar code)
{
	if(code==0)
		snprintf (buf, len, "%s", out_of_spec);
	else if(code==0xFF)
		snprintf (buf, len, "Unknown");
	else
		snprintf (buf, len, "%u", code);
}

static gboolean
dmi_mapped_address_interleave_position (char *buf, size_t len, guchar code)
{
	if(code!=0)
	{
		if(code==0xFF)
			snprintf (buf, len, "Unknown");
		else
			snprintf (buf, len, "%u", code);
		return TRUE;
	}

	return FALSE;
}

static gboolean
dmi_mapped_address_interleaved_data_depth (char *buf, size_t len, guchar code)
{
	if(code!=0)
	{
		if(code==0xFF)
			snprintf (buf, len, "Unknown");
		else
			snprintf (buf, len, "%u", code);

		return TRUE;
	}

	return FALSE;
}

/*
 * 3.3.22 Built-in Pointing Device (Type 21)
 */

static const char *dmi_pointing_device_type(guchar code)
{
	/* 3.3.22.1 */
	static const char *type[]={
		"Other", /* 0x01 */
		"Unknown",
		"Mouse",
		"Track Ball",
		"Track Point",
		"Glide Point",
		"Touch Pad",
		"Touch Screen",
		"Optical Sensor" /* 0x09 */
	};
	
	if(code>=0x01 && code<=0x09)
		return type[code-0x01];
	return out_of_spec;
}

static const char *dmi_pointing_device_interface(guchar code)
{
	/* 3.3.22.2 */
	static const char *interface[]={
		"Other", /* 0x01 */
		"Unknown",
		"Serial",
		"PS/2",
		"Infrared",
		"HIP-HIL",
		"Bus Mouse",
		"ADB (Apple Desktop Bus)" /* 0x08 */
	};
	static const char *interface_0xA0[]={
		"Bus Mouse DB-9", /* 0xA0 */
		"Bus Mouse Micro DIN",
		"USB" /* 0xA2 */
	};
	
	if(code>=0x01 && code<=0x08)
		return interface[code-0x01];
	if(code>=0xA0 && code<=0xA2)
		return interface_0xA0[code-0xA0];
	return out_of_spec;
}

/*
 * 3.3.23 Portable Battery (Type 22)
 */
 
static const char *dmi_battery_chemistry(guchar code)
{
	/* 3.3.23.1 */
	static const char *chemistry[]={
		"Other", /* 0x01 */
		"Unknown",
		"Lead Acid",
		"Nickel Cadmium",
		"Nickel Metal Hydride",
		"Lithium Ion",
		"Zinc Air",
		"Lithium Polymer" /* 0x08 */
	};

	if(code>=0x01 && code<=0x08)
		return chemistry[code-0x01];
	return out_of_spec;
}

static void
dmi_battery_capacity (char *buf, size_t len, gushort code, guchar multiplier)
{
	if(code==0)
		snprintf (buf, len, "Unknown");
	else
		snprintf (buf, len, "%u mWh", code*multiplier);
}

static void
dmi_battery_voltage (char *buf, size_t len, gushort code)
{
	if(code==0)
		snprintf (buf, len, "Unknown");
	else
		snprintf (buf, len, "%u mV", code);
}

static void
dmi_battery_maximum_error (char *buf, size_t len, guchar code)
{
	if(code==0xFF)
		snprintf (buf, len, "Unknown");
	else
		snprintf (buf, len, "%u%%", code);
}

/*
 * 3.3.24 System Reset (Type 23)
 */

static const char *dmi_system_reset_boot_option(guchar code)
{
	static const char *option[]={
		"Operating System", /* 0x1 */
		"System Utilities",
		"Do Not Reboot" /* 0x3 */
	};
	
	if(code>=0x1)
		return option[code-0x1];
	return out_of_spec;
}

static void
dmi_system_reset_count (char *buf, size_t len, gushort code)
{
	if(code==0xFFFF)
		snprintf (buf, len, "Unknown");
	else
		snprintf (buf, len, "%u", code);
}

static void
dmi_system_reset_timer (char *buf, size_t len, gushort code)
{
	if(code==0xFFFF)
		snprintf (buf, len, "Unknown");
	else
		snprintf (buf, len, "%u min", code);
}

/*
 * 3.3.25 Hardware Security (Type 24)
 */

static const char *dmi_hardware_security_status(guchar code)
{
	static const char *status[]={
		"Disabled", /* 0x00 */
		"Enabled",
		"Not Implemented",
		"Unknown" /* 0x03 */
	};
	
	return status[code];
}

/*
 * 3.3.26 System Power Controls (Type 25)
 */

static void
dmi_power_controls_power_on (char *buf, size_t len, guchar *p)
{
	int h_len = 6;
	char helper[h_len];

	/* 3.3.26.1 */
	if(dmi_bcd_range(p[0], 0x01, 0x12))
		snprintf (buf, len, "%02X", p[0]);
	else
		snprintf (buf, len, "*");
	if(dmi_bcd_range(p[1], 0x01, 0x31)) {
		snprintf (helper, h_len, "-%02X", p[1]);
		strcat (buf, helper);
	} else
		strcpy (buf, "-*");
	if(dmi_bcd_range(p[2], 0x00, 0x23)) {
		snprintf (helper, h_len, " %02X", p[2]);
		strcat (buf, helper);
	} else
		strcpy (buf, " *");
	if(dmi_bcd_range(p[3], 0x00, 0x59)) {
		snprintf (helper, h_len, ":%02X", p[3]);
		strcat (buf, helper);
	} else
		strcpy (buf, ":*");
	if(dmi_bcd_range(p[4], 0x00, 0x59)) {
		snprintf (helper, h_len, ":%02X", p[4]);
		strcat (buf, helper);
	} else
		strcpy (buf, ":*");
}

/*
 * 3.3.27 Voltage Probe (Type 26)
 */

static const char *dmi_voltage_probe_location(guchar code)
{
	/* 3.3.27.1 */
	static const char *location[]={
		"Other", /* 0x01 */
		"Unknown",
		"Processor",
		"Disk",
		"Peripheral Bay",
		"System Management Module",
		"Motherboard",
		"Memory Module",
		"Processor Module",
		"Power Unit",
		"Add-in Card" /* 0x0B */
	};
	
	if(code>=0x01 && code<=0x0B)
		return location[code-0x01];
	return out_of_spec;
}

static const char *dmi_probe_status(guchar code)
{
	/* 3.3.27.1 */
	static const char *status[]={
		"Other", /* 0x01 */
		"Unknown",
		"OK",
		"Non-critical",
		"Critical",
		"Non-recoverable" /* 0x06 */
	};
	
	if(code>=0x01 && code<=0x06)
		return status[code-0x01];
	return out_of_spec;
}

static void
dmi_voltage_probe_value (char *buf, size_t len, gushort code)
{
	if(code==0x8000)
		snprintf (buf, len, "Unknown");
	else
		snprintf (buf, len, "%.3f V", (float)(gint16)code/1000);
}

static void
dmi_voltage_probe_resolution (char *buf, size_t len, gushort code)
{
	if(code==0x8000)
		snprintf (buf, len, "Unknown");
	else
		snprintf (buf, len, "%.1f mV", (float)code/10);
}

static void
dmi_probe_accuracy (char *buf, size_t len, gushort code)
{
	if(code==0x8000)
		snprintf (buf, len, "Unknown");
	else
		snprintf (buf, len, "%.2f%%", (float)code/100);
}

/*
 * 3.3.28 Cooling Device (Type 27)
 */

static const char *dmi_cooling_device_type(guchar code)
{
	/* 3.3.28.1 */
	static const char *type[]={
		"Other", /* 0x01 */
		"Unknown",
		"Fan",
		"Centrifugal Blower",
		"Chip Fan",
		"Cabinet Fan",
		"Power Supply Fan",
		"Heat Pipe",
		"Integrated Refrigeration" /* 0x09 */
	};
	static const char *type_0x10[]={
		"Active Cooling", /* 0x10, master.mif says 32 */
		"Passive Cooling" /* 0x11, master.mif says 33 */
	};
	
	if(code>=0x01 && code<=0x09)
		return type[code-0x01];
	if(code>=0x10 && code<=0x11)
		return type_0x10[code-0x10];
	return out_of_spec;
}

static void
dmi_cooling_device_speed (char *buf, size_t len, gushort code)
{
	if(code==0x8000)
		snprintf (buf, len, "Unknown Or Non-rotating");
	else
		snprintf (buf, len, "%u rpm", code);
}

/*
 * 3.3.29 Temperature Probe (Type 28)
 */

static const char *dmi_temperature_probe_location(guchar code)
{
	/* 3.3.29.1 */
	static const char *location[]={
		"Other", /* 0x01 */
		"Unknown",
		"Processor",
		"Disk",
		"Peripheral Bay",
		"System Management Module", /* master.mif says SMB MAster */
		"Motherboard",
		"Memory Module",
		"Processor Module",
		"Power Unit",
		"Add-in Card",
		"Front Panel Board",
		"Back Panel Board",
		"Power System Board",
		"Drive Back Plane" /* 0x0F */
	};
	
	if(code>=0x01 && code<=0x0F)
		return location[code-0x01];
	return out_of_spec;
}

static void
dmi_temperature_probe_value (char *buf, size_t len, gushort code)
{
	if(code==0x8000)
		snprintf (buf, len, "Unknown");
	else
		snprintf (buf, len, "%.1f deg C", (float)(gint16)code/10);
}

static void
dmi_temperature_probe_resolution (char *buf, size_t len, gushort code)
{
	if(code==0x8000)
		snprintf (buf, len, "Unknown");
	else
		snprintf (buf, len, "%.3f deg C", (float)code/1000);
}

/*
 * 3.3.30 Electrical Current Probe (Type 29)
 */

static void
dmi_current_probe_value (char *buf, size_t len, gushort code)
{
	if(code==0x8000)
		snprintf (buf, len, "Unknown");
	else
		snprintf (buf, len, "%.3f A", (float)(gint16)code/1000);
}

static void
dmi_current_probe_resolution (char *buf, size_t len, gushort code)
{
	if(code==0x8000)
		snprintf (buf, len, "Unknown");
	else
		snprintf (buf, len, "%.1f mA", (float)code/10);
}

/*
 * 3.3.33 System Boot Information (Type 32)
 */

static const char *dmi_system_boot_status(guchar code)
{
	static const char *status[]={
		"No errors detected", /* 0 */
		"No bootable media",
		"Operating system failed to load",
		"Firmware-detected hardware failure",
		"Operating system-detected hardware failure",
		"User-requested boot",
		"System security violation",
		"Previously-requested image",
		"System watchdog timer expired" /* 8 */
	};
	
	if(code<=8)
		return status[code];
	if(code>=128 && code<=191)
		return "OEM-specific";
	if(code>=192 && code<=255)
		return "Product-specific";
	return out_of_spec;
}

/*
 * 3.3.34 64-bit Memory Error Information (Type 33)
 */

static void
dmi_64bit_memory_error_address (char *buf, size_t len, u64 code)
{
	if(code.h==0x80000000 && code.l==0x00000000)
		snprintf (buf, len, "Unknown");
	else
		snprintf (buf, len, "0x%08X%08X", code.h, code.l);
}

/*
 * 3.3.35 Management Device (Type 34)
 */

static const char *dmi_management_device_type(guchar code)
{
	/* 3.3.35.1 */
	static const char *type[]={
		"Other", /* 0x01 */
		"Unknown",
		"LM75",
		"LM78",
		"LM79",
		"LM80",
		"LM81",
		"ADM9240",
		"DS1780",
		"MAX1617",
		"GL518SM",
		"W83781D",
		"HT82H791" /* 0x0D */
	};
	
	if(code>=0x01 && code<=0x0D)
		return type[code-0x01];
	return out_of_spec;
}

static const char *dmi_management_device_address_type(guchar code)
{
	/* 3.3.35.2 */
	static const char *type[]={
		"Other", /* 0x01 */
		"Unknown",
		"I/O Port",
		"Memory",
		"SMBus" /* 0x05 */
	};
	
	if(code>=0x01 && code<=0x05)
		return type[code-0x01];
	return out_of_spec;
}

/*
 * 3.3.38 Memory Channel (Type 37)
 */

static const char *dmi_memory_channel_type(guchar code)
{
	/* 3.3.38.1 */
	static const char *type[]={
		"Other", /* 0x01 */
		"Unknown",
		"RAMBus",
		"Synclink" /* 0x04 */
	};
	
	if(code>=0x01 && code<=0x04)
		return type[code-0x01];
	return out_of_spec;
}

static void
dmi_memory_channel_devices (xmlrpc_env *env,
					   xmlrpc_value *array,
					   guchar count,
					   guchar *p)
{
	int i;
	int len = BUF_SIZE;
	char buf[len];
	xmlrpc_value *value;
	
	for(i=1; i<=count; i++)
	{
		value = xmlrpc_struct_new (env);
		xmlrpc_array_append_item (env, array, value);
		xmlrpc_DECREF (value);

		snprintf (buf, len, "%u", p[3*i]);
		RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Load", buf);

		snprintf (buf, len, "%u", WORD(p+3*i+1));
		RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Handle", buf);
	}
cleanup:
	return;
}

/*
 * 3.3.39 IPMI Device Information (Type 38)
 */

static const char *dmi_ipmi_interface_type(guchar code)
{
	/* 3.3.39.1 */
	static const char *type[]={
		"Unknown", /* 0x00 */
		"KCS (Keyboard Control Style)",
		"SMIC (Server Management Interface Chip)",
		"BT (Block Transfer)" /* 0x03 */
	};
	
	if(code<=0x03)
		return type[code-0x01];
	return out_of_spec;
}

/*
 * 3.3.40 System Power Supply (Type 39)
 */

static void
dmi_power_supply_power (char *buf, size_t len, gushort code)
{
	if(code==0x8000)
		snprintf (buf, len, "Unknown");
	else
		snprintf (buf, len, "%.3f W", (float)code/1000);
}

static const char *dmi_power_supply_type(guchar code)
{
	/* 3.3.40.1 */
	static const char *type[]={
		"Other", /* 0x01 */
		"Unknown",
		"Linear",
		"Switching",
		"Battery",
		"UPS",
		"Converter",
		"Regulator" /* 0x08 */
	};
	
	if(code>=0x01 && code<=0x08)
		return type[code-0x01];
	return out_of_spec;
}

static const char *dmi_power_supply_status(guchar code)
{
	/* 3.3.40.1 */
	static const char *status[]={
		"Other", /* 0x01 */
		"Unknown",
		"OK",
		"Non-critical"
		"Critical" /* 0x05 */
	};
	
	if(code>=0x01 && code<=0x05)
		return status[code-0x01];
	return out_of_spec;
}

static const char *
dmi_power_supply_range_switching(guchar code)
{
	/* 3.3.40.1 */
	static const char *switching[]={
		"Other", /* 0x01 */
		"Unknown",
		"Manual",
		"Auto-switch",
		"Wide Range",
		"N/A" /* 0x06 */
	};
	
	if(code>=0x01 && code<=0x06)
		return switching[code-0x01];
	return out_of_spec;
}

/*
 * Main
 */

static xmlrpc_value *
dmi_decode (xmlrpc_env *env,
		  guchar *data,
		  gushort ver)
{
	struct dmi_header *h=(struct dmi_header *)data;
	xmlrpc_value *value, *sub_val;
	char buf[BUF_SIZE];

	value = xmlrpc_struct_new (env);
	
	/*
	 * Note: DMI types 31, 37, 38 and 39 are untested
	 */
	switch(h->type)
	{
		case 0: /* 3.3.1 BIOS Information */
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type",
									"BIOS Information");
			if (h->length<0x12)
				break;

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Vendor",
									dmi_string(h, data[0x04]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Version",
									dmi_string(h, data[0x05]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Release Data",
									dmi_string(h, data[0x08]));

			snprintf (buf, BUF_SIZE, "0x%04X0", WORD(data+0x06));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Address", buf);

			dmi_bios_runtime_size (buf, BUF_SIZE, (0x10000-WORD(data+0x06))<<4);
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Runtime Size", buf);

			snprintf (buf, BUF_SIZE, "%u kB", (data[0x09]+1)<<6);
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "ROM Size", buf);

			sub_val = xmlrpc_build_value (env, "()");
			xmlrpc_struct_set_value (env, value, "Characteristics", sub_val);
			xmlrpc_DECREF (sub_val);
			dmi_bios_characteristics (env, sub_val, QWORD(data+0x0A));

			if(h->length<0x13)
				break;
			dmi_bios_characteristics_x1 (env, sub_val, data[0x12]);

			if(h->length<0x14)
				break;
			dmi_bios_characteristics_x2 (env, sub_val, data[0x13]);
			break;
		
		case 1: /* 3.3.2 System Information */
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type",
									"System Information");
			if(h->length<0x08)
				break;

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Manufacturer",
									dmi_string(h, data[0x04]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Product Name",
									dmi_string(h, data[0x05]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Version",
									dmi_string(h, data[0x06]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Serial Number",
									dmi_string(h, data[0x07]));

			if(h->length<0x19)
				break;

			dmi_system_uuid (buf, BUF_SIZE, data+0x08);
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "UUID", buf);

			snprintf (buf, BUF_SIZE, "Wake-up Type: %s",
					dmi_system_wake_up_type(data[0x18]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Wake-up Type",
									dmi_system_wake_up_type(data[0x18]));
			break;
		
		case 2: /* 3.3.3 Base Board Information */
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type",
									"Base Board Information");
			if(h->length<0x08)
				break;

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Manufacturer",
									dmi_string(h, data[0x04]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Product Name",
									dmi_string(h, data[0x05]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Version",
									dmi_string(h, data[0x06]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Serial Number",
									dmi_string(h, data[0x07]));
			if(h->length<0x0F)
				break;

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Asset Tag",
									dmi_string(h, data[0x08]));

			sub_val = xmlrpc_build_value (env, "()");
			xmlrpc_struct_set_value (env, value, "Features", sub_val);
			xmlrpc_DECREF (sub_val);
			dmi_base_board_features (env, sub_val, data[0x09]);
			
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Location In Chassis",
									dmi_string(h, data[0x0A]));

			snprintf (buf, BUF_SIZE, "0x%04X", WORD(data+0x0B));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Chassis Handle", buf);

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Type",
									dmi_base_board_type(data[0x0D]));

			if(h->length<0x0F+data[0x0E]*sizeof(gushort))
				break;

			snprintf (buf, BUF_SIZE, "%u", data[0x0E]);
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Number Of Contained Object Handlers",
									buf);

			sub_val = xmlrpc_build_value (env, "()");
			xmlrpc_struct_set_value (env, value, "Contained Object Handlers", sub_val);
			xmlrpc_DECREF (sub_val);
			dmi_base_board_handlers (env, sub_val, data[0x0E], data+0x0F);
			break;
		
		case 3: /* 3.3.4 Chassis Information */
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type",
									"Chassis Information");
			if(h->length<0x09)
				break;

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Manufacturer",
									dmi_string(h, data[0x04]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Type",
									dmi_chassis_type(data[0x05]&0x7F));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Lock",
									dmi_chassis_lock(data[0x05]>>7));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Version",
									dmi_string(h, data[0x06]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Serial Number",
									dmi_string(h, data[0x07]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Asset Tag",
									dmi_string(h, data[0x08]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Boot-up State",
									dmi_chassis_state(data[0x09]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Power Supply State",
									dmi_chassis_state(data[0x0A]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Thermal State",
									dmi_chassis_state(data[0x0B]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Security Status",
									dmi_chassis_security_status(data[0x0C]));
			if(h->length<0x11)
				break;

			snprintf (buf, BUF_SIZE, "0x%08X", DWORD(data+0x0D));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "OEM Information", buf);

			if(h->length<0x15)
				break;

			dmi_chassis_height (buf, BUF_SIZE, data[0x11]);
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Height", buf);

			dmi_chassis_power_cords (buf, BUF_SIZE, data[0x12]);
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Number Of Power Cords", buf);

			if(h->length<0x15+data[0x13]*data[0x14])
				break;

			snprintf (buf, BUF_SIZE, "%u", data[0x13]);
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Contained Element Count", buf);

			sub_val = xmlrpc_build_value (env, "()");
			xmlrpc_struct_set_value (env, value, "Contained Elements", sub_val);
			xmlrpc_DECREF (sub_val);
			dmi_chassis_elements(env, sub_val, data[0x13], data[0x14], data+0x15);
			break;
		
		case 4: /* 3.3.5 Processor Information */
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type",
									"Processor Information");
			if(h->length<0x1A)
				break;

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Socket Designation",
									dmi_string(h, data[0x04]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Type",
									dmi_processor_type(data[0x05]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Family",
									dmi_processor_family(data[0x06]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Manufacturer",
									dmi_string(h, data[0x07]));

			/* FIXME!
			dmi_processor_id(data[0x06], data+8, dmi_string(h, data[0x10]), "\t\t");
			*/

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Version",
									dmi_string(h, data[0x10]));

			sub_val = xmlrpc_build_value (env, "()");
			xmlrpc_struct_set_value (env, value, "Voltage", sub_val);
			xmlrpc_DECREF (sub_val);
			dmi_processor_voltage(env, sub_val, data[0x11]);

			dmi_processor_frequency(buf, BUF_SIZE, WORD(data+0x12));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "External Clock", buf);

			dmi_processor_frequency(buf, BUF_SIZE, WORD(data+0x14));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Max Speed", buf);

			dmi_processor_frequency(buf, BUF_SIZE, WORD(data+0x16));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Current Speed", buf);

			if(data[0x18]&(1<<6))
				RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Status",
										dmi_processor_status(data[0x18]&0x07));
			else
				RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Status", "Unpopulated");

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Upgrade",
									dmi_processor_upgrade(data[0x19]));

			if(h->length<0x20)
				break;

			dmi_processor_cache(buf, BUF_SIZE, WORD(data+0x1A), "L1", ver);
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "L1 Cache Handle", buf);
			dmi_processor_cache(buf, BUF_SIZE, WORD(data+0x1C), "L2", ver);
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "L2 Cache Handle", buf);
			dmi_processor_cache(buf, BUF_SIZE, WORD(data+0x1E), "L3", ver);
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "L3 Cache Handle", buf);

			if(h->length<0x23)
				break;

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Serial Number",
									dmi_string(h, data[0x20]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Asset Tag",
									dmi_string(h, data[0x21]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Part Number",
									dmi_string(h, data[0x22]));
			break;
		
		case 5: /* 3.3.6 Memory Controller Information */
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type",
									"Memory Controller Information");
			if(h->length<0x0F)
				break;

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Error Detecting Method",
									dmi_memory_controller_ed_method(data[0x04]));

			sub_val = xmlrpc_build_value (env, "()");
			xmlrpc_struct_set_value (env, value, "Error Correction Capabilities", sub_val);
			xmlrpc_DECREF (sub_val);
			dmi_memory_controller_ec_capabilities (env, sub_val, data[0x05]);

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Supported Interleave",
									dmi_memory_controller_interleave(data[0x06]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Current Interleave",
									dmi_memory_controller_interleave(data[0x07]));
			
			snprintf (buf, BUF_SIZE, "%u MB", 1<<data[0x08]);
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Maximum Memory Module Size", buf);

			snprintf (buf, BUF_SIZE, "%u MB", data[0x0E]*(1<<data[0x08]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Maximum Total Memory Size", buf);

			sub_val = xmlrpc_build_value (env, "()");
			xmlrpc_struct_set_value (env, value, "Supported Speeds", sub_val);
			xmlrpc_DECREF (sub_val);
			dmi_memory_controller_speeds (env, sub_val, WORD(data+0x09));

			sub_val = xmlrpc_build_value (env, "()");
			xmlrpc_struct_set_value (env, value, "Supported Memory Types", sub_val);
			xmlrpc_DECREF (sub_val);
			dmi_memory_module_types (env, sub_val, WORD(data+0x0B));

			sub_val = xmlrpc_build_value (env, "()");
			xmlrpc_struct_set_value (env, value, "Memory Module Voltage", sub_val);
			xmlrpc_DECREF (sub_val);
			dmi_processor_voltage (env, sub_val, data[0x0D]);

			if(h->length<0x0F+data[0x0E]*sizeof(gushort))
				break;

			sub_val = xmlrpc_build_value (env, "()");
			xmlrpc_struct_set_value (env, value, "Memory Module Configuration Handles", sub_val);
			xmlrpc_DECREF (sub_val);
			dmi_memory_controller_slots (env, sub_val, data[0x0E], data+0x0F);

			if(h->length<0x10+data[0x0E]*sizeof(gushort))
				break;

			sub_val = xmlrpc_build_value (env, "()");
			xmlrpc_struct_set_value (env, value, "Enabled Error Correction Capabilities", sub_val);
			xmlrpc_DECREF (sub_val);
			dmi_memory_controller_ec_capabilities (env, sub_val, data[0x0F+data[0x0E]*sizeof(gushort)]);
			break;
		
		case 6: /* 3.3.7 Memory Module Information */
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type",
									"Memory Module Information");
			if(h->length<0x0C)
				break;

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Socket Designation",
									dmi_string(h, data[0x04]));

			dmi_memory_module_connections (buf, BUF_SIZE, data[0x05]);
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Bank Connections", buf);

			dmi_memory_module_speed (buf, BUF_SIZE, data[0x06]);
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Current Speed", buf);

			sub_val = xmlrpc_build_value (env, "()");
			xmlrpc_struct_set_value (env, value, "Type", sub_val);
			xmlrpc_DECREF (sub_val);
			dmi_memory_module_types (env, sub_val, WORD(data+0x07));
			
			dmi_memory_module_size(buf, BUF_SIZE, data[0x09]);
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Installed Size", buf);

			dmi_memory_module_size(buf, BUF_SIZE, data[0x0A]);
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Enabled Size", buf);

			dmi_memory_module_error(buf, BUF_SIZE, data[0x0B]);
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Error Status", buf);
			break;
		
		case 7: /* 3.3.8 Cache Information */
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type",
									"Cache Information");
			if(h->length<0x0F)
				break;

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Socket Designation",
									dmi_string(h, data[0x04]));

			snprintf (buf, BUF_SIZE, "%s, %s, Level %u",
					WORD(data+0x05)&0x0080?"Enabled":"Disabled",
					WORD(data+0x05)&0x0008?"Socketed":"Not Socketed",
					(WORD(data+0x05)&0x0007)+1);
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Configuration", buf);

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Operational Mode",
									dmi_cache_mode((WORD(data+0x05)>>8)&0x0003));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Location",
									dmi_cache_location((WORD(data+0x05)>>5)&0x0003));

			dmi_cache_size (buf, BUF_SIZE, WORD(data+0x09));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Installed Size", buf);

			dmi_cache_size (buf, BUF_SIZE, WORD(data+0x07));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Maximum Size", buf);

			sub_val = xmlrpc_build_value (env, "()");
			xmlrpc_struct_set_value (env, value, "Supported SRAM Types", sub_val);
			xmlrpc_DECREF (sub_val);
			dmi_cache_types (env, sub_val, WORD(data+0x0B));

			sub_val = xmlrpc_build_value (env, "()");
			xmlrpc_struct_set_value (env, value, "Installed SRAM Types", sub_val);
			xmlrpc_DECREF (sub_val);
			dmi_cache_types (env, sub_val, WORD(data+0x0D));

			if(h->length<0x13)
				break;

			dmi_memory_module_speed (buf, BUF_SIZE, data[0x0F]);
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Speed", buf);

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Error Correction Type",
									dmi_cache_ec_type(data[0x10]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "System Type",
									dmi_cache_type(data[0x11]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Associativity",
									dmi_cache_associativity(data[0x12]));
			break;
		
		case 8: /* 3.3.9 Port Connector Information */
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type",
									"Port Connector Information");
			if(h->length<0x09)
				break;

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Internal Reference Designator",
									dmi_string(h, data[0x04]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Internal Connector Type",
									dmi_port_connector_type(data[0x05]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "External Reference Designator",
									dmi_string(h, data[0x06]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "External Connector Type",
									dmi_port_connector_type(data[0x07]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Port Type",
									dmi_port_type(data[0x08]));
			break;
		
		case 9: /* 3.3.10 System Slots */
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type",
									"System Slot Information");

			if(h->length<0x0C)
				break;

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Designation",
									dmi_string(h, data[0x04]));

			snprintf (buf, BUF_SIZE, "%s%s",
					dmi_slot_bus_width(data[0x06]),
					dmi_slot_type(data[0x05]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Type", buf);

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Current Usage",
									dmi_slot_current_usage(data[0x07]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Length",
									dmi_slot_length(data[0x08]));

			if (dmi_slot_id (buf, BUF_SIZE, data[0x09], data[0x0A], data[0x05]))
				RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Slot ID", buf);

			sub_val = xmlrpc_build_value (env, "()");
			xmlrpc_struct_set_value (env, value, "Characteristics", sub_val);
			xmlrpc_DECREF (sub_val);

			if(h->length<0x0D)
				dmi_slot_characteristics (env, sub_val, data[0x0B], 0x00);
			else
				dmi_slot_characteristics (env, sub_val, data[0x0B], data[0x0C]);
			break;
		
		case 10: /* 3.3.11 On Board Devices Information */
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type",
									"On Board Devices Information");

			sub_val = xmlrpc_build_value (env, "()");
			xmlrpc_struct_set_value (env, value, "Devices", sub_val);
			xmlrpc_DECREF (sub_val);
			dmi_on_board_devices (env, sub_val, h);
			break;
		
		case 11: /* 3.3.12 OEM Strings */
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type",
									"OEM Strings");
			if(h->length<0x05)
				break;

			sub_val = xmlrpc_build_value (env, "()");
			xmlrpc_struct_set_value (env, value, "Strings", sub_val);
			xmlrpc_DECREF (sub_val);
			dmi_oem_strings (env, sub_val, h);
			break;	
		
		case 12: /* 3.3.13 System Configuration Options */
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type",
									"System Configuration Options");
			if(h->length<0x05)
				break;

			sub_val = xmlrpc_build_value (env, "()");
			xmlrpc_struct_set_value (env, value, "Options", sub_val);
			xmlrpc_DECREF (sub_val);
			dmi_system_configuration_options (env, sub_val, h);
			break;
		
		case 13: /* 3.3.14 BIOS Language Information */
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type",
									"BIOS Language Information");
			if(h->length<0x16)
				break;

			snprintf (buf, BUF_SIZE, "%u", data[0x04]);
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Installable Languages Count", buf);

			sub_val = xmlrpc_build_value (env, "()");
			xmlrpc_struct_set_value (env, value, "Installable Languages", sub_val);
			xmlrpc_DECREF (sub_val);
			dmi_bios_languages(env, sub_val, h);

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Currently Installed Language",
									dmi_string(h, data[0x15]));
			break;
		
		case 14: /* 3.3.15 Group Associations */
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type",
									"Group Associations");
			if(h->length<0x05)
				break;

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Name",
									dmi_string(h, data[0x04]));

			snprintf (buf, BUF_SIZE, "%u", (h->length-0x05)/3);
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Items Count", buf);

			sub_val = xmlrpc_build_value (env, "()");
			xmlrpc_struct_set_value (env, value, "Items", sub_val);
			xmlrpc_DECREF (sub_val);
			dmi_group_associations_items (env, sub_val, (h->length-0x05)/3, data+0x05);
			break;
		
		case 15: /* 3.3.16 System Event Log */
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type",
									"System Event Log");
			if(h->length<0x14)
				break;

			snprintf (buf, BUF_SIZE, "%u bytes", WORD(data+0x04));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Area Length", buf);

			snprintf (buf, BUF_SIZE, "0x%04X", WORD(data+0x06));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Header Start Offset", buf);

			if (WORD(data+0x08)-WORD(data+0x06)) {
				snprintf (buf, BUF_SIZE, "%u byte%s",
						WORD(data+0x08)-WORD(data+0x06),
						WORD(data+0x08)-WORD(data+0x06)>1?"s":"");
				RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Header Length", buf);
			}

			snprintf (buf, BUF_SIZE, "0x%04X", WORD(data+0x08));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Data Start Offset", buf);

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Access Method",
									dmi_event_log_method(data[0x0A]));

			dmi_event_log_address (buf, BUF_SIZE, data[0x0A], data+0x10);
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Access Address", buf);

			dmi_event_log_status (buf, BUF_SIZE, data[0x0B]);
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Status", buf);

			snprintf (buf, BUF_SIZE, "0x%08X", DWORD(data+0x0C));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Change Token", buf);

			if(h->length<0x17)
				break;

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Header Format",
									dmi_event_log_header_type(data[0x14]));

			snprintf(buf, BUF_SIZE, "%u",	data[0x15]);
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Supported Log Type Descriptors",
									buf);

			if (h->length<0x17+data[0x15]*data[0x16])
				break;

			sub_val = xmlrpc_build_value (env, "()");
			xmlrpc_struct_set_value (env, value, "Descriptors", sub_val);
			xmlrpc_DECREF (sub_val);
			dmi_event_log_descriptors(env, sub_val, data[0x15], data[0x16], data+0x17);
			break;
		
		case 16: /* 3.3.17 Physical Memory Array */
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type",
									"Physical Memory Array");
			if(h->length<0x0F)
				break;

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Location",
									dmi_memory_array_location(data[0x04]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Use",
									dmi_memory_array_use(data[0x05]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Error Correction Type",
									dmi_memory_array_ec_type(data[0x06]));

			dmi_memory_array_capacity (buf, BUF_SIZE, DWORD(data+0x07));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Maximum Capacity", buf);

			dmi_memory_array_error_handle (buf, BUF_SIZE, WORD(data+0x0B));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Error Information Handle",
									buf);

			snprintf (buf, BUF_SIZE, "%u", WORD(data+0x0D));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Number Of Devices", buf);
			break;
		
		case 17: /* 3.3.18 Memory Device */
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type",
									"Memory Device");
			if(h->length<0x15)
				break;

			snprintf (buf, BUF_SIZE, "0x%04X", WORD(data+0x04));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Array Handle", buf);

			dmi_memory_array_error_handle (buf, BUF_SIZE, WORD(data+0x06));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Error Information Handle", buf);

			dmi_memory_device_width (buf, BUF_SIZE, WORD(data+0x08));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Total Width", buf);

			dmi_memory_device_width (buf, BUF_SIZE, WORD(data+0x0A));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Data Width", buf);

			dmi_memory_device_size (buf, BUF_SIZE, WORD(data+0x0C));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Size", buf);

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Form Factor",
									dmi_memory_device_form_factor(data[0x0E]));

			dmi_memory_device_set (buf, BUF_SIZE, data[0x0F]);
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Set", buf);

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Locator",
									dmi_string(h, data[0x10]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Bank Locator",
									dmi_string(h, data[0x11]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Type",
									dmi_memory_device_type(data[0x12]));

			sub_val = xmlrpc_build_value (env, "()");
			xmlrpc_struct_set_value (env, value, "Type Detail", sub_val);
			xmlrpc_DECREF (sub_val);
			dmi_memory_device_type_detail (env, sub_val, WORD(data+0x13));

			if(h->length<0x17)
				break;

			dmi_memory_device_speed (buf, BUF_SIZE, WORD(data+0x15));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Speed", buf);

			if(h->length<0x1B)
				break;

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Manufacturer",
									dmi_string(h, data[0x17]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Serial Number",
									dmi_string(h, data[0x18]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Asset Tag",
									dmi_string(h, data[0x19]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Part Number",
									dmi_string(h, data[0x1A]));
			break;
		
		case 18: /* 3.3.19 32-bit Memory Error Information */
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type",
									"32-bit Memory Error Information");
			if(h->length<0x17)
				break;

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Type",
									dmi_memory_error_type(data[0x04]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Granularity",
									dmi_memory_error_granularity(data[0x05]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Operation",
									dmi_memory_error_operation(data[0x06]));

			dmi_memory_error_syndrome (buf, BUF_SIZE, DWORD(data+0x07));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Vendor Syndrome", buf);

			dmi_32bit_memory_error_address (buf, BUF_SIZE, DWORD(data+0x0B));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Memory Array Address", buf);

			dmi_32bit_memory_error_address (buf, BUF_SIZE, DWORD(data+0x0F));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Device Address", buf);

			dmi_32bit_memory_error_address (buf, BUF_SIZE, DWORD(data+0x13));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Resolution", buf);
			break;
		
		case 19: /* 3.3.20 Memory Array Mapped Address */
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type",
									"Memory Array Mapped Address");
			if(h->length<0x0F)
				break;

			snprintf(buf, BUF_SIZE, "0x%08X%03X",
				    DWORD(data+0x04)>>2, (DWORD(data+0x04)&0x3)<<10);
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Starting Address", buf);

			snprintf(buf, BUF_SIZE, "0x%08X%03X",
				    DWORD(data+0x08)>>2, ((DWORD(data+0x08)&0x3)<<10)+0x3FF);
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Ending Address", buf);
			
			dmi_mapped_address_size (buf, BUF_SIZE, DWORD(data+0x08)-DWORD(data+0x04)+1);
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Range Size", buf);

			snprintf (buf, BUF_SIZE, "0x%04X", WORD(data+0x0C));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Physical Array Handle", buf);

			snprintf(buf, BUF_SIZE, "%u", data[0x0F]);
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Partition Width", buf);
			break;
		
		case 20: /* 3.3.21 Memory Device Mapped Address */
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type",
									"Memory Device Mapped Address");
			if(h->length<0x13)
				break;

			snprintf(buf, BUF_SIZE, "0x%08X%03X",
				    DWORD(data+0x04)>>2, (DWORD(data+0x04)&0x3)<<10);
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Starting Address", buf);

			snprintf(buf, BUF_SIZE, "0x%08X%03X",
				    DWORD(data+0x08)>>2, ((DWORD(data+0x08)&0x3)<<10)+0x3FF);
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Ending Address", buf);
			
			dmi_mapped_address_size (buf, BUF_SIZE, DWORD(data+0x08)-DWORD(data+0x04)+1);
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Range Size", buf);

			snprintf (buf, BUF_SIZE, "0x%04X", WORD(data+0x0C));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Physical Device Handle", buf);

			snprintf (buf, BUF_SIZE, "0x%04X", WORD(data+0x0E));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Memory Array Mapped Address Handle", buf);

			dmi_mapped_address_row_position (buf, BUF_SIZE, data[0x10]);
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Partition Row Position", buf);

			if (dmi_mapped_address_interleave_position (buf, BUF_SIZE, data[0x11]))
				RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Interleave Position", buf);
			if (dmi_mapped_address_interleaved_data_depth (buf, BUF_SIZE, data[0x12]))
				RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Interleaved Data Depth", buf);
			break;
		
		case 21: /* 3.3.22 Built-in Pointing Device */
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type",
									"Built-in Pointing Device");
			if(h->length<0x07)
				break;
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Type",
									dmi_pointing_device_type(data[0x04]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Interface",
									dmi_pointing_device_interface(data[0x05]));

			snprintf (buf, BUF_SIZE, "%u", data[0x06]);
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Buttons", buf);
			break;
		
		case 22: /* 3.3.23 Portable Battery */
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type",
									"Portable Battery");
			if(h->length<0x10)
				break;
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Location",
									dmi_string(h, data[0x04]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Manufacturer",
									dmi_string(h, data[0x05]));
			
			if(data[0x06] || h->length<0x1A)
				RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Manufacturer Date",
										dmi_string(h, data[0x06]));
			if(data[0x07] || h->length<0x1A)
				RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Serial Number",
										dmi_string(h, data[0x07]));

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Name",
									dmi_string(h, data[0x08]));
			if(data[0x09]!=0x02 || h->length<0x1A)
				RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Chemistry",
										dmi_battery_chemistry(data[0x09]));

			if(h->length<0x1A)
				dmi_battery_capacity (buf, BUF_SIZE, WORD(data+0x0A), 1);
			else
				dmi_battery_capacity (buf, BUF_SIZE, WORD(data+0x0A), data[0x15]);

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Design Capacity", buf);

			dmi_battery_voltage (buf, BUF_SIZE, WORD(data+0x0C));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Design Voltage", buf);

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "SBDS Version",
									dmi_string(h, data[0x0E]));

			dmi_battery_maximum_error (buf, BUF_SIZE, data[0x0E]);
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Maximum Error", buf);

			if(h->length<0x1A)
				break;

			if(data[0x07]==0) {
				printf(buf, BUF_SIZE, "%04X", WORD(data+0x10));
				RCD_XMLRPC_STRUCT_SET_STRING (env, value, "SBDS Serial Number", buf);
			}
			if(data[0x06]==0) {
				snprintf (buf, BUF_SIZE, "%u-%02u-%02u",
					    1980+(WORD(data+0x12)>>9), (WORD(data+0x12)>>5)&0x0F,
					    WORD(data+0x12)&0x1F);
				RCD_XMLRPC_STRUCT_SET_STRING (env, value, "SBDS Manufacture Date", buf);
			}
			if(data[0x09]==0x02)
				RCD_XMLRPC_STRUCT_SET_STRING (env, value, "SBDS Chemistry",
										dmi_string(h, data[0x14]));

			snprintf(buf, BUF_SIZE, "0x%08X", DWORD(data+0x16));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "OEM-specific Information", buf);
			break;
		
		case 23: /* 3.3.24 System Reset */
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type",
									"System Reset");
			if(h->length<0x0D)
				break;

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Status",
									data[4]&(1<<0)?"Enabled":"Disabled");
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Watchdog Timer",
									data[4]&(1<<5)?"Present":"No");
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Boot Option",
									dmi_system_reset_boot_option((data[0x04]>>1)&0x3));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Boot Option On Limit",
									dmi_system_reset_boot_option((data[0x04]>>3)&0x3));

			dmi_system_reset_count (buf, BUF_SIZE, WORD(data+0x05));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Reset Count", buf);

			dmi_system_reset_count (buf, BUF_SIZE, WORD(data+0x07));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Reset Limit", buf);

			dmi_system_reset_timer (buf, BUF_SIZE, WORD(data+0x09));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Timer Interval", buf);

			dmi_system_reset_timer (buf, BUF_SIZE, WORD(data+0x0B));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Timeout", buf);
			break;
		
		case 24: /* 3.3.25 Hardware Security */
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type",
									"Hardware Security");
			if(h->length<0x05)
				break;

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Power-On Password Status",
									dmi_hardware_security_status(data[0x04]>>6));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Keyboard Password Status",
									dmi_hardware_security_status((data[0x04]>>4)&0x3));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Administrator Password Status",
									dmi_hardware_security_status((data[0x04]>>2)&0x3));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Front Panel Reset Status",
									dmi_hardware_security_status(data[0x04]&0x3));
			break;
		
		case 25: /* 3.3.26 System Power Controls */
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type",
									"System Power Controls");
			if(h->length<0x09)
				break;

			dmi_power_controls_power_on (buf, BUF_SIZE, data+0x04);
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Next Scheduled Power-on",
									buf);

			break;
		
		case 26: /* 3.3.27 Voltage Probe */
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type",
									"Voltage Probe");
			if(h->length<0x14)
				break;

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Description",
									dmi_string(h, data[0x04]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Location",
									dmi_voltage_probe_location(data[0x05]&0x1f));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Status",
									dmi_probe_status(data[0x05]>>5));

			dmi_voltage_probe_value (buf, BUF_SIZE, WORD(data+0x06));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Maximum Value", buf);

			dmi_voltage_probe_value (buf, BUF_SIZE, WORD(data+0x08));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Minimum Value", buf);

			dmi_voltage_probe_resolution (buf, BUF_SIZE, WORD(data+0x0A));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Resolution", buf);

			dmi_voltage_probe_value (buf, BUF_SIZE, WORD(data+0x0C));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Tolerance", buf);

			dmi_probe_accuracy (buf, BUF_SIZE, WORD(data+0x0E));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Accuracy", buf);

			snprintf (buf, BUF_SIZE, "0x%08X", DWORD(data+0x10));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "OEM-specific Information", buf);

			if(h->length<0x16)
				break;

			dmi_voltage_probe_value (buf, BUF_SIZE, WORD(data+0x14));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Nominal Value", buf);
			break;
		
		case 27: /* 3.3.28 Cooling Device */
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type",
									"Cooling Device");
			if(h->length<0x0C)
				break;

			if(WORD(data+0x04)!=0xFFFF) {
				snprintf (buf, BUF_SIZE, "0x%04X", WORD(data+0x04));
				RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Temperature Probe Handle", buf);
			}

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Type",
									dmi_cooling_device_type(data[0x06]&0x1f));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Status",
									dmi_probe_status(data[0x06]>>5));
			if(data[0x07]!=0x00) {
				snprintf (buf, BUF_SIZE, "%u", data[0x07]);
				RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Cooling Unit Group", buf);
			}

			snprintf (buf, BUF_SIZE, "0x%08X", DWORD(data+0x08));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "OEM-specific Information", buf);

			if(h->length<0x0E)
				break;

			dmi_cooling_device_speed (buf, BUF_SIZE, WORD(data+0x0C));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Nominal Speed", buf);
			break;
		
		case 28: /* 3.3.29 Temperature Probe */
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type",
									"Temperature Probe");
			if(h->length<0x14)
				break;

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Description",
									dmi_string(h, data[0x04]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Location",
									dmi_temperature_probe_location(data[0x05]&0x1F));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Status",
									dmi_probe_status(data[0x05]>>5));

			dmi_temperature_probe_value (buf, BUF_SIZE, WORD(data+0x06));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Maximum Value", buf);

			dmi_temperature_probe_value (buf, BUF_SIZE, WORD(data+0x08));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Minimum Value", buf);

			dmi_temperature_probe_resolution (buf, BUF_SIZE, WORD(data+0x0A));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Resolution", buf);

			dmi_temperature_probe_value (buf, BUF_SIZE, WORD(data+0x0C));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Tolerance", buf);

			dmi_probe_accuracy (buf, BUF_SIZE, WORD(data+0x0E));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Accuracy", buf);

			snprintf (buf, BUF_SIZE, "0x%08X", DWORD(data+0x10));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "OEM-specific Information", buf);

			if(h->length<0x16)
				break;

			dmi_temperature_probe_value (buf, BUF_SIZE, WORD(data+0x14));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Nominal Value", buf);
			break;
		
		case 29: /* 3.3.30 Electrical Current Probe */
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type",
									"Electrical Current Probe");
			if(h->length<0x14)
				break;

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Description",
									dmi_string(h, data[0x04]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Location",
									dmi_voltage_probe_location(data[5]&0x1F));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Status",
									dmi_probe_status(data[0x05]>>5));

			dmi_current_probe_value (buf, BUF_SIZE, WORD(data+0x06));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Maximum Value", buf);

			dmi_current_probe_value (buf, BUF_SIZE, WORD(data+0x08));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Minimum Value", buf);

			dmi_current_probe_resolution (buf, BUF_SIZE, WORD(data+0x0A));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Resolution", buf);

			dmi_current_probe_value (buf, BUF_SIZE, WORD(data+0x0C));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Tolerance", buf);

			dmi_probe_accuracy (buf, BUF_SIZE, WORD(data+0x0E));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Accuracy", buf);

			snprintf (buf, BUF_SIZE, "0x%08X", DWORD(data+0x10));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "OEM-specific Information", buf);

			if(h->length<0x16)
				break;

			dmi_current_probe_value (buf, BUF_SIZE, WORD(data+0x14));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Nominal Value", buf);
			break;
		
		case 30: /* 3.3.31 Out-of-band Remote Access */
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type",
									"Out-of-band Remote Access");
			if(h->length<0x06)
				break;

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Manufacturer Name",
									dmi_string(h, data[0x04]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Inbound Connection",
									data[0x05]&(1<<0)?"Enabled":"Disabled");
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Outbound Connection",
									data[0x05]&(1<<1)?"Enabled":"Disabled");
			break;
		
		case 31: /* 3.3.32 Boot Integrity Services Entry Point */
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type",
									"Boot Integrity Services Entry Point");
			break;
		
		case 32: /* 3.3.33 System Boot Information */
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type",
									"System Boot Information");
			if(h->length<0x0B)
				break;

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Status",
									dmi_system_boot_status(data[0x0A]));
			break;
		
		case 33: /* 3.3.34 64-bit Memory Error Information */
			if(h->length<0x1F)
				break;

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type",
									"64-bit Memory Error Information");

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Type",
									dmi_memory_error_type(data[0x04]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Granularity",
									dmi_memory_error_granularity(data[0x05]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Operation",
									dmi_memory_error_operation(data[0x06]));

			dmi_memory_error_syndrome (buf, BUF_SIZE, DWORD(data+0x07));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Vendor Syndrome", buf);

			dmi_64bit_memory_error_address (buf, BUF_SIZE, QWORD(data+0x0B));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Memory Array Address", buf);

			dmi_64bit_memory_error_address (buf, BUF_SIZE, QWORD(data+0x13));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Device Address", buf);

			dmi_32bit_memory_error_address (buf, BUF_SIZE, DWORD(data+0x1B));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Resolution", buf);
			break;
		
		case 34: /* 3.3.35 Management Device */
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type",
									"Management Device");
			if(h->length<0x0B)
				break;

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Description",
									dmi_string(h, data[0x04]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Type",
									dmi_management_device_type(data[0x05]));

			snprintf (buf, BUF_SIZE, "0x%08X", DWORD(data+0x06));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Address", buf);

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Address Type",
									dmi_management_device_address_type(data[0x0A]));
			break;
		
		case 35: /* 3.3.36 Management Device Component */
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type",
									"Management Device Component");
			if(h->length<0x0B)
				break;

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Description",
									dmi_string(h, data[0x04]));

			snprintf (buf, BUF_SIZE, "0x%04X", WORD(data+0x05));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Management Device Handle", buf);

			snprintf (buf, BUF_SIZE, "0x%04X", WORD(data+0x07));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Component Handle", buf);

			if(WORD(data+0x09)!=0xFFFF) {
				snprintf (buf, BUF_SIZE, "0x%04X", WORD(data+0x09));
				RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Threshold Handle", buf);
			}
			break;
		
		case 36: /* 3.3.37 Management Device Threshold Data */
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type",
									"Management Device Threshold Data");
			if(h->length<0x10)
				break;
			if(WORD(data+0x04)!=0x8000)
				RCD_XMLRPC_STRUCT_SET_INT (env, value, "Lower Non-critical Threshold",
									  (gint16)WORD(data+0x04));
			if(WORD(data+0x06)!=0x8000)
				RCD_XMLRPC_STRUCT_SET_INT (env, value, "Upper Non-critical Threshold",
									  (gint16)WORD(data+0x06));
			if(WORD(data+0x08)!=0x8000)
				RCD_XMLRPC_STRUCT_SET_INT (env, value, "Lower Critical Threshold",
									  (gint16)WORD(data+0x08));
			if(WORD(data+0x0A)!=0x8000)
				RCD_XMLRPC_STRUCT_SET_INT (env, value, "Upper Critical Threshold",
									  (gint16)WORD(data+0x0A));
			if(WORD(data+0x0C)!=0x8000)
				RCD_XMLRPC_STRUCT_SET_INT (env, value, "Lower Non-recoverable Threshold",
									  (gint16)WORD(data+0x0C));
			if(WORD(data+0x0E)!=0x8000)
				RCD_XMLRPC_STRUCT_SET_INT (env, value, "Upper Non-recoverable Threshold",
									  (gint16)WORD(data+0x0E));
			break;
		
		case 37: /* 3.3.38 Memory Channel */
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type",
									"Memory Channel");
			if(h->length<0x07)
				break;

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Type",
									dmi_memory_channel_type(data[0x04]));

			snprintf (buf, BUF_SIZE, "%u", data[0x05]);
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Maximal Load", buf);

			snprintf (buf, BUF_SIZE, "%u", data[0x06]);
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Devices Count", buf);

			if(h->length<0x07+3*data[0x06])
				break;

			sub_val = xmlrpc_build_value (env, "()");
			xmlrpc_struct_set_value (env, value, "Devices", sub_val);
			xmlrpc_DECREF (sub_val);
			dmi_memory_channel_devices (env, sub_val, data[0x06], data+0x07);
			break;
		
		case 38: /* 3.3.39 IPMI Device Information */
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type",
									"IPMI Device Information");
			if(h->length<0x10)
				break;

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Interface Type",
									dmi_ipmi_interface_type(data[0x04]));
			snprintf (buf, BUF_SIZE, "%u.%u", data[0x05]>>4, data[0x05]&0x0F);
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Specification Revision", buf);

			snprintf (buf, BUF_SIZE, "%u", data[0x06]);
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "I2C Slave Address", buf);
			if(data[0x07]==0xFF) {
				snprintf (buf, BUF_SIZE, "%u", data[0x07]);
				RCD_XMLRPC_STRUCT_SET_STRING (env, value, "NV Storage Device Address",
										buf);
			}

			snprintf (buf, BUF_SIZE, "0x%08X (%s)", DWORD(data+0x08),
					DWORD(data+0x08)&1 ? "I/O" : "memory-mapped");
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Base Address", buf);
			
			break;
		
		case 39: /* 3.3.40 System Power Supply */
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type",
									"System Power Supply");
			if(h->length<0x10)
				break;

			
			if(data[0x04]!=0x00) {
				snprintf (buf, BUF_SIZE, "%u", data[0x04]);
				RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Power Unit Group", buf);
			}

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Location",
									dmi_string(h, data[0x05]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Name",
									dmi_string(h, data[0x06]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Manufacturer",
									dmi_string(h, data[0x07]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Serial Number",
									dmi_string(h, data[0x08]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Asset Tag",
									dmi_string(h, data[0x09]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Model Part Number",
									dmi_string(h, data[0x0A]));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Revision",
									dmi_string(h, data[0x0B]));

			dmi_power_supply_power (buf, BUF_SIZE, WORD(data+0x0C));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Max Power Capacity", buf);

			if (WORD(data+0x0E)&(1<<1))
				snprintf (buf, BUF_SIZE, "Present, %s",
						dmi_power_supply_status((WORD(data+0x0E)>>7)&0x07));
			else
				snprintf (buf, BUF_SIZE, "Not Present");

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Status", buf);

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Type",
									dmi_power_supply_type((WORD(data+0x0E)>>10)&0x0F));
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Input Voltage Range Switching",
									dmi_power_supply_range_switching((WORD(data+0x0E)>>3)&0x0F));

			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Plugged",
									WORD(data+0x0E)&(1<<2)?"No":"Yes");
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Hot Replaceable",
									WORD(data+0x0E)&(1<<0)?"Yes":"No");
			if(h->length<0x16)
				break;

			if(WORD(data+0x10)!=0xFFFF) {
				snprintf (buf, BUF_SIZE, "0x%04X", WORD(data+0x10));
				RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Input Voltage Probe Handle",
										buf);
			}
			if(WORD(data+0x12)!=0xFFFF) {
				snprintf(buf, BUF_SIZE, "0x%04X", WORD(data+0x12));
				RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Cooling Device Handle",
										buf);
			}
			if(WORD(data+0x14)!=0xFFFF) {
				snprintf (buf, BUF_SIZE, "0x%04X", WORD(data+0x14));
				RCD_XMLRPC_STRUCT_SET_STRING (env, value, "Input Current Probe Handle",
										buf);
			}
			break;
		
		case 126: /* 3.3.41 Inactive */
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type",
									"Inactive");
			break;
		
		case 127: /* 3.3.42 End Of Table */
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type",
									"End Of Table");
			break;
		
		default:
			RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type",
									h->type>=128 ? "OEM-specific" : "Unknown");
			/* FIXME
			dmi_dump(h, "\t\t");
			*/
	}

cleanup:
	return value;
}

static xmlrpc_value *
dmi_table(xmlrpc_env *env,
		int fd,
		guint32 base,
		gushort len,
		gushort num,
		gushort ver,
		const char *devmem)
{
	guchar *buf;
	guchar *data;
	xmlrpc_value *array, *value;
	int i=0;

	rcd_module_debug (RCD_DEBUG_LEVEL_DEBUG, rcd_module,
				   "%u structures occupying %u bytes",
				   num, len);
	rcd_module_debug (RCD_DEBUG_LEVEL_DEBUG, rcd_module,
				   "Table at 0x%08X", base);
	
	if(base>0xFFFFF)
		return NULL;
	
	if((buf=malloc(len))==NULL)
		return NULL;

	if(lseek(fd, (off_t)base, SEEK_SET)==-1)
	{
		perror(devmem);
		return NULL;
	}
	if(myread(fd, buf, len, devmem)==-1)
	{
		free(buf);
		exit(1);
	}

	array = xmlrpc_build_value (env, "()");
	data=buf;
	while(i<num && data+sizeof(struct dmi_header)<=buf+len)
	{
		guchar *next;
		struct dmi_header *h=(struct dmi_header *)data;
		
		/* look for the next handle */
		next=data+h->length;
		while(next-buf+1<len && (next[0]!=0 || next[1]!=0))
			next++;
		next+=2;
		if(next-buf<=len) {
			value = dmi_decode (env, data, ver);
			xmlrpc_array_append_item (env, array, value);
			xmlrpc_DECREF (value);
		}
		
		data=next;
		i++;
	}
	
	if(i!=num)
		rcd_module_debug (RCD_DEBUG_LEVEL_MESSAGE, rcd_module,
					   "Wrong DMI structures count: %d announced, only %d decoded",
					   num, i);
	if(data-buf!=len)
		rcd_module_debug (RCD_DEBUG_LEVEL_MESSAGE, rcd_module,
					   "Wrong DMI structures length: %d bytes announced, %d bytes decoded",
					   len, data-buf);
	
	free(buf);

	return array;
}


static int checksum(guchar *buf, guchar len)
{
	guchar sum=0, a;
	
	for(a=0; a<len; a++)
		sum+=buf[a];
	return (sum==0);
}

xmlrpc_value *
get_dmi_info (RCDModule *module, xmlrpc_env *env)
{
	guchar buf[0x20];
	int fd = 0;
	xmlrpc_value *value = NULL;
	off_t fp=0xF0000;
	const char *devmem="/dev/mem";

	rcd_module = module;

	if(sizeof(guchar)!=1 || sizeof(gushort)!=2 || sizeof(guint32)!=4 || '\0'!=0)
	{
		rcd_module_debug (RCD_DEBUG_LEVEL_ERROR, rcd_module,
					   "Compiler Incompatibility");
		return NULL;
	}
	
	if((fd=open(devmem, O_RDONLY))==-1 || lseek(fd, fp, SEEK_SET)==-1)
	{
		perror(devmem);
		return NULL;
	}

	while(fp<0xFFFFF)
	{
		if(myread(fd, buf, 0x10, devmem)==-1)
			goto cleanup;
		fp+=16;
		
		if(memcmp(buf, "_SM_", 4)==0 && fp<0xFFFFF)
		{
			if(myread(fd, buf+0x10, 0x10, devmem)==-1)
				goto cleanup;
			fp+=16;
			
			if(checksum(buf, buf[0x05])
			 && memcmp(buf+0x10, "_DMI_", 5)==0
			 && checksum(buf+0x10, 0x0F))
			{
				rcd_module_debug (RCD_DEBUG_LEVEL_INFO, rcd_module,
							   "SMBIOS %u.%u present",
							   buf[0x06], buf[0x07]);
				value = dmi_table (env, fd, DWORD(buf+0x18), WORD(buf+0x16),
							    WORD(buf+0x1C), (buf[0x06]<<8)+buf[0x07],
							    devmem);
				
				/* dmi_table moved us far away */
				lseek(fd, fp, SEEK_SET);
			}
		}
		else if(memcmp(buf, "_DMI_", 5)==0
		 && checksum(buf, 0x0F))
		{
			rcd_module_debug (RCD_DEBUG_LEVEL_INFO, rcd_module,
						   "Legacy DMI %u.%u present",
						   buf[0x0E]>>4, buf[0x0E]&0x0F);
			value = dmi_table (env, fd, DWORD(buf+0x08), WORD(buf+0x06), WORD(buf+0x0C),
						    ((buf[0x0E]&0xF0)<<4)+(buf[0x0E]&0x0F), devmem);
			
			/* dmi_table moved us far away */
			lseek(fd, fp, SEEK_SET);
		}
	}

cleanup:
	
	if (fd)
		close (fd);

	if (env->fault_occurred) {
		if (value)
			xmlrpc_DECREF(value);
		return NULL;
	}
	return value;
}
