/* -*- Mode: C; tab-width: 4; c-basic-offset: 4; indent-tabs-mode: nil -*- */

/*
 * rcd-rpc-si.c
 *
 * Copyright (C) 2002 Ximian, Inc.
 *
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <xmlrpc.h>

#include <rcd-module.h>
#include <rcd-rpc.h>
#include <rcd-rpc-util.h>
#include <rcd-xmlrpc.h>
#include <rcd-world-remote.h>
#include <libredcarpet.h>

#include "dmidecode.h"
#include "../python/rcd-rpc-python.h"
#include "rce-helpers.h"

#define SEND_HWINFO_DELAY 10 * 1000   /* 10 sec */
#define SEND_PACKAGES_DELAY 60 * 1000 /* 1 minute */

static RCDModule *rcd_module = NULL;

int rcd_module_major_version = RCD_MODULE_MAJOR_VERSION;
int rcd_module_minor_version = RCD_MODULE_MINOR_VERSION;

static xmlrpc_value *
si_read_dmi (xmlrpc_env *env, xmlrpc_value *param_array, gpointer user_data)
{
    return get_dmi_info (rcd_module, env);
}

#if 0
static void
si_dmi_sent (char *server_url,
             char *method_name,
             xmlrpc_value *param_array,
             void *user_data,
             xmlrpc_env *fault,
             xmlrpc_value *result)
{
    if (fault->fault_occurred)
        rc_debug (RC_DEBUG_LEVEL_MESSAGE, "Send DMI failed: %s",
                  fault->fault_string);
    else
        rc_debug (RC_DEBUG_LEVEL_DEBUG, "DMI successfully sent");
}

static void
si_send_dmi (void)
{
    xmlrpc_env env;
    xmlrpc_value *value;
    xmlrpc_value *params;

    xmlrpc_env_init (&env);

    value = get_dmi_info (rcd_module, &env);
    XMLRPC_FAIL_IF_FAULT (&env);

    params = xmlrpc_build_value (&env, "(V)", value);
    xmlrpc_DECREF (value);
    XMLRPC_FAIL_IF_FAULT (&env);

    rcd_xmlrpc_client_foreach_host (TRUE, "rcserver.machine.updateDMI",
                                    si_dmi_sent, NULL,
                                    params);
    xmlrpc_DECREF (params);

cleanup:
    xmlrpc_env_clean (&env);
}
#endif

static xmlrpc_value *
si_get_hwinfo (xmlrpc_env *env)
{
    xmlrpc_value *params;
    xmlrpc_value *value;

    params = xmlrpc_build_value (env, "(ss)", "hardware", "read_hw_info");
    value = python_function (env, params, NULL);
    xmlrpc_DECREF (params);

    return value;
}

static xmlrpc_value *
si_read_hwinfo (xmlrpc_env *env, xmlrpc_value *param_array, gpointer user_data)
{
    return si_get_hwinfo (env);
}

static void
si_hwinfo_sent (char *server_url,
                char *method_name,
                xmlrpc_value *param_array,
                void *user_data,
                xmlrpc_env *fault,
                xmlrpc_value *result)
{
    if (fault->fault_occurred)
        rc_debug (RC_DEBUG_LEVEL_MESSAGE, "Send harwdware info failed: %s",
                  fault->fault_string);
    else
        rc_debug (RC_DEBUG_LEVEL_DEBUG, "Hardware info successfully sent");
}

static void
si_send_hwinfo (GSList *services)
{
    xmlrpc_env env;
    xmlrpc_value *value;
    xmlrpc_value *params;
    RCWorldService *service;
    GSList *iter;

    xmlrpc_env_init (&env);

    value = si_get_hwinfo (&env);
    XMLRPC_FAIL_IF_FAULT (&env);

    params = xmlrpc_build_value (&env, "(V)", value);
    xmlrpc_DECREF (value);
    XMLRPC_FAIL_IF_FAULT (&env);

    for (iter = services; iter != NULL; iter = iter->next) {
        xmlrpc_server_info *server;

        service = iter->data;

        server = rcd_xmlrpc_get_server (&env, service->url);
        xmlrpc_client_call_server_asynch_params (server,
                                                 "rcserver.machine.updateHWInfo",
                                                 si_hwinfo_sent, NULL,
                                                 params);
        xmlrpc_server_info_free (server);
    }

    xmlrpc_DECREF (params);

cleanup:
    xmlrpc_env_clean (&env);
}

/******************************************************************************/

static xmlrpc_value *
si_get_distro (xmlrpc_env *env, xmlrpc_value *param_array, gpointer user_data)
{
    RCWorldService *service;
    char *service_id;
    xmlrpc_value *value = NULL;

    xmlrpc_parse_value (env, param_array, "(s)", &service_id);
    XMLRPC_FAIL_IF_FAULT (env);

    service = rc_world_multi_lookup_service_by_id (RC_WORLD_MULTI (rc_get_world ()),
                                                   service_id);

    if (RCD_IS_WORLD_REMOTE (service)) {
        RCDistro *distro = RCD_WORLD_REMOTE (service)->distro;
        value = xmlrpc_build_value (env, "s", rc_distro_get_target (distro));
        XMLRPC_FAIL_IF_FAULT (env);
    } else {
        xmlrpc_env_set_fault_formatted (env, RCD_RPC_FAULT_INVALID_SERVICE,
                                        "Unable to get distro for '%s'",
                                        service_id);
    }

cleanup:
    return value;
}

static xmlrpc_value *
si_get_uptime (xmlrpc_env *env, xmlrpc_value *param_array, gpointer user_data)
{
    FILE *fp;
    int status;
    gdouble uptime, idle;

    fp = fopen ("/proc/uptime", "r");
    if (!fp) {
        xmlrpc_env_set_fault (env, -666, "Can not read uptime");
        return NULL;
    }

    status = fscanf (fp, "%lf %lf", &uptime, &idle);
    fclose (fp);

    if (status < 2) {
        xmlrpc_env_set_fault (env, -666, "Can not parse uptime");
        return NULL;
    }

    return xmlrpc_build_value (env, "d", uptime);
}

/******************************************************************************/

static xmlrpc_value *
deps_to_xdeps (RCPackageDepArray *rc_deps, xmlrpc_env *env)
{
    if (rc_deps == NULL)
        return xmlrpc_build_value (env, "()");

    return rcd_rc_package_dep_array_to_xmlrpc (rc_deps, env);
}

static xmlrpc_value *
pkg_to_xpkg (RCPackage *pkg, gboolean dependencies, xmlrpc_env *env)
{
    xmlrpc_value *sub_val;
    xmlrpc_value *value = NULL;

    value = rcd_rc_package_to_xmlrpc (pkg, env);
    XMLRPC_FAIL_IF_FAULT (env);

    RCD_XMLRPC_STRUCT_SET_STRING (env, value, "arch",
                                  rc_arch_to_string (pkg->arch));

    if (!dependencies)
        return value;

    sub_val = deps_to_xdeps (pkg->requires_a, env);
    XMLRPC_FAIL_IF_FAULT (env);
    xmlrpc_struct_set_value (env, value, "requires", sub_val);
    xmlrpc_DECREF (sub_val);
    XMLRPC_FAIL_IF_FAULT (env);

    sub_val = deps_to_xdeps (pkg->provides_a, env);
    XMLRPC_FAIL_IF_FAULT (env);
    xmlrpc_struct_set_value (env, value, "provides", sub_val);
    xmlrpc_DECREF (sub_val);
    XMLRPC_FAIL_IF_FAULT (env);

    sub_val = deps_to_xdeps (pkg->conflicts_a, env);
    XMLRPC_FAIL_IF_FAULT (env);
    xmlrpc_struct_set_value (env, value, "conflicts", sub_val);
    xmlrpc_DECREF (sub_val);
    XMLRPC_FAIL_IF_FAULT (env);

    sub_val = deps_to_xdeps (pkg->obsoletes_a, env);
    XMLRPC_FAIL_IF_FAULT (env);
    xmlrpc_struct_set_value (env, value, "obsoletes", sub_val);
    xmlrpc_DECREF (sub_val);
    XMLRPC_FAIL_IF_FAULT (env);

    if (rc_package_is_package_set (pkg)) {
        sub_val = deps_to_xdeps (pkg->obsoletes_a, env);
        XMLRPC_FAIL_IF_FAULT (env);
        xmlrpc_struct_set_value (env, value, "children", sub_val);
        xmlrpc_DECREF (sub_val);
        XMLRPC_FAIL_IF_FAULT (env);
    }

cleanup:
    if (env->fault_occurred && value) {
        xmlrpc_DECREF (value);
        value = NULL;
    }

    return value;
}

typedef struct {
    RCPackageSpec *spec;
    RCArch arch;
} PkgHashInfo;

typedef struct {
    xmlrpc_env   *env;
    xmlrpc_value *list;
    GSList       *hash_infos;
} GetPkgsInfo;

static gint
sort_package_infos (gconstpointer a,
                    gconstpointer b)
{
    PkgHashInfo *aa = (PkgHashInfo *) a;
    PkgHashInfo *bb = (PkgHashInfo *) b;

    if (aa->spec->nameq > bb->spec->nameq)
        return 1;
    if (aa->spec->nameq < bb->spec->nameq)
        return -1;

    return 0;
}

/* Destroys the slist! */
static gchar *
calculate_hash (GSList *slist)
{
    GString *string;
    GSList *tmp;
    gchar *hash;

    slist = g_slist_sort (slist, sort_package_infos);

    /* This hopefully optimizes allocations a bit.
       20 is characters per package (randomly chosen) */
    string = g_string_sized_new (20 * g_slist_length (slist));

    for (tmp = slist; tmp; tmp = tmp->next) {
        gchar *buf;
        PkgHashInfo *info = tmp->data;;

        buf = rc_package_spec_to_str (info->spec);
        g_string_append (string, buf);
        g_free (buf);
        g_string_append (string, rc_arch_to_string (info->arch));

        g_free (info);
    }

    g_slist_free (slist);

    hash = rc_md5_digest_from_string (string->str);
    g_string_free (string, TRUE);

    return hash;
}

static gboolean
add_package_cb (RCPackage *pkg, gpointer user_data)
{
    GetPkgsInfo  *info = user_data;
    xmlrpc_value *value;
    PkgHashInfo  *hash_info;

    value = pkg_to_xpkg (pkg, FALSE, info->env);
    XMLRPC_FAIL_IF_FAULT (info->env);

    xmlrpc_array_append_item (info->env, info->list, value);
    xmlrpc_DECREF (value);
    XMLRPC_FAIL_IF_FAULT (info->env);

    hash_info = g_new (PkgHashInfo, 1);
    hash_info->spec = RC_PACKAGE_SPEC (pkg);
    hash_info->arch = pkg->arch;

    info->hash_infos = g_slist_prepend (info->hash_infos, hash_info);

    return TRUE;

cleanup:
    /* TODO: maybe output to rcd log. */
    xmlrpc_env_clean (info->env);

    return TRUE;
}

static char *
get_pkg_list_and_hash (xmlrpc_env    *env,
                       RCWorld       *world,
                       xmlrpc_value **pkg_list)
{
    GetPkgsInfo info;
    char *hash = NULL;

    *pkg_list = xmlrpc_build_value (env, "()");
    XMLRPC_FAIL_IF_FAULT (env);

    info.env = env;
    info.hash_infos = NULL;
    info.list = *pkg_list;

    rc_world_foreach_package (world,
                              RC_CHANNEL_SYSTEM,
                              add_package_cb,
                              &info);

    hash = calculate_hash (info.hash_infos); /* frees the argument */

cleanup:

    return hash;
}

static xmlrpc_value *
package_hash (xmlrpc_env *env, xmlrpc_value *param_array, gpointer user_data)
{
    RCWorld *world = user_data;
    char *hash;
    xmlrpc_value *value;
    xmlrpc_value *ret = NULL;

    hash = get_pkg_list_and_hash (env, world, &value);
    XMLRPC_FAIL_IF_FAULT (env);

    xmlrpc_DECREF (value);
    ret = xmlrpc_build_value (env, "s", hash);
    g_free (hash);

cleanup:
    return ret;
}

static xmlrpc_value *
package_list (xmlrpc_env *env, xmlrpc_value *param_array, gpointer user_data)
{
    RCWorld *world = user_data;
    char *hash, *new_hash;
    xmlrpc_value *value;
    xmlrpc_value *ret = NULL;

    xmlrpc_parse_value (env, param_array, "(s)", &hash);
    XMLRPC_FAIL_IF_FAULT (env);

    new_hash = get_pkg_list_and_hash (env, world, &value);
    XMLRPC_FAIL_IF_FAULT (env);

    if (strncmp (hash, new_hash, 32) == 0) {
        xmlrpc_DECREF (value);
        value = xmlrpc_build_value (env, "()");
    }

    ret = xmlrpc_build_value (env, "{s:s,s:V}",
                              "hash", new_hash,
                              "packages", value);

    xmlrpc_DECREF (value);
    g_free (new_hash);

cleanup:
    return ret;
}

typedef struct {
    xmlrpc_env *env;
    GHashTable *hash;
    xmlrpc_value *installs;
    xmlrpc_value *removals;
    GSList       *hash_infos;
} PkgDiffInfo;

static GHashTable *
str_list_to_hash (xmlrpc_env *env, xmlrpc_value *array)
{
    GHashTable *hash_table;
    int i;

    hash_table = g_hash_table_new (g_str_hash, g_str_equal);

    for (i = 0; i < xmlrpc_array_size (env, array); i++) {
        char *str;
        xmlrpc_value *v = xmlrpc_array_get_item (env, array, i);
        XMLRPC_FAIL_IF_FAULT (env);

        xmlrpc_parse_value (env, v, "s", &str);
        XMLRPC_FAIL_IF_FAULT (env);

        g_hash_table_insert (hash_table, str, GINT_TO_POINTER (1));
    }

    return hash_table;

cleanup:
    g_hash_table_destroy (hash_table);
    return NULL;
}

static gboolean
add_installs (RCPackage *pkg, gpointer user_data)
{
    PkgDiffInfo *info = user_data;
    xmlrpc_value *value;
    gchar *spec;
    PkgHashInfo  *hash_info;

    hash_info = g_new (PkgHashInfo, 1);
    hash_info->spec = RC_PACKAGE_SPEC (pkg);
    hash_info->arch = pkg->arch;

    info->hash_infos = g_slist_prepend (info->hash_infos, hash_info);

    spec = rc_package_spec_to_str (&pkg->spec);

    if (g_hash_table_lookup (info->hash, spec))
        g_hash_table_remove (info->hash, spec);
    else {
        value = pkg_to_xpkg (pkg, TRUE, info->env);
        XMLRPC_FAIL_IF_FAULT (info->env);

        xmlrpc_array_append_item (info->env, info->installs, value);
        xmlrpc_DECREF (value);
    }

cleanup:
    g_free (spec);

    if (info->env->fault_occurred)
        /* TODO: maybe output to rcd log. */
        xmlrpc_env_clean (info->env);

    return TRUE;
}

static void
add_removals (gpointer key, gpointer key_value, gpointer user_data)
{
    PkgDiffInfo *info = user_data;
    xmlrpc_value *value;

    value = xmlrpc_build_value (info->env, "s", (gchar *) key);
    XMLRPC_FAIL_IF_FAULT (info->env);

    xmlrpc_array_append_item (info->env, info->removals, value);
    xmlrpc_DECREF (value);

cleanup:
    if (info->env->fault_occurred)
        /* TODO: maybe output to rcd log. */
        xmlrpc_env_clean (info->env);
}

static xmlrpc_value *
package_diff (xmlrpc_env *env, xmlrpc_value *param_array, gpointer user_data)
{
    RCWorld *world = user_data;
    char *hash;
    PkgDiffInfo info;
    GHashTable *hash_table;
    xmlrpc_value *array;
    xmlrpc_value *ret = NULL;

    xmlrpc_parse_value (env, param_array, "(V)", &array);
    XMLRPC_FAIL_IF_FAULT (env);

    hash_table = str_list_to_hash (env, array);
    XMLRPC_FAIL_IF_FAULT (env);

    info.env = env;
    info.hash = hash_table;
    info.hash_infos = NULL;
    info.installs = xmlrpc_build_value (env, "()");
    info.removals = xmlrpc_build_value (env, "()");

    rc_world_foreach_package (world,
                              RC_CHANNEL_SYSTEM,
                              add_installs,
                              &info);

    /* Now we have list of new packages in info.list and
       info.hash only contains packages which are not in
       world, ie they are removed.
    */

    g_hash_table_foreach (hash_table, add_removals, &info);
    g_hash_table_destroy (hash_table);

    hash = calculate_hash (info.hash_infos); /* frees the argument */

    ret = xmlrpc_build_value (env, "{s:s,s:V,s:V}",
                              "hash", hash,
                              "installs", info.installs,
                              "removals", info.removals);
    xmlrpc_DECREF (info.installs);
    xmlrpc_DECREF (info.removals);

    g_free (hash);

cleanup:

    return ret;
}

typedef struct {
    xmlrpc_env *env;
    GHashTable *hash;
    xmlrpc_value *packages;
} PkgDepsInfo;

static gboolean
deps_add_pkg (RCPackage *pkg, gpointer user_data)
{
    PkgDepsInfo *info = user_data;
    xmlrpc_value *value;
    gchar *spec;

    spec = rc_package_spec_to_str (&pkg->spec);

    if (g_hash_table_lookup (info->hash, spec)) {
        g_hash_table_remove (info->hash, spec);

        value = pkg_to_xpkg (pkg, TRUE, info->env);
        XMLRPC_FAIL_IF_FAULT (info->env);

        xmlrpc_array_append_item (info->env, info->packages, value);
        xmlrpc_DECREF (value);
    }

cleanup:
    g_free (spec);

    if (info->env->fault_occurred)
        /* TODO: maybe output to rcd log. */
        xmlrpc_env_clean (info->env);

    return TRUE;
}

static xmlrpc_value *
package_deps (xmlrpc_env *env, xmlrpc_value *param_array, gpointer user_data)
{
    RCWorld *world = user_data;
    PkgDepsInfo info;
    GHashTable *hash_table;
    xmlrpc_value *array;

    xmlrpc_parse_value (env, param_array, "(V)", &array);
    XMLRPC_FAIL_IF_FAULT (env);

    hash_table = str_list_to_hash (env, array);
    XMLRPC_FAIL_IF_FAULT (env);

    info.env = env;
    info.hash = hash_table;
    info.packages = xmlrpc_build_value (env, "()");

    rc_world_foreach_package (world,
                              RC_CHANNEL_SYSTEM,
                              deps_add_pkg,
                              &info);

    g_hash_table_destroy (hash_table);
    return info.packages;

cleanup:
    return NULL;
}

/******************************************************************************/

static void
package_hash_sent (char *server_url,
                   char *method_name,
                   xmlrpc_value *param_array,
                   void *user_data,
                   xmlrpc_env *fault,
                   xmlrpc_value *result)
{
    if (fault->fault_occurred)
        rc_debug (RC_DEBUG_LEVEL_MESSAGE, "Send package hash failed: %s",
                  fault->fault_string);
    else
        rc_debug (RC_DEBUG_LEVEL_DEBUG, "Package hash successfully sent");
}

static void
package_hash_send (GSList *services)
{
    xmlrpc_env env;
    xmlrpc_value *value;
    char *hash;
    RCWorld *world;
    RCWorldService *service;
    GSList *iter;
    xmlrpc_value *params = NULL;

    xmlrpc_env_init (&env);

    world = rc_get_world ();
    hash = get_pkg_list_and_hash (&env, world, &value);
    XMLRPC_FAIL_IF_FAULT (&env);
    xmlrpc_DECREF (value);

    rc_debug (RC_DEBUG_LEVEL_DEBUG, "Sending new hash: %s", hash);

    params = xmlrpc_build_value (&env, "(s)", hash);
    XMLRPC_FAIL_IF_FAULT (&env);

    for (iter = services; iter != NULL; iter = iter->next) {
        xmlrpc_server_info *server;

        service = iter->data;

        server = rcd_xmlrpc_get_server (&env, service->url);
        xmlrpc_client_call_server_asynch_params (server,
                                                 "rcserver.machine.updateWorldHash",
                                                 package_hash_sent, NULL,
                                                 params);
        xmlrpc_server_info_free (server);
    }

    xmlrpc_DECREF (params);

cleanup:
    g_free (hash);
    xmlrpc_env_clean (&env);
}

/*****************************************************************************/

static void
packages_changed_cb (RCWorld *world, gpointer user_data)
{
    static NotifyServicesCache *cache = NULL;

    if (cache == NULL)
        cache = notify_services_cache_new (package_hash_send, FALSE,
                                           SEND_PACKAGES_DELAY);

    notify_services (cache, NULL);
}

static void
connect_packages_changed (RCWorldMulti *multi,
                          RCWorld      *subworld,
                          gpointer      user_data)
{
    if (RC_IS_WORLD_SYNTHETIC (subworld)  ||
        RC_IS_WORLD_SYSTEM (subworld)) {
        g_signal_connect (subworld,
                          "changed_packages",
                          G_CALLBACK (packages_changed_cb),
                          NULL);
    }
}

/*****************************************************************************/

void
rcd_module_load (RCDModule *module)
{
    RCWorld *world;

    /* Initialize the module */
    module->name = "rcd.si";
    module->description = "System Information Module";
    module->version = 0;
    module->interface_major = 1;
    module->interface_minor = 0;

    rcd_module = module;

    world = rc_get_world ();

    g_signal_connect (RC_WORLD_MULTI (world),
                      "subworld_added",
                      G_CALLBACK (connect_packages_changed),
                      NULL);

    /* Register RPC methods */
    rcd_rpc_register_method ("rcd.si.dmi",    si_read_dmi, "superuser", NULL);
    rcd_rpc_register_method ("rcd.si.hwinfo", si_read_hwinfo, "superuser", NULL);

    rcd_rpc_register_method ("rcd.si.distro", si_get_distro, "superuser", NULL);
    rcd_rpc_register_method ("rcd.si.uptime", si_get_uptime, "superuser", NULL);

    rcd_rpc_register_method ("rcd.si.package_hash", package_hash, "superuser", world);
    rcd_rpc_register_method ("rcd.si.package_list", package_list, "superuser", world);
    rcd_rpc_register_method ("rcd.si.package_diff", package_diff, "superuser", world);
    rcd_rpc_register_method ("rcd.si.package_deps", package_deps, "superuser", world);

    notify_rce_add (si_send_hwinfo, FALSE, SEND_HWINFO_DELAY);
    notify_rce_add (package_hash_send, FALSE, SEND_PACKAGES_DELAY);

} /* rcd_module_load */
