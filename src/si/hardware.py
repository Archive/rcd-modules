#!/usr/bin/python

import commands
import sys
import re
import glob
import syslog

################################################################################

def parse_net_info(buf):
    info = []
    dev = None
    for line in buf.split('\n'):
        match = re.search("^(\w+)", line)
        if match:
            d = match.group(1)
            dev = {"device"    : d,
                   "hwaddr"    : "00:00:00:00:00:00",
                   "ip"        : None,
                   "broadcast" : None,
                   "mask"      : None}

        if not dev:
            continue

        match = re.search("HWaddr ([\:\w]+)", line)
        if match:
            dev["hwaddr"] = match.group(1)

        match = re.search("inet addr:([\.\d]+)", line)
        if match:
            dev["ip"] = match.group(1)

        match = re.search("Bcast:([\.\d]+)", line)
        if match:
            dev["broadcast"] = match.group(1)

        match = re.search("Mask:([\.\d]+)", line)
        if match:
            dev["mask"] = match.group(1)

        if not len(line):
            info.append(dev)
            dev = None

    return info

def read_net_info():
    cmd = "/sbin/ifconfig"

    out = commands.getstatusoutput(cmd)
    if out[0]:
        syslog.syslog(syslog.LOG_WARNING, "Could not get network info")
        return None

    return parse_net_info(out[1])

################################################################################

def parse_mount_info(buf):
    info = []
    for line in buf.split('\n'):
        pieces = re.split("\s+", line)
        if len(pieces) < 5:
            continue
        if pieces[0] and pieces[0][0] != "/":
            continue

        dev = {"device" : pieces[0],
               "size"   : pieces[1],
               "used"   : pieces[2],
               "mounted" : pieces[5] }
        info.append(dev)

    return info

def read_mount_info():
    cmd = "/bin/df"

    out = commands.getstatusoutput(cmd)
    if out[0]:
        syslog.syslog(syslog.LOG_WARNING, "Could not get disk info")
        return None

    return parse_mount_info(out[1])

################################################################################

def parse_mem_info(lines):
    info = {}

    pattern_table = (
        ("memory", re.compile("^MemTotal:\s+(\d+)")),
        ("swap",   re.compile("^SwapTotal:\s+(\d+)")),
        )

    for line in lines:
        for pattern in pattern_table:
            match = pattern[1].search (line)
            if match:
                info[pattern[0]] = str(int(match.group(1)) * 1024)
                break

    return info

def read_mem_info():
    path = "/proc/meminfo"

    try:
        f = open(path, "r")
    except IOError:
        syslog.syslog(syslog.LOG_WARNING, "Could not get memory info")
        return None

    buf = f.readlines()
    f.close()

    return parse_mem_info(buf)

################################################################################

def read_hardware_platform():
    cmd = "/bin/uname -i"

    out = commands.getstatusoutput(cmd)
    if out[0]:
        syslog.syslog(syslog.LOG_WARNING, "Could not get hardware platform")
        return None

    return out[1]

################################################################################

def parse_cpu_info_i386(lines):
    info = []
    dev = {}
    for line in lines:
        # Remove newline
        line = line[:-1]
        if not len(line):
            if dev:
                info.append(dev)
                dev = {}
            continue

        try:
            (name, val)  = re.split("\s+:\s+", line)
        except ValueError:
            continue

        name = name.strip()
        val = val.strip()

        if name == "processor":
            dev = {"cpu"   : val,
                   "name"  : None,
                   "id"    : None,
                   "mhz"   : None,
                   "cache" : None}
            continue

        if name == "vendor_id":
            dev["id"] = val
            continue

        if name == "model name":
            dev["name"] = val
            continue

        if name == "cpu MHz":
            dev["mhz"] = val
            continue

        if name == "cache size":
            dev["cache"] = val

    return info

def parse_cpu_info_ppc64(lines):
    info = []
    dev = {}
    for line in lines:
        # Remove newline
        line = line[:-1]
        if not len(line):
            if dev:
                info.append(dev)
                dev = {}
            continue

        try:
            (name, val)  = re.split("\s+:\s+", line)
        except ValueError:
            continue

        name = name.strip()
        val = val.strip()

        if name == "processor":
            dev = {"cpu"   : val,
                   "name"  : None,
                   "id"    : None,
                   "mhz"   : None,
                   "cache" : None}
            continue

        if name == "cpu":
            dev["name"] = val
            continue

        if name == "clock":
            dev["mhz"] = val
            continue

        if name == "revision":
            dev["id"] = val
            continue

        if name == "i-cache":
            dev["cache"] = val
            continue

    return info

def parse_cpu_info_s390(lines):
    info = []
    dev = {}
    vendor_id = None
    pattern = re.compile("^processor (\d+)$")

    for line in lines:
        # Remove newline
        line = line[:-1]
        if not len(line):
            continue

        try:
            (name, val)  = re.split(":", line)
        except ValueError:
            continue

        name = name.strip()
        val = val.strip()

        if name == "vendor_id":
            vendor_id = val
            continue

        match = pattern.match(name)
        if match:
            dev = {"cpu" : match.group(1),
                   "name": vendor_id,
                   "id"  : None}

            for piece in val.split(","):
                (name, val) = piece.split("=")
                name = name.strip()
                val = val.strip()

                if name == "identification":
                    dev["id"] = val

            info.append(dev)

    return info


def read_cpu_info():
    path = "/proc/cpuinfo"

    platforms = {
        'i386'  : parse_cpu_info_i386,
        'ppc64' : parse_cpu_info_ppc64,
        's390'  : parse_cpu_info_s390,
        's390x' : parse_cpu_info_s390,
        'x86_64': parse_cpu_info_i386
        }

    platform = read_hardware_platform()
    if not platform:
        return None
    if not platform in platforms.keys():
        syslog.syslog(syslog.LOG_WARNING, "CPU info for platform '%s' " \
                      "is not supported" % platform)
        return None

    parser = platforms[platform]

    try:
        f = open(path, "r")
    except IOError:
        syslog.syslog(syslog.LOG_WARNING, "Could not get CPU info")
        return None

    buf = f.readlines()
    f.close()

    return parser(buf)


################################################################################

def parse_ide_disk(path):

    def read_first_line(path):
        try:
            f = open(path, "r")
        except IOError:
            return None
        buf = f.readline()
        f.close()
        return buf[:-1]

    if read_first_line(path + "/media") != "disk":
        return None

    disk = {"device"   : path.split('/')[-1],
            "model"    : read_first_line(path + "/model"),
            "bus"      : "IDE",
            "geometry" : None,
            "size"     : None }

    buf = read_first_line(path + "/geometry")
    if buf:
        match = re.search("^physical\s+([\d\/]+)", buf)
        if match:
            disk["geometry"] = match.group(1)

    buf = read_first_line(path + "/capacity")
    if buf:
        i = int(buf)
        disk["size"] = int (i * 512 / 1024 / 1024)

    return disk

def get_ide_disks():
    return glob.glob("/proc/ide/hd*")

def read_ide_disks():
    info = []
    for d in get_ide_disks():
        dev = parse_ide_disk(d)
        if dev:
            info.append(dev)

    return info

################################################################################
## This works only on RH

def parse_audio_info(lines):
    info = []
    dev = {}
    for line in lines:
        # Remove newline
        line = line[:-1]
        if line == "-":
            if dev:
                info.append(dev)
                dev = {}
            continue

        try:
            (name, val)  = re.split(":\s+", line)
        except ValueError:
            continue

        name = name.strip()
        val = val.strip()

        if name == "class" and val == "AUDIO":
            dev = {"desc"   : None,
                   "bus"    : None,
                   "driver" : None}
            continue

        if not dev:
            continue

        if name == "desc":
            dev["desc"] = val
            continue
        if name == "bus":
            dev["bus"] = val
            continue
        if name == "driver":
            dev["driver"] = val

    return info

def read_audio_info():
    file = "/etc/sysconfig/hwconf"

    try:
        f = open(file, "r")
    except IOError:
        syslog.syslog(syslog.LOG_DEBUG, "Could not read audio info")
        return None

    buf = f.readlines()
    f.close()

    return parse_audio_info(buf)

################################################################################
## This works only on RH

def parse_video_info(lines):
    info = []
    dev = {}
    for line in lines:
        # Remove newline
        line = line[:-1]
        if line == "-":
            if dev:
                info.append(dev)
                dev = {}
            continue

        try:
            (name, val)  = re.split(":\s+", line)
        except ValueError:
            continue

        name = name.strip()
        val = val.strip()

        if name == "class" and val == "VIDEO":
            dev = {"desc"   : None,
                   "bus"    : None,
                   "driver" : None}
            continue

        if not dev:
            continue

        if name == "desc":
            dev["desc"] = val
            continue
        if name == "bus":
            dev["bus"] = val
            continue
        if name == "driver":
            dev["driver"] = val

    return info

def read_video_info():
    file = "/etc/sysconfig/hwconf"

    try:
        f = open(file, "r")
    except IOError:
        syslog.syslog(syslog.LOG_DEBUG, "Could not read video info")
        return None

    buf = f.readlines()
    f.close()

    return parse_video_info(buf)

################################################################################

def read_hw_info():
    syslog.openlog("rcd-hardware")
    syslog.syslog(syslog.LOG_DEBUG, "starting up")
    hw = {}

    info = read_net_info()
    if info:
        hw["net"] = info

    info = read_mount_info()
    if info:
        hw["mount"] = info

    info = read_mem_info()
    if info:
        hw["memory"] = info

    info = read_cpu_info()
    if info:
        hw["cpu"] = info

    info = read_ide_disks()
    if info:
        hw["disk"] = info

    info = read_audio_info()
    if info:
        hw["audio"] = info

    info = read_video_info()
    if info:
        hw["video"] = info

    return hw

if __name__ == "__main__":
    hw = read_hw_info()
    print hw
