/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */

#include <xmlrpc.h>
#include <rcd-module.h>
#include <rcd-recurring.h>
#include <rcd-transaction.h>
#include <libredcarpet.h>

#ifndef RCD_RPC_ST_H
#define RCD_RPC_ST_H

typedef enum {
    PP_STATUS_NONE      = 0,
    PP_STATUS_PENDING   = 1,
    PP_STATUS_FAILED    = 2,
    PP_STATUS_SUCCEEDED = 3
} PrePositionStatus;

typedef struct {
    RCDRecurring recurring;

    guint ref_count;
    char *trid;
    char *name;

    char *service_id;

    PrePositionStatus pp_status;
    time_t            pp_time;

    time_t first_run;
    guint  interval;

    RCDTransactionFlags flags;

    int rollback;
    gboolean allow_removals;

    xmlrpc_value *pre_scripts;
    xmlrpc_value *post_scripts;

    xmlrpc_value *install;
    xmlrpc_value *remove;

    xmlrpc_value *install_channels;
    xmlrpc_value *update_channels;

    xmlrpc_value *patches;

    char *client_id;
    char *client_version;
} SuperTransaction;

RCDModule *super_transaction_module (void);

#endif /* RCD_RPC_ST_H */
