/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */

#include "st-patch.h"
#include "st-transaction.h"
#include <rcd-rpc-util.h>
#include <rcd-xmlrpc.h>
#include <rc-you-transaction.h>
#include <you-util.h>

void
super_transaction_install_patches (SuperTransaction *st,
                                   xmlrpc_env *env)
{
    RCYouPatchSList *patches;
    RCPending *download_pending, *transaction_pending;
    GSList *pending_list;
    RCYouTransaction *transaction;
    RCDIdentity *identity;

    patches = rc_xmlrpc_array_to_rc_you_patch_slist
        (st->patches, env, RC_YOU_PATCH_FROM_XMLRPC_PATCH);
    if (env->fault_occurred) {
        log_xmlrpc_fault (st, env);
        goto cleanup;
    }

    transaction = rc_you_transaction_new ();
    rc_you_transaction_set_id (transaction, st->trid);
    rc_you_transaction_set_patches (transaction, patches);
    rc_you_transaction_set_flags (transaction, st->flags);

    identity = rcd_identity_new ();
    identity->username = g_strdup ("root");
    identity->privileges = rcd_privileges_from_string ("superuser");

    rc_you_transaction_set_client_info (transaction,
                                        st->client_id,
                                        st->client_version,
                                        "localhost",
                                        identity);

    rcd_identity_free (identity);

    rc_you_transaction_begin (transaction);

    download_pending = rc_you_transaction_get_download_pending (transaction);
    transaction_pending =
        rc_you_transaction_get_transaction_pending (transaction);

    g_object_unref (transaction);

    pending_list = NULL;
    if (download_pending)
        pending_list = g_slist_prepend (pending_list, download_pending);

    if (transaction_pending)
        pending_list = g_slist_prepend (pending_list, transaction_pending);

    if (pending_list) {
        rcd_rpc_block_on_pending_list (env, pending_list, TRUE,
                                       RCD_RPC_FAULT_TRANSACTION_FAILED);
    }

    g_slist_free (pending_list);

cleanup:
    if (patches) {
        rc_you_patch_slist_unref (patches);
        g_slist_free (patches);
    }
}
