/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */

#include <libredcarpet.h>
#include <rcd-transaction.h>
#include <rcd-prefs.h>
#include <rcd-rpc-util.h>
#include "st-rollback.h"

/* This function is evil. */
RCPackage *
synth_package_from_update (RCPackage *real_package, RCPackageUpdate *update)
{
    RCPackage *package;

    package = rc_package_copy (real_package);

    /* Clear out the RCPackageSpec and copy in the update's */
    rc_package_spec_free_members (RC_PACKAGE_SPEC (package));
    rc_package_spec_copy (RC_PACKAGE_SPEC (package), RC_PACKAGE_SPEC (update));

    /* Delete the old history and replace it with one entry: the update */
    rc_package_update_slist_free (package->history);
    package->history = NULL;
    package->history = g_slist_prepend (package->history,
                                        rc_package_update_copy (update));

    return package;
}

static void
rollback_actions_to_packages (RCRollbackActionSList  *actions,
                              RCPackageSList        **install_packages,
                              RCPackageSList        **remove_packages)
{
    RCRollbackActionSList *iter;

    for (iter = actions; iter; iter = iter->next) {
        RCRollbackAction *action = iter->data;
        RCPackage *package;

        if (rc_rollback_action_is_install (action)) {
            RCPackage *real_package = rc_rollback_action_get_package (action);
            RCPackageUpdate *update =
                rc_rollback_action_get_package_update (action);

            package = synth_package_from_update (real_package, update);

            *install_packages = g_slist_prepend (*install_packages, package);
        }
        else {
            package = rc_package_ref (rc_rollback_action_get_package (action));

            *remove_packages = g_slist_prepend (*remove_packages, package);
        }
    }
}

static void
rollback_finished_cb (RCDTransaction *transaction,
                      gpointer        user_data)
{
    RCRollbackActionSList *actions = user_data;

    rc_rollback_restore_files (actions);
    rc_rollback_action_slist_free (actions);
}

void
super_transaction_rollback (SuperTransaction *st,
                            xmlrpc_env       *env)
{
    RCRollbackActionSList *actions;
    RCDTransaction *transaction;
    RCPending *download_pending, *transaction_pending;
    GSList *pending_list;
    RCDIdentity *identity;
    RCPackageSList *install_packages = NULL;
    RCPackageSList *remove_packages = NULL;

    if (st->rollback == 0) {
        return;
    }

    if (!rcd_prefs_get_rollback ()) {
        xmlrpc_env_set_fault (env, RCD_RPC_FAULT_TRANSACTION_FAILED,
                              "Rollback is disabled");
        return;
    }

    actions = rc_rollback_get_actions (st->rollback);
    if (actions == NULL) {
        xmlrpc_env_set_fault (env, RCD_RPC_FAULT_TRANSACTION_FAILED,
                              "Could not get rollback actions");
        return;
    }

    rollback_actions_to_packages (actions,
                                 &install_packages,
                                 &remove_packages);

    if (g_slist_length (install_packages) == 0 &&
        g_slist_length (remove_packages) == 0) {
        if (install_packages)
            rc_package_slist_unref (install_packages);
        if (remove_packages)
            rc_package_slist_unref (remove_packages);

        return;
    }

    transaction = rcd_transaction_new ();
    rcd_transaction_set_rollback (transaction, TRUE);
    rcd_transaction_set_id (transaction, st->trid);
    rcd_transaction_set_install_packages (transaction, install_packages);
    rcd_transaction_set_remove_packages (transaction, remove_packages);
    rcd_transaction_set_flags (transaction, st->flags);

    identity = rcd_identity_new ();
    identity->username = g_strdup ("root");
    identity->privileges = rcd_privileges_from_string ("superuser");

    rcd_transaction_set_client_info (transaction,
                                     st->client_id,
                                     st->client_version,
                                     "localhost",
                                     identity);

    rcd_identity_free (identity);

    if (install_packages) {
        rc_package_slist_unref (install_packages);
        g_slist_free (install_packages);
    }

    if (remove_packages) {
        rc_package_slist_unref (remove_packages);
        g_slist_free (remove_packages);
    }

    g_signal_connect (transaction, "transaction_finished",
                      G_CALLBACK (rollback_finished_cb),
                      actions);

    rcd_transaction_begin (transaction);

    download_pending = rcd_transaction_get_download_pending (transaction);
    transaction_pending =
        rcd_transaction_get_transaction_pending (transaction);

    g_object_unref (transaction);

    pending_list = NULL;
    if (download_pending)
        pending_list = g_slist_prepend (pending_list, download_pending);

    if (transaction_pending)
        pending_list = g_slist_prepend (pending_list, transaction_pending);

    if (pending_list) {
        rcd_rpc_block_on_pending_list (env, pending_list, TRUE,
                                       RCD_RPC_FAULT_TRANSACTION_FAILED);
    }

    g_slist_free (pending_list);
}
