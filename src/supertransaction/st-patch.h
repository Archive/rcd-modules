#include "rcd-rpc-st.h"

#ifndef __ST_PATCH__
#define __ST_PATCH__

void super_transaction_install_patches (SuperTransaction *st, xmlrpc_env *env);

#endif /* __ST_PATCH__ */
