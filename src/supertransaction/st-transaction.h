#ifndef __ST_TRANSACTION__
#define __ST_TRANSACTION__

#include "rcd-rpc-st.h"

void log_xmlrpc_fault (SuperTransaction *st, xmlrpc_env *fault);
void super_transaction_transact (SuperTransaction *st, xmlrpc_env *env);

#endif /* __ST_TRANSACTION__ */
