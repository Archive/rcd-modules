/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */

#include "st-script.h"
#include <rcd-rpc.h>

static int
run_script (xmlrpc_env       *env,
            SuperTransaction *st,
            const char       *sid,
            xmlrpc_value     *content)
{
    xmlrpc_value *params;
    xmlrpc_value *value;
    int ret = -1;

    params = xmlrpc_build_value (env, "(Vssss)",
                                 content,
                                 st->trid,
                                 sid,
                                 st->client_id,
                                 st->client_version);
    XMLRPC_FAIL_IF_FAULT (env);

    value = rcd_rpc_call_method (env,
                                 "rcd.script.run_blocking",
                                 params);

    xmlrpc_DECREF (params);
    XMLRPC_FAIL_IF_FAULT (env);

    xmlrpc_parse_value (env, value, "i", &ret);
    xmlrpc_DECREF (value);

cleanup:
    return ret;
}

static int
super_transaction_script (SuperTransaction *st,
                          xmlrpc_value     *scripts,
                          xmlrpc_env       *env)
{
    int i, len;
    int ret;

    len = xmlrpc_array_size (env, scripts);
    XMLRPC_FAIL_IF_FAULT (env);

    for (i = 0; i < len; i++) {
        xmlrpc_value *content;
        char *sid;
        xmlrpc_value *s = xmlrpc_array_get_item (env, scripts, i);

        xmlrpc_parse_value (env, s, "(sV)", &sid, &content);
        XMLRPC_FAIL_IF_FAULT (env);

        ret = run_script (env, st, sid, content);
        XMLRPC_FAIL_IF_FAULT (env);

        if (ret != 0)
            return ret;
    }

    return 0;

cleanup:
    return -1;
}

void
super_transaction_pre_script (SuperTransaction *st,
                              xmlrpc_env       *env)
{
    super_transaction_script (st, st->pre_scripts, env);
}

void
super_transaction_post_script (SuperTransaction *st,
                               xmlrpc_env       *env)
{
    super_transaction_script (st, st->post_scripts, env);
}
