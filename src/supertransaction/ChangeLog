2005-03-30  James Willcox  <james@ximian.com>

	* st-transaction.c: (super_transaction_transact):

	Block recurring actions during supertransactions

2004-11-03  Tambet Ingo  <tambet@ximian.com>

	* rcd-rpc-st.c (super_transaction_from_xmlrpc_value): Don't use time_t
	directly when getting values from xmlrpc_value.

2004-07-29  Tambet Ingo  <tambet@ximian.com>

	* rcd-rpc-st.c (super_transact_register): Print out the transaction XML
	so it's better to point fingers.

2004-07-28  Tambet Ingo  <tambet@ximian.com>

	* rcd-rpc-st.c: If patch support is enabled, log that.

2004-07-27  Tambet Ingo  <tambet@ximian.com>

	* rcd-rpc-st.c: Include config.h. Doh!

2004-07-12  Tambet Ingo  <tambet@ximian.com>

	* rcd-rpc-st.c (super_transaction_first): Add interval to recurring
	transactions start time (in case it's in past) to make them more
	scalable. Fixes #60711.

2004-06-21  James Willcox  <james@ximian.com>

	* rcd-rpc-st.c: (super_transaction_start):

	Put some #ifdefs and stuff in to fix build on non-yast-enabled
	platforms.

2004-06-10  Tambet Ingo  <tambet@ximian.com>

	* st-patch.[ch]: New files to support YOU patches.

	* rcd-rpc-st.c (super_transaction_free): Free patches.
	(super_transaction_start): If transation has patches,
	only install patches.
	(super_transaction_from_xmlrpc_value): Add support fir patches.
	(super_transaction_verify): Make sure the transaction doesn't
	have both patches and packages to install/remove.

	* Makefile.am: Conditionally build patch support.

2004-04-19  James Willcox  <james@ximian.com>

	* rcd-rpc-st.c: (super_transaction_refresh):

	Avoid bailing out if rc_world_refresh() returns NULL.

2004-04-01  Tambet Ingo  <tambet@ximian.com>

	* st-rollback.c (super_transaction_rollback): Send a better
	error message if rollback is not enabled.

2004-03-31  Tambet Ingo  <tambet@ximian.com>

	* st-transaction.c (resolve_deps): Succeed if "allow_removals" is set.

	* rcd-rpc-st.c (super_transaction_from_xmlrpc_value): Add
	"allow_removals" flag.

2004-01-23  Joe Shaw  <joe@ximian.com>

	* st-transaction.c (resolve_deps): We still need the cleanup label.

2004-01-23  Tambet Ingo  <tambet@ximian.com>

	* st-transaction.c (resolve_deps): Fix package lists memory problems.

2004-01-20  Tambet Ingo  <tambet@ximian.com>

	* rcd-rpc-st.c (super_transaction_first): Change the first run logic.

2004-01-16  Tambet Ingo  <tambet@ximian.com>

	* st-transaction.c (get_removal_failure_info): Fix a stupid logic
	bug. Fixes #52391.

2003-11-10  Tambet Ingo  <tambet@ximian.com>

	* rcd-rpc-st.c: Implement reference counting.

	* st-script.c (super_transaction_script): Be VERY careful.

	* rcd-rpc-st.c (super_transaction_refresh): Start refreshing only
	when it's going to succeed.

2003-10-31  Joe Shaw  <joe@ximian.com>

	* rcd-rpc-st.c (rcd_module_load): Update calls to notify_rce_add()
	and notify_rce_remove() for new API.

2003-10-31  Tambet Ingo  <tambet@ximian.com>

	* st-transaction.c (log_xmlrpc_fault): Implement.
	(log_xmlrpc_fault_sent): Implement.
	(fault_to_log_struct): Implement.
	(super_transaction_transact): Log error if necessary.

	* rcd-rpc-st.c (super_transaction_refresh): Log error on failure.

2003-10-29  Tambet Ingo  <tambet@ximian.com>

	* rcd-rpc-st.c (rcd_module_load): Use rce-helpers to do the dirty work.

	* st-transaction.c (rc_package_slist_add_pkg): Take the ownership of
	package before putting it to list.

2003-10-20  Tambet Ingo  <tambet@ximian.com>

	* rcd-rpc-st.c (super_transact_register): Set xmlrpc fault instead
	of directly logging it.
	(super_transaction_refresh): Implement.
	(super_transaction_execute): Refresh before executing the transaction.
	(subworld_added_cb): Don't do anything if we have started a refresh.
	(subworld_removed_cb): Ditto.

	* st-transaction.c: Moved xmlrpc parsing of packages and channels
	here, doing it right before the actual transaction now.

2003-10-01  Tambet Ingo  <tambet@ximian.com>

	* rcd-rpc-st.c (super_transact_register): Complain on errors.
	(parse_packages_to_install): Split up functions to parse packages
	to install and to remove.
	(parse_packages_to_remove): Implement.

2003-09-29  Tambet Ingo  <tambet@ximian.com>

	* rcd-rpc-st.c: Remove all transactions saving/restoring from
	supertransactions.
	(super_transaction_verify): Implement.
	(super_transact_register): Don't believe everything you read!
	(super_transaction_register_from_xmlrpc_array): Ditto.
	(super_transaction_error_quark): Implement.

2003-09-27  Tambet Ingo  <tambet@ximian.com>

	* st-rollback.c (super_transaction_rollback): Don't start transaction
	if there's nothing to install or remove.

	* st-transaction.c (add_channel_update_pkgs): Get rcd_module correctly.
	(add_channel_update_pkgs): Ditto.
	(resolve_deps): Ditto.
	(resolve_deps): Ditto.
	(super_transaction_transact): Don't start transaction if there's
	nothing to install or remove.

	* rcd-rpc-st.c (super_transaction_module): Helper function to get
	rcd_module variable.
	(super_transaction_execute): Block RCDRecurring while executing.
	(super_transaction_new): Add service_id field.
	(super_transaction_unregister_by_service): Implement.
	(super_transaction_register_from_xmlrpc_array): Implement.
	(register_super_transactions_for_service): Implement.
	(subworld_added_cb): Implement.
	(subworld_removed_cb): Implement.
	(connect_world_activated): Implement.
	(rcd_module_load): Listen to "subworld_added", "subworld_removed", and
	"activated" signals and update pending supertransactions accordingly.

2003-09-25  Joe Shaw  <joe@ximian.com>

	* st-rollback.c (synth_package_from_update): Don't reset the
	channel, we'll need it later.

2003-09-25  Tambet Ingo  <tambet@ximian.com>

	* Makefile.am (librcd_st_la_SOURCES): Add missing file.

	* rcd-rpc-st.c: Rewrite. Don't use XML-RPC interfaces
	of rcd, just call C functions. Resolve dependencies.

2003-09-22  Tambet Ingo  <tambet@ximian.com>

	* rcd-rpc-st.c (add_channel_updates_cb): Comapre string
	values, not their addresses, doh!

2003-09-19  Tambet Ingo  <tambet@ximian.com>

	* rcd-rpc-st.c (run_script): Change argument order to match
	expected.

2003-09-17  Tambet Ingo  <tambet@ximian.com>

	* rcd-rpc-st.c (super_transact_run_scripts): Take scripts
	in format (sid, content).
	(run_script): Pass on sid.
