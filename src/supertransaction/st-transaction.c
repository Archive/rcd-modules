/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */

#include "st-transaction.h"
#include <rcd-rpc-util.h>
#include <rcd-xmlrpc.h>


static xmlrpc_value *
fault_to_log_struct (xmlrpc_env *env, SuperTransaction *st, xmlrpc_env *fault)
{
    xmlrpc_value *value = NULL;

    /* Common part for all logs */
    value = xmlrpc_struct_new (env);
    XMLRPC_FAIL_IF_FAULT (env);

    RCD_XMLRPC_STRUCT_SET_STRING (env, value, "trid", st->trid);
    XMLRPC_FAIL_IF_FAULT (env);

    RCD_XMLRPC_STRUCT_SET_INT (env, value, "endtime", time(NULL));
    XMLRPC_FAIL_IF_FAULT (env);

    RCD_XMLRPC_STRUCT_SET_STRING (env, value, "client", st->client_id);
    XMLRPC_FAIL_IF_FAULT (env);

    RCD_XMLRPC_STRUCT_SET_STRING (env, value, "version", st->client_version);
    XMLRPC_FAIL_IF_FAULT (env);

    RCD_XMLRPC_STRUCT_SET_INT (env, value, "status", 0);
    XMLRPC_FAIL_IF_FAULT (env);

    RCD_XMLRPC_STRUCT_SET_STRING (env, value, "message",
                                  fault->fault_string);
    XMLRPC_FAIL_IF_FAULT (env);

cleanup:
    if (env->fault_occurred && value != NULL) {
        xmlrpc_DECREF (value);
        value = NULL;
    }

    return value;
}

static void
log_xmlrpc_fault_sent (char *server_url,
                       char *method_name,
                       xmlrpc_value *param_array,
                       void *user_data,
                       xmlrpc_env *fault,
                       xmlrpc_value *result)
{
    if (fault->fault_occurred)
        rcd_module_debug (RCD_DEBUG_LEVEL_WARNING,
                          super_transaction_module (),
                          "Unable to send log to '%s': %s",
                          server_url, fault->fault_string);
    else
        rcd_module_debug (RCD_DEBUG_LEVEL_DEBUG,
                          super_transaction_module (),
                          "Successfully sent log to '%s'",
                          server_url);
}

void
log_xmlrpc_fault (SuperTransaction *st, xmlrpc_env *fault)
{
    xmlrpc_env env;
    xmlrpc_value *value;
    xmlrpc_value *params;

    xmlrpc_env_init (&env);
    value = fault_to_log_struct (&env, st, fault);
    XMLRPC_FAIL_IF_FAULT (&env);

    params = xmlrpc_build_value (&env, "(V)", value);
    xmlrpc_DECREF (value);
    XMLRPC_FAIL_IF_FAULT (&env);

    rcd_xmlrpc_client_foreach_host (TRUE, "rcserver.transaction.log",
                                    log_xmlrpc_fault_sent,
                                    NULL,
                                    params);
    xmlrpc_DECREF (params);

cleanup:
    xmlrpc_env_clean (&env);
}

typedef struct {
    RCPackageSList **dest;
    RCWorld *world;
    GSList *channels;
} AddChannelPkgsInfo;

static void
rc_package_slist_add_pkg (AddChannelPkgsInfo *info,
                          RCPackage *pkg)
{
    *info->dest = g_slist_prepend (*info->dest, rc_package_ref (pkg));
}

static gboolean
add_channel_install_cb (RCPackage *pkg, gpointer user_data)
{
    AddChannelPkgsInfo *info = user_data;
    const char *name;

    name = rc_package_get_name (pkg);

    /* When we install a whole channel, we skip packages where
       a package of the same name is already installed on the system */

    if (rc_world_get_package (info->world,
                              RC_CHANNEL_SYSTEM,
                              name) == NULL)
        rc_package_slist_add_pkg (info, pkg);

    return TRUE;
}

static void
add_channel_install_pkgs (xmlrpc_env      *env,
                          GSList          *channels,
                          RCPackageSList **pkgs)
{
    AddChannelPkgsInfo info;
    RCChannel *ch;
    char *cid;
    GSList *iter;

    info.dest = pkgs;
    info.world = rc_get_world ();

    for (iter = channels; iter != NULL; iter = iter->next) {
        cid = iter->data;

        ch = rc_world_get_channel_by_id (info.world, cid);
        if (ch)
            rc_world_foreach_package (info.world, ch,
                                      add_channel_install_cb,
                                      &info);
        else {
            rcd_module_debug (RCD_DEBUG_LEVEL_ERROR,
                              super_transaction_module (),
                              "Can not find channel '%s'", cid);
            xmlrpc_env_set_fault_formatted (env, RCD_RPC_FAULT_INVALID_CHANNEL,
                                            "Can not find channel '%s'", cid);
        }
    }
}

static void
add_channel_updates_cb (RCPackage *old,
                        RCPackage *nuevo,
                        gpointer   user_data)
{
    AddChannelPkgsInfo *info = user_data;
    GSList *iter;
    gboolean found = FALSE;

    for (iter = info->channels; iter != NULL; iter = iter->next) {
        const char *cid = iter->data;
        if (strcmp (cid, rc_channel_get_id (nuevo->channel)) == 0) {
            found = TRUE;
            break;
        }
    }

    if (found)
        rc_package_slist_add_pkg (info, nuevo);
}

static void
add_channel_update_pkgs (GSList *channels, RCPackageSList **pkgs)
{
    AddChannelPkgsInfo info;
    RCChannel *ch;
    char *cid;
    GSList *iter;

    info.dest = pkgs;
    info.world = rc_get_world ();
    info.channels = NULL;

    for (iter = channels; iter != NULL; iter = iter->next) {
        cid = iter->data;

        ch = rc_world_get_channel_by_id (info.world, cid);
        if (ch)
            info.channels = g_slist_append (info.channels, cid);
        else
            rcd_module_debug (RCD_DEBUG_LEVEL_WARNING,
                              super_transaction_module (),
                              "Can not find channel '%s'", cid);
    }

    if (info.channels)
        rc_world_foreach_system_upgrade (info.world,
                                         FALSE,
                                         add_channel_updates_cb,
                                         &info);

    if (info.channels)
        g_slist_free (info.channels);
}

static GSList *
parse_channels (xmlrpc_env *env, xmlrpc_value *array)
{
    int i, len;
    xmlrpc_value *tmp;
    GSList *list = NULL;

    len = xmlrpc_array_size (env, array);
    XMLRPC_FAIL_IF_FAULT (env);

    for (i = 0; i < len; i++) {
        char *sid;

        tmp = xmlrpc_array_get_item (env, array, i);
        XMLRPC_FAIL_IF_FAULT (env);

        xmlrpc_parse_value (env, tmp, "s", &sid);
        XMLRPC_FAIL_IF_FAULT (env);

        list = g_slist_prepend (list, g_strdup (sid));
    }

cleanup:
    if (env->fault_occurred && list != NULL) {
        g_slist_foreach (list, (GFunc) g_free, NULL);
        g_slist_free (list);
        list = NULL;
    }

    return list;
}

static RCPackageSList *
super_transact_construct_installs (xmlrpc_env *env, SuperTransaction *st)
{
    GSList *install_channels = NULL;
    GSList *update_channels = NULL;
    RCPackageSList *pkgs = NULL;

    /* Packages to install */
    pkgs = rcd_xmlrpc_array_to_rc_package_slist (
        st->install, env,
        RCD_PACKAGE_FROM_FILE | RCD_PACKAGE_FROM_STREAMED_PACKAGE |
        RCD_PACKAGE_FROM_XMLRPC_PACKAGE);
    XMLRPC_FAIL_IF_FAULT (env);
    
    /* Packages from channels to install */
    install_channels = parse_channels (env, st->install_channels);
    XMLRPC_FAIL_IF_FAULT (env);

    add_channel_install_pkgs (env, install_channels, &pkgs);
    XMLRPC_FAIL_IF_FAULT (env);

    /* Packages from channels to update */
    update_channels = parse_channels (env, st->update_channels);
    XMLRPC_FAIL_IF_FAULT (env);

    add_channel_update_pkgs (update_channels, &pkgs);

cleanup:
    g_slist_foreach (install_channels, (GFunc) g_free, NULL);
    g_slist_free (install_channels);

    g_slist_foreach (update_channels, (GFunc) g_free, NULL);
    g_slist_free (update_channels);

    if (env->fault_occurred) {
        rc_package_slist_unref (pkgs);
        g_slist_free (pkgs);
        pkgs = NULL;
    }

    return pkgs;
}

static RCPackageSList *
super_transact_construct_removals (xmlrpc_env *env, SuperTransaction *st)
{
    RCPackageSList *pkgs = NULL;

    pkgs = rcd_xmlrpc_array_to_rc_package_slist (
        st->remove, env,
        RCD_PACKAGE_FROM_NAME | RCD_PACKAGE_FROM_XMLRPC_PACKAGE);

    return pkgs;
}

/*****************************************************************************/

static void
append_dep_info (RCResolverInfo *info, gpointer user_data)
{
    GString *dep_failure_info = user_data;
    gboolean debug = FALSE;

    if (getenv ("RCD_DEBUG_DEPS"))
        debug = TRUE;

    if (debug || rc_resolver_info_is_important (info)) {
        char *msg = rc_resolver_info_to_string (info);

        g_string_append_printf (
            dep_failure_info, "\n%s%s%s",
            (debug && rc_resolver_info_is_error (info)) ? "ERR " : "",
            (debug && rc_resolver_info_is_important (info)) ? "IMP " : "",
            msg);
        g_free (msg);
    }
} /* append_dep_info */

static char *
dep_get_failure_info (RCResolver *resolver)
{
    RCResolverQueue *queue;
    GString *dep_failure_info = g_string_new ("Unresolved dependencies:\n");
    char *str;

    /* FIXME: Choose a best invalid queue */
    queue = (RCResolverQueue *) resolver->invalid_queues->data;

    rc_resolver_context_foreach_info (queue->context, NULL, -1,
                                      append_dep_info, dep_failure_info);

    str = dep_failure_info->str;

    g_string_free (dep_failure_info, FALSE);

    return str;
} /* dep_get_failure_info */

static void
prepend_pkg (RCPackage *pkg, RCPackageStatus status, gpointer user_data)
{
    GSList **slist = user_data;

    if (rc_package_status_is_to_be_installed (status) ||
        (rc_package_status_is_to_be_uninstalled (status)
         && rc_package_is_installed (pkg))) {

        *slist = g_slist_prepend (*slist, rc_package_ref (pkg));
    } 
} /* prepend_pkg */

static void
prepend_pkg_pair (RCPackage *pkg_to_add,
                  RCPackageStatus status_to_add,
                  RCPackage *pkg_to_remove,
                  RCPackageStatus status_to_remove, 
                  gpointer user_data)
{
    GSList **slist = user_data;
    
    *slist = g_slist_prepend (*slist, rc_package_ref (pkg_to_add));

    /* We don't need to do the removal part of the upgrade */
} /* prepend_pkg_pair */

static void
rc_package_slists_merge (RCPackageSList **packages,
                         RCPackageSList *extras)
{
    GHashTable *hash;
    GSList *iter;

    hash = g_hash_table_new (rc_package_spec_hash,
                             rc_package_spec_equal);

    for (iter = *packages; iter != NULL; iter = iter->next)
        g_hash_table_insert (hash, iter->data, iter->data);

    for (iter = extras; iter != NULL; iter = iter->next)
        if (!g_hash_table_lookup (hash, iter->data)) {
            g_hash_table_insert (hash, iter->data, iter->data);
            *packages = g_slist_prepend (*packages, iter->data);
        }

    g_hash_table_destroy (hash);
}

static gchar *
get_removal_failure_info (RCPackageSList *required,
                          RCPackageSList *specified)
{
    GHashTable *hash;
    gchar *str;
    GSList *iter;
    GString *info = g_string_new ("This transaction requires the "
                                  "removal of the following packages:");

    hash = g_hash_table_new (rc_package_spec_hash,
                             rc_package_spec_equal);

    for (iter = specified; iter != NULL; iter = iter->next)
        g_hash_table_insert (hash, iter->data, iter->data);

    for (iter = required; iter != NULL; iter = iter->next)
        if (!g_hash_table_lookup (hash, iter->data))
            g_string_append_printf (info, "\n%s",
                                    rc_package_spec_to_str_static (RC_PACKAGE_SPEC (iter->data)));

    g_hash_table_destroy (hash);
    str = info->str;
    g_string_free (info, FALSE);

    return str;
}

static void
resolve_deps (xmlrpc_env      *env,
              RCPackageSList **install_packages,
              RCPackageSList **remove_packages,
              gboolean         allow_removals)
{
    RCResolver *resolver;
    RCPackageSList *extra_install_packages = NULL;
    RCPackageSList *extra_remove_packages = NULL;
    RCPackageSList *specified_removals = NULL;

    resolver = rc_resolver_new ();

    rc_resolver_add_packages_to_install_from_slist (resolver,
                                                    *install_packages);
    rc_resolver_add_packages_to_remove_from_slist (resolver,
                                                   *remove_packages);

    rc_resolver_resolve_dependencies (resolver);

    if (!resolver->best_context) {
        gchar *dep_failure_info;

        rcd_module_debug (RCD_DEBUG_LEVEL_WARNING,
                          super_transaction_module (),
                          "Resolution failed!");

        dep_failure_info = dep_get_failure_info (resolver);
        xmlrpc_env_set_fault (env, RCD_RPC_FAULT_FAILED_DEPENDENCIES,
                              dep_failure_info);
        g_free (dep_failure_info);
        goto cleanup;
    }

    rc_resolver_context_foreach_install (resolver->best_context,
                                         prepend_pkg,
                                         &extra_install_packages);
    rc_resolver_context_foreach_uninstall (resolver->best_context,
                                           prepend_pkg,
                                           &extra_remove_packages);
    rc_resolver_context_foreach_upgrade (resolver->best_context,
                                         prepend_pkg_pair,
                                         &extra_install_packages);

    specified_removals = g_slist_copy (*remove_packages);
    rc_package_slists_merge (remove_packages, extra_remove_packages);
    rc_package_slists_merge (install_packages, extra_install_packages);

    if (!allow_removals &&
        g_slist_length (*remove_packages) >
        g_slist_length (specified_removals)) {
        gchar *failure_info;

        rcd_module_debug (RCD_DEBUG_LEVEL_WARNING,
                          super_transaction_module (),
                          "This transaction requires removal of packages");
        failure_info = get_removal_failure_info (*remove_packages,
                                                 specified_removals);
        
        xmlrpc_env_set_fault (env, RCD_RPC_FAULT_FAILED_DEPENDENCIES,
                              failure_info);
        g_free (failure_info);
    }

cleanup:
    rc_resolver_free (resolver);

    if (extra_install_packages)
        g_slist_free (extra_install_packages);

    if (extra_remove_packages)
        g_slist_free (extra_remove_packages);

    if (specified_removals)
        g_slist_free (specified_removals);
}

void
super_transaction_transact (SuperTransaction *st,
                            xmlrpc_env       *env)
{
    RCDTransaction *transaction;
    RCPending *download_pending, *transaction_pending;
    GSList *pending_list;
    RCDIdentity *identity;
    RCPackageSList *install = NULL;
    RCPackageSList *remove = NULL;

    rcd_recurring_block ();

    install = super_transact_construct_installs (env, st);
    if (env->fault_occurred) {
        log_xmlrpc_fault (st, env);
        goto cleanup;
    }

    remove = super_transact_construct_removals (env, st);
    if (env->fault_occurred) {
        log_xmlrpc_fault (st, env);
        goto cleanup;
    }

    resolve_deps (env, &install, &remove, st->allow_removals);
    if (env->fault_occurred) {
        log_xmlrpc_fault (st, env);
        goto cleanup;
    }

    if (g_slist_length (install) == 0 &&
        g_slist_length (remove) == 0) {
        goto cleanup;
    }

    transaction = rcd_transaction_new ();
    rcd_transaction_set_id (transaction, st->trid);
    rcd_transaction_set_install_packages (transaction, install);

    rcd_transaction_set_remove_packages (transaction, remove);
    rcd_transaction_set_flags (transaction, st->flags);

    identity = rcd_identity_new ();
    identity->username = g_strdup ("root");
    identity->privileges = rcd_privileges_from_string ("superuser");

    rcd_transaction_set_client_info (transaction,
                                     st->client_id,
                                     st->client_version,
                                     "localhost",
                                     identity);

    rcd_identity_free (identity);

    rcd_transaction_begin (transaction);

    download_pending = rcd_transaction_get_download_pending (transaction);
    transaction_pending =
        rcd_transaction_get_transaction_pending (transaction);

    g_object_unref (transaction);

    pending_list = NULL;
    if (download_pending)
        pending_list = g_slist_prepend (pending_list, download_pending);

    if (transaction_pending)
        pending_list = g_slist_prepend (pending_list, transaction_pending);

    if (pending_list) {
        rcd_rpc_block_on_pending_list (env, pending_list, TRUE,
                                       RCD_RPC_FAULT_TRANSACTION_FAILED);
    }

    g_slist_free (pending_list);

cleanup:
    rcd_recurring_allow ();

    if (install) {
        rc_package_slist_unref (install);
        g_slist_free (install);
    }

    if (remove) {
        rc_package_slist_unref (remove);
        g_slist_free (remove);
    }
}

/*****************************************************************************/
