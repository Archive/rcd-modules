/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */

#include <config.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <xmlrpc.h>
#include <rcd-module.h>
#include <rcd-prefs.h>
#include <rcd-recurring.h>
#include <rcd-transaction.h>
#include <libredcarpet.h>
#include <rcd-rpc.h>
#include <rcd-world-remote.h>
#include <rcd-rpc-util.h>
#include <rcd-xmlrpc.h>

#include "rcd-rpc-st.h"
#include "st-script.h"
#include "st-rollback.h"
#include "st-transaction.h"
#include "rce-helpers.h"

#ifdef HAVE_YAST
#include "st-patch.h"
#endif

int rcd_module_major_version = RCD_MODULE_MAJOR_VERSION;
int rcd_module_minor_version = RCD_MODULE_MINOR_VERSION;

static RCDModule *rcd_module = NULL;

#define ST_ERROR super_transaction_error_quark()

static GQuark
super_transaction_error_quark (void)
{
    static GQuark quark;

    if (!quark)
        quark = g_quark_from_static_string ("supertransaction_error");

    return quark;
}

RCDModule *
super_transaction_module (void)
{
    return rcd_module;
}

/*****************************************************************************/

static void
super_transaction_free (SuperTransaction *st)
{
    if (st == NULL)
        return;

    g_free (st->name);
    g_free (st->trid);
    g_free (st->client_id);
    g_free (st->client_version);
    g_free (st->service_id);

    if (st->pre_scripts)
        xmlrpc_DECREF (st->pre_scripts);
    if (st->post_scripts)
        xmlrpc_DECREF (st->post_scripts);

    if (st->install)
        xmlrpc_DECREF (st->install);

    if (st->remove)
        xmlrpc_DECREF (st->remove);

    if (st->install_channels)
        xmlrpc_DECREF (st->install_channels);

    if (st->update_channels)
        xmlrpc_DECREF (st->update_channels);

    if (st->patches)
        xmlrpc_DECREF (st->patches);

    g_free (st);
}

static void
super_transaction_unref (SuperTransaction *st)
{
    g_assert (st->ref_count > 0);

    st->ref_count--;
    if (st->ref_count == 0 && !getenv ("RCD_DONT_FREE_ST"))
        super_transaction_free(st);
}

static SuperTransaction *
super_transaction_ref (SuperTransaction *st)
{
    st->ref_count++;
    return st;
}

static void
super_transaction_destroy (RCDRecurring *rec)
{
    SuperTransaction *st = (SuperTransaction *) rec;

    super_transaction_unref (st);
}

static gchar *
super_transaction_label (RCDRecurring *rec)
{
    SuperTransaction *st = (SuperTransaction *) rec;

    return g_strdup_printf ("supertransaction '%s'", st->name);
}

static PrePositionStatus
super_transaction_pre_position (SuperTransaction *st)
{
    xmlrpc_env env;
    RCDTransactionFlags saved_flags;
    PrePositionStatus status = PP_STATUS_FAILED;

    xmlrpc_env_init (&env);

    saved_flags = st->flags;
    st->flags |= RCD_TRANSACTION_FLAGS_DOWNLOAD_ONLY;
    super_transaction_transact (st, &env);
    st->flags = saved_flags;

    if (env.fault_occurred) {
        rcd_module_debug (RCD_DEBUG_LEVEL_WARNING, rcd_module,
                          "'%s' pre-position failed: %s",
                          st->name, env.fault_string);
        xmlrpc_env_clean (&env);
    } else
        status = PP_STATUS_SUCCEEDED;

    return status;
}

static void
super_transaction_start (SuperTransaction *st)
{
    xmlrpc_env env;
    int patch_count;

    xmlrpc_env_init (&env);

    patch_count = xmlrpc_array_size (&env, st->patches);
    if (patch_count > 0) {
#ifdef HAVE_YAST
        super_transaction_install_patches (st, &env);
        XMLRPC_FAIL_IF_FAULT (&env);
#else
        rcd_module_debug (RCD_DEBUG_LEVEL_DEBUG, rcd_module,
                          "Transaction '%s' has patches but rcd-modules was"
                          " not compiled with patch support.",
                          st->name);
        goto cleanup;
#endif
    } else {
        /* PRE script */
        super_transaction_pre_script (st, &env);
        XMLRPC_FAIL_IF_FAULT (&env);

        /* Rollback */
        super_transaction_rollback (st, &env);
        XMLRPC_FAIL_IF_FAULT (&env);

        /* "Real" transaction */
        super_transaction_transact (st, &env);
        XMLRPC_FAIL_IF_FAULT (&env);

        /* POST script */
        super_transaction_post_script (st, &env);
        XMLRPC_FAIL_IF_FAULT (&env);
    }

    rcd_module_debug (RCD_DEBUG_LEVEL_DEBUG, rcd_module,
                      "'%s' successfully executed", st->name);

cleanup:
    if (env.fault_occurred)
        rcd_module_debug (RCD_DEBUG_LEVEL_WARNING, rcd_module,
                          "'%s' failed: %s",
                          st->name,
                          env.fault_string);

    xmlrpc_env_clean (&env);
}

static gboolean
super_transaction_refresh (SuperTransaction *st)
{
    xmlrpc_env env;
    RCWorld *world;
    RCPending *pending;
    GSList *pending_list;

    xmlrpc_env_init (&env);

    world = rc_get_world ();
    pending = rc_world_refresh (world);
    if (pending) {
        pending_list = g_slist_prepend (NULL, pending);

        rcd_rpc_block_on_pending_list (&env, pending_list, FALSE,
                                       RCD_RPC_FAULT_CANT_REFRESH);
        g_slist_free (pending_list);
    } 

    if (env.fault_occurred) {
        rcd_module_debug (RCD_DEBUG_LEVEL_ERROR, rcd_module,
                          "'%s' refresh failed: %s",
                          st->name, env.fault_string);
        log_xmlrpc_fault (st, &env);
        xmlrpc_env_clean (&env);
        return FALSE;
    }

    return TRUE;
}

static void
super_transaction_execute (RCDRecurring *rec)
{
    SuperTransaction *st = (SuperTransaction *) rec;

    rcd_recurring_block ();
    super_transaction_ref (st);

    if (super_transaction_refresh (st)) {
        if (st->pp_status == PP_STATUS_PENDING)
            st->pp_status = super_transaction_pre_position (st);
        else
            super_transaction_start (st);
    }

    super_transaction_unref (st);
    rcd_recurring_allow ();
}

static time_t
super_transaction_first (RCDRecurring *rec, time_t now)
{
    SuperTransaction *st = (SuperTransaction *) rec;
    time_t first = st->first_run;

    if (st->pp_status == PP_STATUS_PENDING)
        return st->pp_time;

    /*
      Start time:

      0       right now
      past    now + rand(15)
      future  specified time + rand(15)
      recurring (start in past) - now + interval + rand(15)
    */

    if (first == 0)
        return 0;

    if (first < 0)
        first = now + st->interval;

    if (st->interval == 0 || st->interval > 14*60)
        first += random () % (14 * 60);

    return first;
}

static time_t
super_transaction_next (RCDRecurring *rec, time_t previous)
{
    SuperTransaction *st = (SuperTransaction *) rec;

    /* Pre-position has just succeeded
       next is transaction time */
    if (st->pp_status == PP_STATUS_SUCCEEDED) {
        st->pp_status = PP_STATUS_NONE;
        return st->first_run;
    }

    /* Interval not set, there's no next time! */
    if (st->interval < 1)
        return 0;

    /* Pre-position failed, but we have a recurring transaction.
       next is transaction time */
    if (st->pp_status == PP_STATUS_FAILED) {
        st->pp_status = PP_STATUS_NONE;
        return st->first_run;
    }

    /* Just a regular recurring transaction */
    st->first_run = previous + st->interval;
    return st->first_run;
}

static SuperTransaction *
super_transaction_new (const char *name, const char *trid,
                       const char *client, const char *client_version)
{
    SuperTransaction *st;
    gchar *service_id, *tmp;

    tmp = strchr (trid, '|');
	if (tmp != NULL)
        service_id = g_strndup (trid, tmp - trid);
    else
        return NULL;

    st = g_new0 (SuperTransaction, 1);

    st->ref_count         = 1;
    st->name              = g_strdup (name);
    st->trid              = g_strdup (trid);
    st->client_id         = g_strdup (client);
    st->client_version    = g_strdup (client_version);
    st->service_id        = service_id;
    st->allow_removals    = FALSE;

    st->recurring.tag     = g_quark_from_static_string ("supertransaction");

    st->recurring.destroy = super_transaction_destroy;
    st->recurring.label   = super_transaction_label;
    st->recurring.execute = super_transaction_execute;
    st->recurring.first   = super_transaction_first;
    st->recurring.next    = super_transaction_next;

    st->pp_status         = PP_STATUS_NONE;

    return st;
}

typedef struct {
    const char *needle_trid;
    SuperTransaction *needle;
} SuperTransactionSearchInfo;

static void
registered_cb (SuperTransaction *st, gpointer user_data)
{
    SuperTransactionSearchInfo *info = user_data;

    if (strcmp (st->trid, info->needle_trid) == 0)
        info->needle = st;
}

static SuperTransaction *
super_transaction_find_by_id (const char *trid)
{
    SuperTransactionSearchInfo info;

    info.needle_trid = trid;
    info.needle = NULL;

    rcd_recurring_foreach (g_quark_from_static_string ("supertransaction"),
                           (RCDRecurringFn) registered_cb,
                           &info);
    return info.needle;
}

static void
super_transaction_register_recurring (SuperTransaction *st)
{
    SuperTransaction *old_st;

    old_st = super_transaction_find_by_id (st->trid);
    if (old_st)
        rcd_recurring_remove ((RCDRecurring *) old_st);

    rcd_recurring_add ((RCDRecurring *) st);
}

static void
super_transaction_unregister_recurring (SuperTransaction *st)
{
    rcd_recurring_remove ((RCDRecurring *) st);
}

static void
unregister_by_service_cb (SuperTransaction *st, gpointer user_data)
{
    const char *service_id = user_data;

    if (strcmp (st->service_id, service_id) == 0)
        super_transaction_unregister_recurring (st);
}

static void
super_transaction_unregister_by_service (const char *service_id)
{
    rcd_recurring_foreach (g_quark_from_static_string ("supertransaction"),
                           (RCDRecurringFn) unregister_by_service_cb,
                           (char *) service_id);
}

/*****************************************************************************/

static xmlrpc_value *
rcd_xmlrpc_struct_get_array (xmlrpc_env   *env,
                             xmlrpc_value *value,
                             const gchar  *key)
{
    xmlrpc_value *array;

    if (xmlrpc_struct_has_key (env, value, (char *) key)) {
        array = xmlrpc_struct_get_value (env, value, (char *) key);
        if (xmlrpc_value_type (array) == XMLRPC_TYPE_ARRAY)
            xmlrpc_INCREF (array);
        else
            xmlrpc_env_set_fault(env, RCD_RPC_FAULT_INVALID_STREAM_TYPE,
                                 "Invalid package stream type");
    } else
        array = xmlrpc_build_value (env, "()");

    return array;
}

static SuperTransaction *
super_transaction_from_xmlrpc_value (xmlrpc_env *env, xmlrpc_value *value)
{
    SuperTransaction *st = NULL;
    xmlrpc_int32 when;
    gchar *name, *trid, *client, *client_version;

    if (xmlrpc_value_type (value) != XMLRPC_TYPE_STRUCT) {
        xmlrpc_env_set_fault(env, RCD_RPC_FAULT_INVALID_STREAM_TYPE,
                             "Invalid package stream type");
        return NULL;
    }

    xmlrpc_parse_value (env, value, "{s:s,s:s,s:s,s:s,*}",
                        "trid",    &trid,
                        "name",    &name,
                        "client",  &client,
                        "version", &client_version);
    XMLRPC_FAIL_IF_FAULT (env);

    st = super_transaction_new (name, trid, client, client_version);
    if (st == NULL) {
        xmlrpc_env_set_fault (env, RCD_RPC_FAULT_INVALID_STREAM_TYPE,
                              "Invalid package stream type");
        goto cleanup;
    }

    if (xmlrpc_struct_has_key (env, value, "starttime")) {
        RCD_XMLRPC_STRUCT_GET_INT (env, value, "starttime", when);
        XMLRPC_FAIL_IF_FAULT (env);
        st->first_run = (time_t) when;
    }

    if (xmlrpc_struct_has_key (env, value, "frequency")) {
        RCD_XMLRPC_STRUCT_GET_INT (env, value, "frequency", when);
        XMLRPC_FAIL_IF_FAULT (env);
        st->interval = (guint) when;
    }

    if (xmlrpc_struct_has_key (env, value, "pre_position")) {
        RCD_XMLRPC_STRUCT_GET_INT (env, value, "pre_position", when);
        XMLRPC_FAIL_IF_FAULT (env);
        st->pp_time = (time_t) when;

        /* Use pre-position only if it happens before transaction */
        if (st->pp_time < st->first_run)
            st->pp_status = PP_STATUS_PENDING;
    }

    st->pre_scripts = rcd_xmlrpc_struct_get_array (env, value, "pre_scripts");
    XMLRPC_FAIL_IF_FAULT (env);
    st->post_scripts = rcd_xmlrpc_struct_get_array (env, value, "post_scripts");
    XMLRPC_FAIL_IF_FAULT (env);

    st->install = rcd_xmlrpc_struct_get_array (env, value, "install_pkgs");
    XMLRPC_FAIL_IF_FAULT (env);

    st->remove = rcd_xmlrpc_struct_get_array (env, value, "remove_pkgs");
    XMLRPC_FAIL_IF_FAULT (env);

    st->install_channels = rcd_xmlrpc_struct_get_array (env, value,
                                                        "install_channels");
    XMLRPC_FAIL_IF_FAULT (env);

    st->update_channels = rcd_xmlrpc_struct_get_array (env, value,
                                                       "update_channels");
    XMLRPC_FAIL_IF_FAULT (env);

    st->patches = rcd_xmlrpc_struct_get_array (env, value,
                                               "patches");

    if (xmlrpc_struct_has_key (env, value, "dry_run")) {
        int dry_run;

        RCD_XMLRPC_STRUCT_GET_INT (env, value, "dry_run", dry_run);
        XMLRPC_FAIL_IF_FAULT (env);

        if (dry_run)
            st->flags |= RCD_TRANSACTION_FLAGS_DRY_RUN;
    }

    if (xmlrpc_struct_has_key (env, value, "rollback")) {
        RCD_XMLRPC_STRUCT_GET_INT (env, value, "rollback", st->rollback);
        XMLRPC_FAIL_IF_FAULT (env);
    }

    if (xmlrpc_struct_has_key (env, value, "allow_removals")) {
        RCD_XMLRPC_STRUCT_GET_INT (env, value, "allow_removals", st->allow_removals);
        XMLRPC_FAIL_IF_FAULT (env);
    }

cleanup:
    if (env->fault_occurred) {
        super_transaction_unref (st);
        st = NULL;
    }

    return st;
}

static void
super_transaction_verify (SuperTransaction *st,
                          xmlrpc_env       *env,
                          GError           **err)
{
    int pre_script_count;
    int post_script_count;
    int install_pkg_count;
    int remove_pkg_count;
    int install_channel_count;
    int update_channel_count;
    int pkg_actions;
    int script_actions;
    int patch_count;

    pre_script_count = xmlrpc_array_size (env, st->pre_scripts);
    post_script_count = xmlrpc_array_size (env, st->post_scripts);
    install_pkg_count = xmlrpc_array_size (env, st->install);
    remove_pkg_count = xmlrpc_array_size (env, st->remove);
    install_channel_count = xmlrpc_array_size (env, st->install_channels);
    update_channel_count = xmlrpc_array_size (env, st->update_channels);
    patch_count = xmlrpc_array_size (env, st->patches);

    /* Some helper variables */
    script_actions = pre_script_count + post_script_count;
    pkg_actions = install_pkg_count + remove_pkg_count +
        install_channel_count + update_channel_count;

    /* Make sure at least one "action" is present */
    if (script_actions == 0 &&
        pkg_actions == 0 &&
        patch_count == 0 &&
        st->rollback == 0) {
        g_set_error (err, ST_ERROR, ST_ERROR,
                     "Transaction '%s' has no actions, ignoring",
                     st->name);
        return;
    }

    /* If rollback is set, there shouldn't be any other
       install/remove actions
    */
    if ((st->rollback > 0 && pkg_actions > 0) ||
        (pkg_actions > 0 && st->rollback > 0)) {
        g_set_error (err, ST_ERROR, ST_ERROR,
                     "Transaction '%s' has rollback set "
                     "and has other actions, ignoring",
                     st->name);
        return;
    }

    /* If pre-position is set but nothing to isntall/remove */
    if (st->pp_time > 0 && pkg_actions == 0 && patch_count == 0) {
        g_set_error (err, ST_ERROR, ST_ERROR,
                     "Transaction '%s' has pre-position "
                     "but nothing to install/remove, ignoring",
                     st->name);
        return;
    }

    /* Recurring transactions can't have install/remove actions */
    if (st->interval > 0 &&
        (install_pkg_count > 0 ||
         remove_pkg_count > 0 ||
         patch_count > 0)) {
        g_set_error (err, ST_ERROR, ST_ERROR,
                     "Transaction '%s' is recurring and "
                     "has packages to install/remove, ignoring",
                     st->name);
        return;
    }

    /* Transactions can't have patches and packages */
    if (patch_count > 0 && pkg_actions > 0) {
        g_set_error (err, ST_ERROR, ST_ERROR,
                     "Transaction '%s' has both patches and "
                     "packagages to install/remove, ignoring",
                     st->name);
        return;
    }
}

/*****************************************************************************/

static xmlrpc_value *
super_transact_register (xmlrpc_env   *env,
                         xmlrpc_value *param_array,
                         void         *user_data)
{
    xmlrpc_value *value;
    SuperTransaction *st = NULL;
    GError *err = NULL;

    xmlrpc_parse_value (env, param_array, "(V)", &value);
    XMLRPC_FAIL_IF_FAULT (env);

    if (rcd_prefs_get_debug_level () == 6)
        rcd_debug_serialize (value);

    st = super_transaction_from_xmlrpc_value (env, value);
    XMLRPC_FAIL_IF_FAULT (env);

    super_transaction_verify (st, env, &err);
    if (err) {
        xmlrpc_env_set_fault (env, RCD_RPC_FAULT_INVALID_STREAM_TYPE,
                              err->message);
        g_error_free (err);
        goto cleanup;
    }

    super_transaction_register_recurring (st);

    return xmlrpc_build_value (env, "i", 0);

cleanup:
    if (st)
        super_transaction_unref (st);

    rcd_module_debug (RCD_DEBUG_LEVEL_ERROR, rcd_module,
                      "Can not register transaction: %s",
                      env->fault_string);

    return NULL;
}

static xmlrpc_value *
super_transact_unregister (xmlrpc_env   *env,
                           xmlrpc_value *param_array,
                           void         *user_data)
{
    char *trid;
    SuperTransaction *st;

    xmlrpc_parse_value (env, param_array, "(s)", &trid);
    XMLRPC_FAIL_IF_FAULT (env);

    st = super_transaction_find_by_id (trid);
    if (st) {
        rcd_recurring_remove ((RCDRecurring *) st);
        return xmlrpc_build_value (env, "i", 0);
    }
    else
        xmlrpc_env_set_fault_formatted (env, 1,
                                        "Supertransaction '%s' not found",
                                        trid);
cleanup:
    return NULL;
}

/*****************************************************************************/

static void
super_transaction_register_from_xmlrpc_array (xmlrpc_env *env,
                                              xmlrpc_value *array)
{
    int i, len;
    xmlrpc_value *value;
    SuperTransaction *st;
    GError *err = NULL;

    len = xmlrpc_array_size (env, array);
    XMLRPC_FAIL_IF_FAULT (env);

    for (i = 0; i < len; i++) {
        value = xmlrpc_array_get_item (env, array, i);
        XMLRPC_FAIL_IF_FAULT (env);

        st = super_transaction_from_xmlrpc_value (env, value);
        XMLRPC_FAIL_IF_FAULT (env);

        super_transaction_verify (st, env, &err);
        if (err) {
            rcd_module_debug (RCD_DEBUG_LEVEL_WARNING, rcd_module,
                              err->message);
            g_error_free (err);
            err = NULL;
            super_transaction_unref (st);
        } else
            super_transaction_register_recurring (st);
    }

cleanup:
    return;
}

static void
register_super_transactions_for_service (RCWorldService *world)
{
    xmlrpc_env env;
    xmlrpc_server_info *server;
    xmlrpc_value *array = NULL;

    xmlrpc_env_init (&env);
    server = rcd_xmlrpc_get_server (&env, world->url);
    XMLRPC_FAIL_IF_FAULT (&env);

    array = xmlrpc_client_call_server (&env, server,
                                       "rcserver.transaction.getAll",
                                       "()");
    XMLRPC_FAIL_IF_FAULT (&env);

    super_transaction_register_from_xmlrpc_array (&env, array);

cleanup:
    xmlrpc_server_info_free (server);    

    if (array)
        xmlrpc_DECREF (array);
    if (env.fault_occurred) {
        rcd_module_debug (RCD_DEBUG_LEVEL_ERROR, rcd_module,
                          "Error adding supertransactions to '%s': %s",
                          world->name, env.fault_string);
        xmlrpc_env_clean (&env);
    }
}

static void
services_added (GSList *services)
{
    GSList *iter;
    RCWorldService *service;

    for (iter = services; iter != NULL; iter = iter->next) {
        service = iter->data;
        register_super_transactions_for_service (service);
    }
}

static void
services_removed (GSList *services)
{
    GSList *iter;
    RCWorldService *service;

    for (iter = services; iter != NULL; iter = iter->next) {
        service = iter->data;
        super_transaction_unregister_by_service (service->unique_id);
    }
}

/*****************************************************************************/

void rcd_module_load (RCDModule *);

void
rcd_module_load (RCDModule *module)
{
    module->name = "rcd.supertransaction";
    module->description = "Server SuperTransaction";
    module->version = 0;
    module->interface_major = 0;
    module->interface_minor = 0;

    rcd_module = module;

#ifdef HAVE_YAST
    rcd_module_debug (RCD_DEBUG_LEVEL_INFO, rcd_module,
                      "YaST patch support is enabled");
#endif    

    rcd_rpc_register_method ("rcd.supertransaction.register",
                             super_transact_register,
                             "superuser", NULL);
    rcd_rpc_register_method ("rcd.supertransaction.unregister",
                             super_transact_unregister,
                             "superuser", NULL);

    notify_rce_add (services_added, FALSE, 5 * 1000);
    notify_rce_remove (services_removed, FALSE, 5 * 1000);
}

/*****************************************************************************/
