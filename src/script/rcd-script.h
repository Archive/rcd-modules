/* -*- Mode: C; tab-width: 4; c-basic-offset: 4; indent-tabs-mode: nil -*- */

/*
 * rcd-script.h
 *
 * Copyright (C) 2003 Ximian, Inc.
 *
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <glib.h>
#include <time.h>

typedef struct _RCDScript RCDScript;

struct _RCDScript {
    gchar   *trid;
    gchar   *sid;
    gchar   *name;

    gchar   *client_id;
    gchar   *client_version;

    time_t   start_time;
    gint     timeout;

    gint     exit_status;
    GString *stdout;
    GString *stderr;
};

RCDScript *rcd_script_new  (const char *trid, const char *sid);
void       rcd_script_free (RCDScript *script);
void       rcd_script_log  (RCDScript *script);

void       rcd_script_set_content (RCDScript *script, const char *buf, size_t len);

void       rcd_script_set_client  (RCDScript  *script,
                                   const char *client_id,
                                   const char *version);
