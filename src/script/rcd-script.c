/* -*- Mode: C; tab-width: 4; c-basic-offset: 4; indent-tabs-mode: nil -*- */

/*
 * rcd-script.c
 *
 * Copyright (C) 2003 Ximian, Inc.
 *
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include "rcd-script.h"

#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>

#include <rcd-xmlrpc.h>
#include <rcd-rpc-util.h>

RCDScript *
rcd_script_new (const char *trid, const char *sid)
{
    RCDScript *script;

    script = g_new0 (RCDScript, 1);

    script->trid = g_strdup (trid);
    script->sid = g_strdup (sid);
    script->stdout = g_string_new (NULL);
    script->stderr = g_string_new (NULL);
    script->start_time = time (NULL);

    return script;
}

void
rcd_script_free (RCDScript *script)
{
    g_return_if_fail (script != NULL);

    g_free (script->trid);
    g_free (script->sid);
    g_free (script->client_id);
    g_free (script->client_version);

    if (script->name)
        unlink (script->name);
    g_free (script->name);

    if (script->stdout)
        g_string_free (script->stdout, TRUE);

    if (script->stderr)
        g_string_free (script->stderr, TRUE);

    g_free (script);
}

void
rcd_script_set_content (RCDScript *script, const char *buf, size_t len)
{
    int fd;
    char *file_name;

    g_return_if_fail (script != NULL);
    g_return_if_fail (buf != NULL);

    fd = g_file_open_tmp ("rcd-XXXXXX", &file_name, NULL);
    rc_write (fd, buf, len);
    rc_close (fd);

    chmod (file_name, S_IRUSR | S_IXUSR);

    script->name = file_name;
}

void
rcd_script_set_client (RCDScript  *script,
                       const char *client_id,
                       const char *version)
{
    g_return_if_fail (script != NULL);
    g_return_if_fail (client_id != NULL);
    g_return_if_fail (version != NULL);

    script->client_id = g_strdup (client_id);
    script->client_version = g_strdup (version);
}

static xmlrpc_value *
rcd_script_to_xmlrpc (xmlrpc_env *env, RCDScript *script)
{
    xmlrpc_value *value = NULL;

    /* Common part for all logs */
    value = xmlrpc_struct_new (env);
    XMLRPC_FAIL_IF_FAULT (env);

    RCD_XMLRPC_STRUCT_SET_STRING (env, value, "trid", script->trid);
    XMLRPC_FAIL_IF_FAULT (env);

    RCD_XMLRPC_STRUCT_SET_INT (env, value, "endtime", time(NULL));
    XMLRPC_FAIL_IF_FAULT (env);

    RCD_XMLRPC_STRUCT_SET_STRING (env, value, "client", script->client_id);
    XMLRPC_FAIL_IF_FAULT (env);

    RCD_XMLRPC_STRUCT_SET_STRING (env, value, "version", script->client_version);
    XMLRPC_FAIL_IF_FAULT (env);

    RCD_XMLRPC_STRUCT_SET_INT (env, value, "status",
                               (script->exit_status == 0) ? 1 : 0);
    XMLRPC_FAIL_IF_FAULT (env);

    /* Script part */

    RCD_XMLRPC_STRUCT_SET_STRING (env, value, "log_type", "script");
    XMLRPC_FAIL_IF_FAULT (env);

    RCD_XMLRPC_STRUCT_SET_STRING (env, value, "sid", script->sid);
    XMLRPC_FAIL_IF_FAULT (env);

    RCD_XMLRPC_STRUCT_SET_STRING (env, value, "type", "P");
    XMLRPC_FAIL_IF_FAULT (env);

    RCD_XMLRPC_STRUCT_SET_STRING (env, value, "stdout", script->stdout->str);
    XMLRPC_FAIL_IF_FAULT (env);

    RCD_XMLRPC_STRUCT_SET_STRING (env, value, "stderr", script->stderr->str);
    XMLRPC_FAIL_IF_FAULT (env);

#if 0
    RCD_XMLRPC_STRUCT_SET_INT (env, value, "killed", WTERMSIG (script->exit_status));
    XMLRPC_FAIL_IF_FAULT (env);
#endif

    RCD_XMLRPC_STRUCT_SET_INT (env, value, "exit_status", script->exit_status);
    XMLRPC_FAIL_IF_FAULT (env);

cleanup:
    if (env->fault_occurred && value) {
        xmlrpc_DECREF (value);
        value = NULL;
    }

    return value;
}

static void
rcd_script_log_sent (char *server_url,
                     char *method_name,
                     xmlrpc_value *param_array,
                     void *user_data,
                     xmlrpc_env *fault,
                     xmlrpc_value *result)
{
    if (fault->fault_occurred)
        rc_debug (RC_DEBUG_LEVEL_MESSAGE,
                  "Unable to send script log to '%s': %s",
                  server_url, fault->fault_string);
    else
        rc_debug (RC_DEBUG_LEVEL_DEBUG,
                  "Successfully sent script log to '%s'",
                  server_url);
}

void
rcd_script_log (RCDScript *script)
{
    xmlrpc_env env;
    xmlrpc_value *value;
    xmlrpc_value *params;

    g_return_if_fail (script != NULL);

    xmlrpc_env_init (&env);
    XMLRPC_FAIL_IF_FAULT (&env);

    value = rcd_script_to_xmlrpc (&env, script);
    XMLRPC_FAIL_IF_FAULT (&env);

    params = xmlrpc_build_value (&env, "(V)", value);
    xmlrpc_DECREF (value);
    XMLRPC_FAIL_IF_FAULT (&env);

    rcd_xmlrpc_client_foreach_host (TRUE, "rcserver.transaction.log",
                                    rcd_script_log_sent,
                                    NULL,
                                    params);
    xmlrpc_DECREF (params);

cleanup:
    xmlrpc_env_clean (&env);
}
