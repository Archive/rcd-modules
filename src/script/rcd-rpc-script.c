/* -*- Mode: C; tab-width: 4; c-basic-offset: 4; indent-tabs-mode: nil -*- */

/*
 * rcd-rpc-script.c
 *
 * Copyright (C) 2003 Ximian, Inc.
 *
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include <time.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <xmlrpc.h>
#include <libredcarpet.h>

#include <rcd-module.h>
#include <rcd-rpc.h>
#include <rcd-rpc-util.h>
#include <rcd-xmlrpc.h>

#include "rcd-script.h"

#define POLL_INTERVAL 500 /* half a second */

static RCDModule *rcd_module = NULL;

typedef struct {
    RCDScript  *script;
    xmlrpc_env *env;

	gint        child_pid;
	GIOChannel *stdout_channel;
	GIOChannel *stderr_channel;
} RunnerData;

static void
runner_data_free (RunnerData *data)
{
    if (data->script)
        rcd_script_free (data->script);

	g_free (data);
}

static gboolean
runner_finished (RunnerData *data)
{
    rcd_script_log (data->script);

    return FALSE;
}

static gboolean
runner_read (GIOChannel *ioc, GIOCondition condition, gpointer data)
{
	gboolean retval = FALSE;
	gchar buf[1024];
	gsize len;
	GIOStatus io_status;
	RunnerData *rd = data;

try_read:
	io_status = g_io_channel_read_chars (ioc, buf, 1024, &len, NULL);

	switch (io_status) {
	case G_IO_STATUS_AGAIN:
		goto try_read;
		break;
	case G_IO_STATUS_NORMAL:
		retval = TRUE;
		break;
	default:
		break;
	}

	if (len > 0) {
		if (ioc == rd->stdout_channel)
			rd->script->stdout = g_string_append_len (rd->script->stdout,
                                                      buf, len);
		else
			rd->script->stderr = g_string_append_len (rd->script->stderr,
                                                      buf, len);
	}

	return retval;
}

static gboolean
runner_poll (RunnerData *data)
{
	int status, pid;
	time_t now;
    RCDScript *script = data->script;

	pid = waitpid (data->child_pid, &status, WNOHANG);
	if (pid > 0) {
		/* Finished */
		script->exit_status = status;
		g_idle_add ((GSourceFunc) runner_finished, data);
		return FALSE;
	}

	now = time (NULL);
	if ((now - script->start_time) >= script->timeout) {
		/* Timeout */
		if (kill (data->child_pid, SIGTERM) == -1)
			/* Die! */
			kill (data->child_pid, SIGKILL);
	}

	return TRUE;
}

static gboolean
runner (RunnerData *data)
{
    gint child_pid;
    gint stdout_fd, stderr_fd;
    GError *error = NULL;
    RCDScript *script = data->script;
    char *argv[] = {script->name, NULL};

	if (!g_spawn_async_with_pipes (NULL, /* Working directory */
                                   argv,
                                   NULL, /* env pointer */
                                   G_SPAWN_DO_NOT_REAP_CHILD, /* flags */
                                   NULL, /* child setup function */
                                   NULL, /* user data to function */
                                   &child_pid, /* child pid */
                                   NULL, /* stdin */
                                   &stdout_fd, /* stdout */
                                   &stderr_fd, /* stderr */
                                   &error)) {

        xmlrpc_env_set_fault (data->env, 1, error->message);
		g_error_free (error);
		return FALSE;
	}

	data->child_pid = child_pid;

	data->stdout_channel = g_io_channel_unix_new (stdout_fd);
	g_io_channel_set_flags (data->stdout_channel, G_IO_FLAG_NONBLOCK, NULL);

	data->stderr_channel = g_io_channel_unix_new (stderr_fd);
	g_io_channel_set_flags (data->stderr_channel, G_IO_FLAG_NONBLOCK, NULL);

	g_io_add_watch (data->stdout_channel, G_IO_IN | G_IO_HUP,
                    runner_read, data);
	g_io_channel_unref (data->stdout_channel);

	g_io_add_watch (data->stderr_channel, G_IO_IN | G_IO_HUP,
                    runner_read, data);
	g_io_channel_unref (data->stderr_channel);

	g_timeout_add (POLL_INTERVAL,
                   (GSourceFunc) runner_poll,
                   data);

    return TRUE;
}

static xmlrpc_value *
script_run (xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *buf;
    size_t len;
    int timeout;
    char *trid, *sid, *client_id, *client_version;
    RunnerData *data;

    xmlrpc_parse_value (env, param_array, "(6issss)", &buf, &len,
                        &timeout, &trid, &sid,
                        &client_id, &client_version);
    XMLRPC_FAIL_IF_FAULT (env);

    data = g_new0 (RunnerData, 1);
    data->env = env;

    data->script = rcd_script_new (trid, sid);
    rcd_script_set_content (data->script, buf, len);
    rcd_script_set_client (data->script, client_id, client_version);
    data->script->timeout = timeout;

    if (runner (data))
        return xmlrpc_build_value (env, "i", 0);
    else
        runner_data_free (data);

cleanup:
    return NULL;
} /* script_run */

static void
run_blocking (xmlrpc_env *env, RCDScript *script)
{
    char *stdout, *stderr;
    gint exit_status;
    GError *error = NULL;
    
    if (!g_spawn_command_line_sync (script->name, &stdout, &stderr,
                                    &exit_status, &error)) {
        rcd_module_debug (RCD_DEBUG_LEVEL_WARNING, rcd_module,
                          "run_blocking: %s", error->message);

        xmlrpc_env_set_fault (env, 1, error->message);
		g_error_free (error);
		return;
	}

    script->exit_status = exit_status;
    script->stdout = g_string_append (script->stdout, stdout);
    script->stderr = g_string_append (script->stderr, stderr);

    g_free (stdout);
    g_free (stderr);

    rcd_script_log (script);

    if (exit_status != 0)
        xmlrpc_env_set_fault (env, 1, "Script failed");
}

static xmlrpc_value *
script_run_blocking (xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *buf;
    size_t len;
    char *trid, *sid, *client_id, *client_version;
    RCDScript *script;

    xmlrpc_parse_value (env, param_array, "(6ssss)",
                        &buf, &len,
                        &trid, &sid,
                        &client_id, &client_version);
    XMLRPC_FAIL_IF_FAULT (env);

    script = rcd_script_new (trid, sid);
    rcd_script_set_content (script, buf, len);
    rcd_script_set_client (script, client_id, client_version);

    run_blocking (env, script);
    rcd_script_free (script);

cleanup:
    if (!env->fault_occurred)
        return xmlrpc_build_value (env, "i", 0);

    return NULL;
}


void rcd_module_load (RCDModule *);

void
rcd_module_load (RCDModule *module)
{
    /* Initialize the module */
    module->name = "rcd.script";
    module->description = "A module to spawn processes";
    module->version = VERSION;
    module->interface_major = 1;
    module->interface_minor = 0;

    rcd_module = module;

    /* Register RPC methods */
    rcd_rpc_register_method ("rcd.script.run", script_run, "superuser", NULL);
    rcd_rpc_register_method ("rcd.script.run_blocking",
                             script_run_blocking, "superuser", NULL);
} /* rcd_module_load */

int rcd_module_major_version = RCD_MODULE_MAJOR_VERSION;
int rcd_module_minor_version = RCD_MODULE_MINOR_VERSION;
