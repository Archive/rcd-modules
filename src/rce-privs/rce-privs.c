/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */

/*
 * rce-privs.c
 *
 * Copyright (C) 2003 Ximian, Inc.
 *
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>

#include <rcd-module.h>
#include <rcd-rpc-util.h>
#include <rcd-world-remote.h>
#include <rcd-xmlrpc.h>
#include "rce-helpers.h"

RCDModule *rcd_module = NULL;

static RCDIdentity *
lookup_identity (RCDIdentityBackend *backend, const char *username)
{
    RCDWorldRemote *remote = RCD_WORLD_REMOTE (backend->user_data);
    GSList *iter;

    for (iter = remote->identities; iter; iter = iter->next) {
        RCDIdentity *identity = iter->data;

        if (!strcmp (identity->username, username))
            return rcd_identity_copy (identity);
    }

    return NULL;
}

static void
foreach_identity (RCDIdentityBackend *backend, RCDIdentityFn fn,
                  gpointer user_data)
{
    RCDWorldRemote *remote = RCD_WORLD_REMOTE (backend->user_data);
    GSList *iter;

    for (iter = remote->identities; iter; iter = iter->next) {
        RCDIdentity *identity = iter->data;

        fn (identity, user_data);
    }
}    

static void
got_privs_cb (char *server_url, char *method_name, xmlrpc_value *param_array,
              void *user_data, xmlrpc_env *fault, xmlrpc_value *result)
{
    RCDWorldRemote *remote = RCD_WORLD_REMOTE (user_data);
    GSList *prev_identities;
    xmlrpc_env env;
    int size = 0, i;

    if (fault->fault_occurred) {
        rcd_module_debug (RCD_DEBUG_LEVEL_ERROR, rcd_module,
                  "Unable to download privileges from %s: %s",
                  RC_WORLD_SERVICE (remote)->url, fault->fault_string);
        return;
    }

    prev_identities = remote->identities;
    remote->identities = NULL;

    if (!remote->identity_backend) {
        remote->identity_backend = rcd_identity_backend_new (FALSE);
        remote->identity_backend->is_editable = FALSE;
        remote->identity_backend->user_data = remote;
        remote->identity_backend->lookup_fn = lookup_identity;
        remote->identity_backend->foreach_fn = foreach_identity;

        rcd_identity_add_backend (remote->identity_backend);
    }

    xmlrpc_env_init (&env);

    size = xmlrpc_array_size (&env, result);
    XMLRPC_FAIL_IF_FAULT (&env);

    for (i = 0; i < size; i++) {
        xmlrpc_value *v;
        char *username, *password, *privs;
        RCDIdentity *identity;

        v = xmlrpc_array_get_item (&env, result, i);
        XMLRPC_FAIL_IF_FAULT (&env);

        RCD_XMLRPC_STRUCT_GET_STRING (&env, v, "username", username);

        identity = rcd_identity_lookup (username);

        if (identity) {
            rcd_module_debug (RCD_DEBUG_LEVEL_WARNING, rcd_module,
                      "Not replacing existing identity for '%s'",
                      username);

            rcd_identity_free (identity);
            g_free (username);
        } else {
            RCD_XMLRPC_STRUCT_GET_STRING (&env, v, "password", password);

            RCD_XMLRPC_STRUCT_GET_STRING (&env, v, "privs", privs);

            identity = rcd_identity_new ();
            identity->username = username;
            identity->password = password;
            identity->privileges = rcd_privileges_from_string (privs);
            g_free (privs);

            remote->identities = g_slist_prepend (remote->identities,
                                                  identity);
        }
    }

cleanup:
    if (env.fault_occurred) {
        rcd_module_debug (RCD_DEBUG_LEVEL_CRITICAL, rcd_module,
                  "Privilege information from the server is malformed: %s",
                  env.fault_string);
        g_slist_foreach (remote->identities, (GFunc) rcd_identity_free, NULL);
        g_slist_free (remote->identities);
        remote->identities = prev_identities;
    } else {
        g_slist_foreach (prev_identities, (GFunc) rcd_identity_free, NULL);
        g_slist_free (prev_identities);
    }
}

static void
fetch_privileges (RCDWorldRemote *remote)
{
    xmlrpc_env env;
    xmlrpc_server_info *server_info;

    xmlrpc_env_init (&env);

    server_info = rcd_xmlrpc_get_server (&env, RC_WORLD_SERVICE (remote)->url);
    XMLRPC_FAIL_IF_FAULT (&env);

    xmlrpc_client_call_server_asynch (server_info, "rcserver.machine.getPrivs",
                                      got_privs_cb, remote, "()");

    xmlrpc_server_info_free (server_info);

cleanup:
    if (env.fault_occurred) {
        rcd_module_debug (RCD_DEBUG_LEVEL_ERROR, rcd_module,
                  "Unable to download privileges from %s",
                  RC_WORLD_SERVICE (remote)->url);
    }

    xmlrpc_env_clean (&env);
}

static void
services_added (GSList *services)
{
    GSList *iter;

    for (iter = services; iter != NULL; iter = iter->next) {
        RCDWorldRemote *remote = RCD_WORLD_REMOTE (iter->data);

        fetch_privileges (remote);
    }
}

static void
services_removed (GSList *services)
{
    GSList *iter;

    for (iter = services; iter; iter = iter->next) {
        RCDWorldRemote *remote = RCD_WORLD_REMOTE (iter->data);

        if (remote->identity_backend) {
            rcd_identity_remove_backend (remote->identity_backend);
            g_free (remote->identity_backend);
            remote->identity_backend = NULL;

            g_slist_foreach (remote->identities,
                             (GFunc) rcd_identity_free, NULL);
            g_slist_free (remote->identities);
            remote->identities = NULL;
        }
    }
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

int rcd_module_major_version = RCD_MODULE_MAJOR_VERSION;
int rcd_module_minor_version = RCD_MODULE_MINOR_VERSION;

void rcd_module_load (RCDModule *);

void
rcd_module_load (RCDModule *module)
{
    module->name = "rcd.rce-privs";
    module->description = "Red Carpet Enterprise Privileges";
    module->version = 0;
    module->interface_major = 0;
    module->interface_minor = 0;
    
    rcd_module = module;

    notify_rce_add (services_added, TRUE, 0);
    notify_rce_remove (services_removed, TRUE, 0);
}
