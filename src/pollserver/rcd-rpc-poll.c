/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */

#include <xmlrpc.h>

#include <rcd-module.h>
#include <rcd-recurring.h>
#include <rcd-rpc.h>
#include <rcd-prefs.h>
#include <rcd-world-remote.h>
#include <rcd-rpc-prefs.h>
#include <rcd-xmlrpc.h>

#define RECURRING_POLL_MIN 3600 /* 1 hour */

static RCDModule *rcd_module = NULL;
static RCDRecurring recurring_poll;

int rcd_module_major_version = RCD_MODULE_MAJOR_VERSION;
int rcd_module_minor_version = RCD_MODULE_MINOR_VERSION;

static void recurring_poll_start (void);
static void recurring_poll_stop (void);

static int
recurring_poll_prefs_get_interval (void)
{
    return rcd_prefs_get_int ("/ModuleServerPoll/server-poll-interval=3600");
}

static gboolean
recurring_poll_prefs_set_interval (guint32 interval, GError **err)
{
    guint32 old_interval;

    if (interval != 0 && interval < RECURRING_POLL_MIN) {
        g_set_error (err, RCD_PREFS_ERROR, RCD_PREFS_ERROR,
                     "server-poll frequencies of less than %d seconds are not "
                     "allowed", RECURRING_POLL_MIN);
        rcd_module_debug (RC_DEBUG_LEVEL_WARNING, rcd_module,
                          "server-poll frequencies of less than %d seconds "
                          "are not allowed.", RECURRING_POLL_MIN);
        return FALSE;
    }

    old_interval = recurring_poll_prefs_get_interval ();

    if (old_interval && !interval)
        recurring_poll_stop ();

    rcd_prefs_set_int ("/ModuleServerPoll/server-poll-interval",
                       (int) interval);
    rcd_module_debug (RC_DEBUG_LEVEL_MESSAGE, rcd_module,
                      "Setting server-poll interval to %u seconds",
                      interval);

    if (!old_interval && interval)
        recurring_poll_start ();

    return TRUE;
}

static gboolean
call_method (const gchar *method_name, xmlrpc_value *args)
{
    xmlrpc_env env;
    xmlrpc_value *value;

    xmlrpc_env_init (&env);

    rcd_module_debug (RCD_DEBUG_LEVEL_DEBUG, rcd_module,
                      "Going to call method '%s'", method_name);

    value = rcd_rpc_call_method (&env, method_name, args);
    XMLRPC_FAIL_IF_FAULT (&env);
    xmlrpc_DECREF (value);

cleanup:
    if (env.fault_occurred) {
        rcd_module_debug (RCD_DEBUG_LEVEL_ERROR, rcd_module,
                          "Error calling method %s: %s",
                          method_name, env.fault_string);
        xmlrpc_env_clean (&env);
        return FALSE;
    }

    rcd_module_debug (RCD_DEBUG_LEVEL_DEBUG, rcd_module,
                      "Method '%s' succeeded", method_name);

    return TRUE;
}

static gboolean
read_task (xmlrpc_env *env,
           xmlrpc_server_info *server,
           xmlrpc_value **id,
           char **method_name,
           xmlrpc_value **params)
{
    int len;
    const char *method = "rcserver.task.get";
    char *tmp;
    xmlrpc_value *task = NULL;

    task = xmlrpc_client_call_server (env, server, (char *) method, "()");
    XMLRPC_FAIL_IF_FAULT (env);

    len = xmlrpc_array_size (env, task);
    XMLRPC_FAIL_IF_FAULT (env);

    if (len == 0) {
        xmlrpc_DECREF (task);
        return FALSE; /* No more tasks scheduled */
    }

    xmlrpc_parse_value (env, task, "(VsV)", id, &tmp, params);
    XMLRPC_FAIL_IF_FAULT (env);

    xmlrpc_INCREF (*id);
    xmlrpc_INCREF (*params);
    *method_name = g_strdup (tmp);

cleanup:
    if (task)
        xmlrpc_DECREF (task);

    if (env->fault_occurred) {
        rcd_module_debug (RCD_DEBUG_LEVEL_ERROR, rcd_module,
                          "Error getting task info: %s", env->fault_string);
        return FALSE;
    }

    return TRUE;
}

static void
report_method_value (xmlrpc_env *env,
                     xmlrpc_server_info *server,
                     xmlrpc_value *value)
{
    xmlrpc_value *ret = NULL;

    ret = xmlrpc_client_call_server_params (env, server,
                                            "rcserver.task.report",
                                            value);

    if (ret)
        xmlrpc_DECREF (ret);
}

static gboolean
poll_one (RCWorld *world, gpointer user_data)
{
    char *method_name;
    xmlrpc_env env;
    xmlrpc_value *id;
    xmlrpc_value *params;
    xmlrpc_server_info *server;

    if (!RCD_WORLD_REMOTE (world)->premium_service)
        return TRUE;

    xmlrpc_env_init (&env);
    server = rcd_xmlrpc_get_server (&env, RC_WORLD_SERVICE(world)->url);

    while (read_task (&env, server, &id, &method_name, &params)) {
        xmlrpc_value *ret;

        ret = xmlrpc_build_value (&env, "(Vi)", id,
                                  call_method (method_name, params));
        xmlrpc_DECREF (id);
        xmlrpc_DECREF (params);
        g_free (method_name);

        report_method_value (&env, server, ret);
        XMLRPC_FAIL_IF_FAULT (&env);
    }

cleanup:
    xmlrpc_server_info_free (server);

    if (env.fault_occurred) {
        rcd_module_debug (RCD_DEBUG_LEVEL_ERROR, rcd_module,
                          "Error polling server: %s", env.fault_string);
        xmlrpc_env_clean (&env);
    }

    return TRUE;
}

static void
recurring_poll_execute (RCDRecurring *recur)
{
    rcd_recurring_block ();

    rc_world_multi_foreach_subworld_by_type (RC_WORLD_MULTI (rc_get_world ()),
                                             RCD_TYPE_WORLD_REMOTE,
                                             poll_one, NULL);

    rcd_recurring_allow ();
}

static time_t
recurring_poll_first (RCDRecurring *recur, time_t now)
{
    static gboolean startup = TRUE;

    if (startup) {
        startup = FALSE;
        return now + 10;
    }
	return now + recurring_poll_prefs_get_interval ();
}

static time_t
recurring_poll_next (RCDRecurring *recur, time_t previous)
{
	return previous + recurring_poll_prefs_get_interval ();
}

static void
recurring_poll_start (void)
{
    if (!recurring_poll_prefs_get_interval ()) {
        rcd_module_debug (RC_DEBUG_LEVEL_INFO, rcd_module,
                          "server-poll disabled");
        return;
    }

    rcd_module_debug (RC_DEBUG_LEVEL_MESSAGE, rcd_module,
                      "Starting server-poll");


    recurring_poll.tag = g_quark_from_static_string ("server-poll");
    recurring_poll.label   = NULL;
    recurring_poll.execute = recurring_poll_execute;
    recurring_poll.first   = recurring_poll_first;
    recurring_poll.next    = recurring_poll_next;

    rcd_recurring_add (&recurring_poll);
}

static void
recurring_poll_stop (void)
{
    rcd_module_debug (RC_DEBUG_LEVEL_MESSAGE, rcd_module,
                      "Stopping server-poll");

    rcd_recurring_remove (&recurring_poll);
}

static xmlrpc_value *
recurring_poll_now (xmlrpc_env   *env,
                    xmlrpc_value *param_array,
                    void         *user_data)
{
	recurring_poll_execute (NULL);

	return xmlrpc_build_value (env, "i", 0);
}

void rcd_module_load (RCDModule *);

void
rcd_module_load (RCDModule *module)
{
    module->name = "rcd.serverpoll";
    module->description = "Server Poll";
    module->version = 0;
    module->interface_major = 0;
    module->interface_minor = 0;

    rcd_module = module;

    rcd_rpc_prefs_register_pref (
	    "server-poll-interval", RCD_PREF_INT,
	    "Interval for recurring server poll in seconds",
	    "ServerPoll",
	    (RCDPrefGetFunc) recurring_poll_prefs_get_interval, "view",
	    (RCDPrefSetFunc) recurring_poll_prefs_set_interval, "superuser");

    rcd_rpc_register_method ("rcd.serverpoll.poll", recurring_poll_now,
                             "superuser", NULL);

    recurring_poll_start ();
}
