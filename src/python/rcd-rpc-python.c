/* -*- Mode: C; tab-width: 4; c-basic-offset: 4; indent-tabs-mode: nil -*- */

/*
 * rcd-rpc-python.c
 *
 * Copyright (C) 2003 Ximian, Inc.
 *
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include "config.h"

#include "rcd-rpc-python.h"
#include <rcd-rpc.h>
#include <Python.h>

static RCDModule *rcd_module = NULL;

static void
set_xmlrpc_fault (xmlrpc_env *env)
{
	PyObject *s = NULL;
	PyObject *exception, *v, *tb;
	gboolean set = FALSE;

	PyErr_Fetch(&exception, &v, &tb);
	if (exception) {
		PyErr_NormalizeException(&exception, &v, &tb);

		s = PyObject_Str (v);
		if (s && PyString_Check (s)) {
			set = TRUE;
			xmlrpc_env_set_fault (env, 1,
                                  PyString_AsString (s));
		}
	}

	if (!set)
		xmlrpc_env_set_fault (env, 1,
                              "Unknown error");

	Py_XDECREF (s);
}

/**
 * PyObject_to_xmlrpc_value:
 * @env: 
 * @po: 
 * 
 * 
 * 
 * Return Value: 
 **/
static xmlrpc_value *
PyObject_to_xmlrpc_value (xmlrpc_env *env, PyObject *po)
{
	xmlrpc_value *value = NULL;

    /* None */
    if (po == Py_None)
        value = xmlrpc_build_value (env, "i", 0);
    
	/* Int */
	else if (PyInt_Check (po))
		value = xmlrpc_build_value (env, "i", PyInt_AsLong (po));

	/* Float */
	else if (PyFloat_Check (po))
		value = xmlrpc_build_value (env, "d", PyFloat_AsDouble (po));

	/* String */
	else if (PyString_Check (po))
		value = xmlrpc_build_value (env, "s", PyString_AsString (po));

	/* List */
	else if (PyList_Check (po)) {
		int i;
		xmlrpc_value *sub_val;

		value = xmlrpc_build_value (env, "()");
        XMLRPC_FAIL_IF_FAULT (env);
		for (i = 0; i < PyList_Size (po); i++) {
			sub_val = PyObject_to_xmlrpc_value (env, PyList_GetItem (po, i));
            if (env->fault_occurred) {
                xmlrpc_DECREF (value);
                goto cleanup;
            }
			xmlrpc_array_append_item (env, value, sub_val);
            if (env->fault_occurred) {
                xmlrpc_DECREF (sub_val);
                xmlrpc_DECREF (value);
                goto cleanup;
            }
            xmlrpc_DECREF (sub_val);
		}

		return value;
	}

	/* Tuple */
	else if (PyTuple_Check (po)) {
		int i;
		xmlrpc_value *sub_val;

		value = xmlrpc_build_value (env, "()");
        XMLRPC_FAIL_IF_FAULT (env);
		for (i = 0; i < PyTuple_Size (po); i++) {
			sub_val = PyObject_to_xmlrpc_value (env, PyTuple_GetItem (po, i));
            if (env->fault_occurred) {
                xmlrpc_DECREF (value);
                goto cleanup;
            }
			xmlrpc_array_append_item (env, value, sub_val);
            if (env->fault_occurred) {
                xmlrpc_DECREF (sub_val);
                xmlrpc_DECREF (value);
                goto cleanup;
            }
            xmlrpc_DECREF (sub_val);
		}

		return value;
	}

	/* Dict */
	else if (PyDict_Check (po)) {
		int i;
		PyObject *pkeys;
		PyObject *pkey;
		PyObject *pval;
		xmlrpc_value *xkey;
		xmlrpc_value *xval;

		value = xmlrpc_struct_new (env);
        XMLRPC_FAIL_IF_FAULT (env);

		pkeys = PyDict_Keys (po);
		for (i = 0; i < PyList_Size (pkeys); i++) {
			pkey = PyList_GetItem (pkeys, i);
			pval = PyDict_GetItem (po, pkey);

			xkey = PyObject_to_xmlrpc_value (env, pkey);
            if (env->fault_occurred) {
                xmlrpc_DECREF (value);
                goto cleanup;
            }

			xval = PyObject_to_xmlrpc_value (env, pval);
            if (env->fault_occurred) {
                xmlrpc_DECREF (xkey);
                xmlrpc_DECREF (value);
                goto cleanup;
            }

			xmlrpc_struct_set_value_v (env, value, xkey, xval);
            if (env->fault_occurred) {
                xmlrpc_DECREF (xkey);
                xmlrpc_DECREF (xval);
                xmlrpc_DECREF (value);
                goto cleanup;
            }
            xmlrpc_DECREF (xkey);
            xmlrpc_DECREF (xval);
		}
	}

cleanup:
    if (env->fault_occurred)
        value = NULL;

	return value;
}

/**
 * xmlrpc_value_to_PyObject:
 * @env: 
 * @xvalue: 
 * 
 * 
 * 
 * Return Value: New reference (or NULL).
 **/
static PyObject *
xmlrpc_value_to_PyObject (xmlrpc_env *env, xmlrpc_value *xvalue)
{
	xmlrpc_type type;
	PyObject *value = NULL;

	type = xmlrpc_value_type (xvalue);

	/* Int */
	if (type == XMLRPC_TYPE_INT) {
		xmlrpc_int32 i;

		xmlrpc_parse_value (env, xvalue, "i", &i);
		value = Py_BuildValue ("i", i);
	}

	/* Boolean */
	else if (type == XMLRPC_TYPE_BOOL) {
		xmlrpc_bool bool;

		xmlrpc_parse_value (env, xvalue, "b", &bool);
		value = Py_BuildValue ("i", bool);
	}

	/* Float */
	else if (type == XMLRPC_TYPE_DOUBLE) {
		double d;

		xmlrpc_parse_value (env, xvalue, "d", &d);
		value = Py_BuildValue ("d", d);
	}

	/* String */
	else if (type == XMLRPC_TYPE_STRING) {
		char *str;

		xmlrpc_parse_value (env, xvalue, "s", &str);
		value = Py_BuildValue ("s", str);
	}

    /* Base64 */
    else if (type == XMLRPC_TYPE_BASE64) {
        char *buf;
        size_t len;

        xmlrpc_parse_value (env, xvalue, "6", &buf, &len);
        value = Py_BuildValue ("s#", buf, len);
    }

	/* Array */
	else if (type == XMLRPC_TYPE_ARRAY) {
		int i, len;
		xmlrpc_value *xval;
		PyObject *pval;

		len = xmlrpc_array_size (env, xvalue);

		value = PyList_New (len);
		if (value == NULL) {
			set_xmlrpc_fault (env);
			return NULL;
		}

		for (i = 0; i < len; i++) {
			xval = xmlrpc_array_get_item (env, xvalue, i);
			pval = xmlrpc_value_to_PyObject (env, xval);
			if (pval == NULL)
				/* Let's ignore unknown xmlrpc_values */
				continue;

			if (PyList_SetItem (value, i, pval) < 0) {
				Py_DECREF (pval);
				Py_DECREF (value);
				set_xmlrpc_fault (env);

				return NULL;
			}
		}
	}

	/* Dict */
	else if (type == XMLRPC_TYPE_STRUCT) {
		int i, success;
		xmlrpc_value *xkey, *xval;
		PyObject *pkey, *pval;

		value = PyDict_New ();
		if (value == NULL) {
			set_xmlrpc_fault (env);
			return NULL;
		}

		for (i = 0; i < xmlrpc_struct_size (env, xvalue); i++) {
			xmlrpc_struct_get_key_and_value (env, xvalue, i, &xkey, &xval);
			pkey = xmlrpc_value_to_PyObject (env, xval);
			if (pkey == NULL) {
				/* We should be able to convert all hashable types */
				Py_DECREF (value);
				return NULL;
			}
			pval = xmlrpc_value_to_PyObject (env, xkey);
			if (pval == NULL) {
				/* Let's ignore unknown xmlrpc_values */
				Py_DECREF (pkey);
				continue;
			}

			success = PyDict_SetItem (value, pkey, pval);
			Py_DECREF (pkey);
			Py_DECREF (pval);

			if (success < 0) {
				Py_DECREF (value);
				set_xmlrpc_fault (env);
				return NULL;
			}
		}
	}

	if (value == NULL)
		set_xmlrpc_fault (env);

	return value;
}

/**
 * get_py_func:
 * @list: 
 * 
 * 
 * 
 * Return Value: New reference (or NULL);
 **/
static PyObject *
get_py_func (xmlrpc_env *env, PyObject *list)
{
	PyObject *module, *func_str;
	PyObject *module_str, *dict;
	PyObject *func = NULL;

	if (list == NULL ||
	    PyList_Check (list) == 0 ||
	    PyList_Size (list) < 2) {
		xmlrpc_env_set_fault (env, 1,
                              "Incorrect parameters");
		return NULL;
	}

	module = NULL;
	func_str = NULL;
	dict = NULL;

	module_str = PyList_GetItem (list, 0);
	if (!module_str || !PyString_Check (module_str))
		module_str = NULL;

	if (module_str) {
        rcd_module_debug (RCD_DEBUG_LEVEL_DEBUG, rcd_module,
                          "Module to import: '%s'",
                          PyString_AsString (module_str));
		module = PyImport_Import (module_str);
    }

	if (module)
		dict = PyModule_GetDict (module);
	else {
        PyErr_Print();
		xmlrpc_env_set_fault (env, 1,
                              "Could not import module");
    }

	if (dict) {
		func_str = PyList_GetItem (list, 1);
		if (!func_str || !PyString_Check (func_str)) {
            PyErr_Print();
			xmlrpc_env_set_fault (env, 1,
                                  "Could not extract function name");
			func_str = NULL;
		}
	}

	if (func_str) {
        rcd_module_debug (RCD_DEBUG_LEVEL_DEBUG, rcd_module,
                          "Function to call: '%s'",
                          PyString_AsString (func_str));
		func = PyDict_GetItem (dict, func_str);
		if (func) {
			if (PyCallable_Check (func))
                Py_INCREF (func);
            else {
                PyErr_Print();
				xmlrpc_env_set_fault_formatted (env, 1,
                                                "%s.%s is not callable",
                                                PyString_AsString (module_str),
                                                PyString_AsString (func_str));
				func = NULL;
			}
		} else {
			xmlrpc_env_set_fault_formatted (env, 1,
                                            "Module '%s' does not have function '%s'",
                                            PyString_AsString (module_str),
                                            PyString_AsString (func_str));
			func = NULL;
		}
	}

	Py_XDECREF (module);

	return func;
}

/**
 * get_py_func_args:
 * @list: 
 * 
 * 
 * 
 * Return Value: New reference (or NULL).
 **/
static PyObject *
get_py_func_args (xmlrpc_env *env, PyObject *list)
{
	int len;
	PyObject *args;

	if (list == NULL ||
	    PyList_Check (list) == 0) {
		xmlrpc_env_set_fault (env, 1,
						  "Could not extract function arguments");
		return NULL;
	}

	len = PyList_Size (list);
	if (len > 2) {
		PyObject *slice;

		slice = PyList_GetSlice (list, 2, len);
		args = PyList_AsTuple (slice);
		Py_DECREF (slice);
	} else
		args = PyTuple_New (0);

	return args;
}

xmlrpc_value *
python_function (xmlrpc_env *env, xmlrpc_value *xparam_array, gpointer user_data)
{
	xmlrpc_value *xvalue = NULL;
	PyObject *pparam_array;
	PyObject *pvalue, *pfunc, *pargs;

	pparam_array = xmlrpc_value_to_PyObject (env, xparam_array);
	if (!pparam_array)
		return NULL;

	pfunc = get_py_func (env, pparam_array);
	if (pfunc == NULL)
		return NULL;

	pargs = get_py_func_args (env, pparam_array);

	/* Done with pparam_array */
	Py_DECREF (pparam_array);

	if (pargs == NULL) {
		Py_DECREF (pfunc);
		return NULL;
	}

	pvalue = PyObject_CallObject (pfunc, pargs);

	/* Done with pfunc and pargs */
    Py_DECREF (pfunc);
	Py_DECREF (pargs);

	if (pvalue == NULL) {
		set_xmlrpc_fault (env);
		return NULL;
	}

	xvalue = PyObject_to_xmlrpc_value (env, pvalue);
	Py_DECREF (pvalue);

    if (env->fault_occurred)
        return NULL;

	return xvalue;
}

static void
append_module_path (void)
{
	PyObject *sysmod, *dict, *path, *new_path;

	sysmod = PyImport_ImportModule ("sys");
	if (sysmod)
		dict = PyModule_GetDict (sysmod);
	else
		dict = NULL;

	if (dict)
		path = PyDict_GetItemString (dict, "path");
	else
		path = NULL;

	if (path && PyList_Check (path)) {
		new_path = PyString_FromString (PYTHON_MODULE_DIR);
		if (new_path) {
			PyList_Insert (path, 0, new_path);
            rcd_module_debug (RCD_DEBUG_LEVEL_DEBUG, rcd_module,
                              "Appended '%s' to sys.path",
                              PYTHON_MODULE_DIR);
        }
		Py_XDECREF (new_path);
	} else
        rcd_module_debug (RCD_DEBUG_LEVEL_WARNING, rcd_module,
                          "Couldn't append '%s' to sys.path",
                          PYTHON_MODULE_DIR);

	Py_XDECREF (sysmod);
}

void
rcd_module_load (RCDModule *module)
{
    /* Initialize the module */
    module->name = "rcd.python";
    module->description = "Embeded Python Interpreter";
    module->version = 0;
    module->interface_major = 1;
    module->interface_minor = 0;

    rcd_module = module;

    Py_Initialize ();
    append_module_path ();

    /* Register RPC methods */
    rcd_rpc_register_method ("rcd.python", python_function, NULL, NULL);
} /* rcd_module_load */

int rcd_module_major_version = RCD_MODULE_MAJOR_VERSION;
int rcd_module_minor_version = RCD_MODULE_MINOR_VERSION;
