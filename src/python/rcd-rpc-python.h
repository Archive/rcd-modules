/* -*- Mode: C; tab-width: 4; c-basic-offset: 4; indent-tabs-mode: nil -*- */

/*
 * rcd-rpc-python.h
 *
 * Copyright (C) 2003 Ximian, Inc.
 *
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#ifndef __RCD_RPC_PYTHON_H__
#define __RCD_RPC_PYTHON_H__

#include <glib.h>
#include <xmlrpc.h>
#include <rcd-module.h>

void          rcd_module_load (RCDModule *module);
xmlrpc_value *python_function (xmlrpc_env *env,
						 xmlrpc_value *xparam_array,
						 gpointer user_data);

#endif /* __RCD_RPC_PYTHON_H__ */
