/* -*- Mode: C; tab-width: 4; c-basic-offset: 4; indent-tabs-mode: nil -*- */
/*
 * rce-helpers.c
 *
 * Copyright (C) 2003 Ximian, Inc.
 *
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

/*****************************************************************************/
/* Notify Services Cache */

#include <glib.h>
#include <rcd-shutdown.h>
#include <rcd-world-remote.h>
#include "rce-helpers.h"

struct _NotifyServicesCache {
    gboolean          on_refresh;
    gboolean          all_services;
    GHashTable       *services;
    guint             timeout;
    guint             timeout_id;
    NotifyServicesFn  timeout_fn;
};

NotifyServicesCache *
notify_services_cache_new (NotifyServicesFn callback, 
                           gboolean on_refresh,
                           guint delay)
{
    NotifyServicesCache *cache;

    cache = g_new (NotifyServicesCache, 1);

    cache->on_refresh = on_refresh;
    cache->all_services = FALSE;
    cache->services = g_hash_table_new (g_str_hash, g_str_equal);
    cache->timeout = delay;
    cache->timeout_id = 0;
    cache->timeout_fn = callback;

    return cache;
}

static gboolean
cache_clean_cb (gpointer key, gpointer value, gpointer user_data)
{
    RCWorldService *service = value;
    g_object_unref (G_OBJECT (service));

    return TRUE;
}

static void
notify_services_cache_clean (NotifyServicesCache *cache)
{
    cache->all_services = FALSE;
    g_hash_table_foreach_remove (cache->services,
                                 cache_clean_cb,
                                 NULL);
}

void
notify_services_destroy (NotifyServicesCache *cache)
{
    notify_services_cache_clean (cache);
    if (cache->timeout_id != 0) {
        g_source_remove (cache->timeout_id);
        cache->timeout_id = 0;
    }

    g_free (cache);
}

static gboolean
get_all_services_cb (RCWorld *world, gpointer user_data)
{
    RCDWorldRemote *remote = RCD_WORLD_REMOTE (world);
    GSList **services = user_data;

    if (remote->premium_service)
        *services = g_slist_prepend (*services, remote);

    return TRUE;
}

static void
get_registered_services_cb (gpointer key, gpointer value, gpointer user_data)
{
    RCWorldService *service = value;
    GSList **services = user_data;

    *services = g_slist_prepend (*services, service);
}

static gboolean
notify_services_timeout (gpointer user_data)
{
    NotifyServicesCache *cache = user_data;
    GSList *services = NULL;

    if (cache->timeout_fn) {
        if (cache->all_services) {
            RCWorldMulti *world = RC_WORLD_MULTI (rc_get_world ());

            rc_world_multi_foreach_subworld_by_type (world,
                                                     RCD_TYPE_WORLD_REMOTE,
                                                     get_all_services_cb,
                                                     &services);
        } else {
            g_hash_table_foreach (cache->services,
                                  get_registered_services_cb,
                                  &services);
        }

        (*cache->timeout_fn)(services);
    }

    if (services)
        g_slist_free (services);

    notify_services_cache_clean (cache);
    cache->timeout_id = 0;

    return FALSE;
}

void
notify_services (NotifyServicesCache *cache, RCWorldService *service)
{
    gchar *id;

    if (service == NULL) {
        notify_services_cache_clean (cache);
        cache->all_services = TRUE;
    } else {
        id = g_hash_table_lookup (cache->services, service);
        if (!id)
            g_hash_table_insert (cache->services,
                                 service->unique_id,
                                 g_object_ref (service));
    }

    if (cache->timeout_id == 0) {
        cache->timeout_id = g_timeout_add (cache->timeout,
                                           notify_services_timeout,
                                           cache);
    }
}

/*****************************************************************************/
/* RCE subworld add/remove helpers */

static GSList *rce_add_list = NULL;
static GSList *rce_remove_list = NULL;

static void
notify (GSList *cache_list, RCWorldMulti *multi, RCDWorldRemote *remote)
{
    GSList *iter;
    NotifyServicesCache *cache;
    RCWorldService *service = RC_WORLD_SERVICE (remote);

    for (iter = cache_list; iter != NULL; iter = iter->next) {
        cache = iter->data;

        if (!rc_world_is_refreshing (RC_WORLD (multi)) || cache->on_refresh)
            notify_services (cache, service);
    }
}

static void
subworld_activated_cb (RCDWorldRemote *world, gpointer user_data)
{
    notify (rce_add_list, RC_WORLD_MULTI (rc_get_world ()), world);
}

static void
subworld_added_cb (RCWorldMulti *multi,
                   RCWorld      *subworld,
                   gpointer      user_data)
{
    RCDWorldRemote *remote;

    if (!RCD_IS_WORLD_REMOTE (subworld))
        return;

    remote = RCD_WORLD_REMOTE (subworld);

    if (!remote->premium_service)
        return;

    g_signal_connect (remote,
                      "activated",
                      G_CALLBACK (subworld_activated_cb),
                      NULL);

    notify (rce_add_list, multi, remote);
}

static void
subworld_removed_cb (RCWorldMulti *multi,
                     RCWorld      *subworld,
                     gpointer      user_data)
{
    RCDWorldRemote *remote;

    if (!RCD_IS_WORLD_REMOTE (subworld))
        return;

    remote = RCD_WORLD_REMOTE (subworld);

    if (!remote->premium_service)
        return;

    notify (rce_remove_list, multi, remote);
}

static void
notify_services_cleanup (gpointer user_data)
{
    GSList **list = user_data;
    GSList *iter;

    for (iter = *list; iter; iter = iter->next)
        notify_services_destroy ((NotifyServicesCache *) iter->data);

    g_slist_free (*list);
    *list = NULL;
}

static void
notify_service_add_init (void)
{
    static gboolean inited = FALSE;

    if (!inited) {
        rcd_shutdown_add_handler (notify_services_cleanup, &rce_add_list);
        g_signal_connect (RC_WORLD_MULTI (rc_get_world ()),
                          "subworld_added",
                          G_CALLBACK (subworld_added_cb),
                          NULL);
        inited = TRUE;
    }
}

static void
notify_service_remove_init (void)
{
    static gboolean inited = FALSE;

    if (!inited) {
        rcd_shutdown_add_handler (notify_services_cleanup, &rce_remove_list);
        g_signal_connect (RC_WORLD_MULTI (rc_get_world ()),
                          "subworld_removed",
                          G_CALLBACK (subworld_removed_cb),
                          NULL);
        inited = TRUE;
    }
}

void
notify_rce_add (NotifyServicesFn callback, gboolean on_refresh, guint delay)
{
    NotifyServicesCache *cache;

    notify_service_add_init ();
    cache = notify_services_cache_new (callback, on_refresh, delay);
    rce_add_list = g_slist_append (rce_add_list, cache);
}

void
notify_rce_remove (NotifyServicesFn callback, gboolean on_refresh, guint delay)
{
    NotifyServicesCache *cache;

    notify_service_remove_init ();
    cache = notify_services_cache_new (callback, on_refresh, delay);
    rce_remove_list = g_slist_append (rce_remove_list, cache);
}
