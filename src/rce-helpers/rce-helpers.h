/* -*- Mode: C; tab-width: 4; c-basic-offset: 4; indent-tabs-mode: nil -*- */
/*
 * rce-helpers.h
 *
 * Copyright (C) 2003 Ximian, Inc.
 *
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#ifndef __RCE_HELPERS_H__
#define __RCE_HELPERS_H__

#include <libredcarpet.h>

typedef void (*NotifyServicesFn) (GSList *services);

typedef struct _NotifyServicesCache NotifyServicesCache;

NotifyServicesCache *notify_services_cache_new (NotifyServicesFn callback,
                                                gboolean on_refresh,
                                                guint delay);

void notify_services_destroy (NotifyServicesCache *cache);
void notify_services (NotifyServicesCache *cache, RCWorldService *service);

/* RCE world add/remove helpers */

void notify_rce_add    (NotifyServicesFn callback,
                        gboolean on_refresh,
                        guint delay);
void notify_rce_remove (NotifyServicesFn callback,
                        gboolean on_refresh,
                        guint delay);

#endif /* __RCE_HELPERS_H__ */
