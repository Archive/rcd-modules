/* -*- Mode: C; tab-width: 4; c-basic-offset: 4; indent-tabs-mode: nil -*- */

/*
 * rcd-hostname.c
 *
 * Copyright (C) 2004 Ximian, Inc.
 *
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <sys/utsname.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <string.h>
#include <time.h>
#include <xmlrpc.h>

#include <rcd-module.h>
#include <rcd-xmlrpc.h>
#include <rcd-prefs.h>
#include <rcd-heartbeat.h>

static RCDModule *rcd_module = NULL;

int rcd_module_major_version = RCD_MODULE_MAJOR_VERSION;
int rcd_module_minor_version = RCD_MODULE_MINOR_VERSION;

static void
hostname_sent (char *server_url,
               char *method_name,
               xmlrpc_value *param_array,
               void *user_data,
               xmlrpc_env *env,
               xmlrpc_value *result)
{
    if (env->fault_occurred)
        rcd_module_debug (RCD_DEBUG_LEVEL_MESSAGE,
                          rcd_module,
                          "Send hostname/ip failed: %s",
                          env->fault_string);
    else
        rcd_module_debug (RCD_DEBUG_LEVEL_DEBUG,
                          rcd_module,
                          "Hostname/IP successfully sent");
}

static void
hostname_send (const gchar *hostname, const gchar *ip)
{
    xmlrpc_env env;
    xmlrpc_value *params;

    xmlrpc_env_init (&env);

    params = xmlrpc_build_value (&env, "(ssi)", hostname, ip, 
                                 rcd_prefs_get_remote_server_port ());
    rcd_xmlrpc_client_foreach_host (TRUE, "rcserver.machine.updateAddress",
                                    hostname_sent, NULL, params);
    xmlrpc_DECREF (params);
    xmlrpc_env_clean (&env);
}

static void
hostname_update (gpointer user_data)
{
    struct utsname uname_buf;
    const gchar *hostname, *ip;

    if (uname (&uname_buf) < 0) {
        rcd_module_debug (RCD_DEBUG_LEVEL_WARNING,
                          rcd_module,
                          "Couldn't get hostname from uname()");
        hostname = "";
    } else
        hostname = uname_buf.nodename;

    ip = rcd_prefs_get_bind_ipaddress ();
    if (ip == NULL)
        ip = "";

    hostname_send (hostname, ip);
}

static gboolean
hostname_startup_cb (gpointer user_data)
{
    hostname_update (NULL);
    return FALSE;
}

static guint
hostname_get_first_timeout ()
{
    int fd;
    unsigned int seed;
    long int rnd;

    /* If debug level is high, send it in one minute,
       to make debugging easier. */
    if (rcd_prefs_get_debug_level () > 4)
        return 60 * 1000;

    /* Following is some 31337-ness from Joe */

    /* We don't really need to seed srandom from /dev/urandom, but
       it makes me feel all cool and 31337. */
    fd = open ("/dev/urandom", O_RDONLY);
    if (fd >= 0)
        read (fd, &seed, sizeof (seed));
    else
        seed = (guint) time (NULL) + (guint) getpid ();
    srandom (seed);

    /* Another unpardonable sin of random number generation. */
    rnd = random () % (10 * 60);

    /* Convert it to milliseconds */
    return rnd * 1000;
}

/*****************************************************************************/

void rcd_module_load (RCDModule *);

void
rcd_module_load (RCDModule *module)
{
    module->name = "rcd.hostname";
    module->description = "Module to update dynamic IP and hostname";
    module->version = 0;
    module->interface_major = 1;
    module->interface_minor = 0;

    rcd_module = module;

    g_timeout_add (hostname_get_first_timeout (), hostname_startup_cb, NULL);

    rcd_heartbeat_register_func (hostname_update, NULL);
} /* rcd_module_load */
